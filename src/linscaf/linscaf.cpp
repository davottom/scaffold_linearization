#include "io/io_handler.hpp"
#include "linscaf_wrapper.hpp"
#include "utils/instance.hpp"
#include "utils/scaffold_graph.hpp"
#include "utils/scaffolding_typedefs.hpp"

#include <string>

using namespace io;
using namespace scaffold;

void merge(Instance &i1, Instance &i2)
{
  std::map<ScafVertex,ScafVertex> corr_m;
  for(auto it_v=i2.get_vertices();it_v;++it_v){
    ScafVertex v=i1.add_vertex();

    corr_m.insert(std::make_pair(*it_v,v));
    i1[v].index_to_name();
  }

  for(auto it_e=i2.get_non_matching_edges();it_e;++it_e)
    i1.add_edge(corr_m[i2.source(*it_e)], corr_m[i2.target(*it_e)]);

  for(auto it_e=i2.get_matching_edges();it_e;++it_e)
    i1.add_matching_edge(corr_m[i2.source(*it_e)], corr_m[i2.target(*it_e)]);
}


int main(const int argc, const char **argv)
{
  // initialize command-line options
  auto opt_desc = initialize_options(argv[0]);

  add_linearization_options(opt_desc);
  // parse arguments
  const auto var_map = parse_arguments(argc, argv, opt_desc);
  SequenceMap *sequences = get_sequences(get_option<std::string>(var_map, "is", ""));
  const Options options =
    {
      handle_options(var_map, opt_desc),
    };

  const LinearizationOptions linoptions = handle_linearization_options(var_map);

  Instance *I(get_instance(options, sequences));
  go_linearize(I,sequences,options,linoptions);

  delete I;
  if(sequences!=nullptr)
    delete sequences;
  exit(EXIT_SUCCESS);
}
