
/** \file ambigous_paths.hpp
 * tools to remove ambigous paths in a solution graph with multiplicities
 *
 * A solution graph with multiplicities is not necessarily degree-2, but may contain
 * multiforcations. In this case, it may not be obvious how to split the graph into
 * paths and cycles. Indeed, it may not even be unambigous. It has been shown that
 * such a graph can be unambigously split into paths and cycles if and only if it does
 * not contain "ambigous paths". An ambigous path is an alternating path between two
 * vertices u, v such that both u and v are incident to edges of different multiplicity.
 *
 * Ambigous paths can be removed by deleting edges, but minimizing the number of
 * deleted edges to make a solution graph unambigous is basically VERTEX COVER (and,
 * thus, NP-hard). Here, we use a simple heuristic for the problem that just selects
 * one of the two necessary vertices for each edge & removes all covered edges.
 */

#pragma once


#include "utils/profiling.hpp"
#include "utils/stats.hpp"
#include "utils/scaffolding_typedefs.hpp"
#include "utils/predicates.hpp"

#ifdef has_HTD
#include "treeWidthLin/tw_dp_lin.hpp"
#endif

#ifdef CPLEX
#include "utils/ilp_common.hpp"
#endif

namespace scaffold {


  //! find and destroy ambiguous paths in sg by deleting edges
  /** this repeatedly finds an ambiguous path and deletes edges at one of its endpoints to make it unambiguous */
  void kill_ambiguous_paths_brutal(ScaffoldGraph &sg, LinearizationStatistics &stats)
  {
    for(auto u_iter = sg.get_vertices(); u_iter; ++u_iter) {
      const ScafVertex &u = *u_iter;
      if(sg.degree(sg.matched_with(u))>1) {
        DEBUG4(std::cout << "found ambiguous path " << sg[u].name << "-->" << sg[result.first].name << std::endl);
        // delete all non-matching edges incident with u
        stats.nb_cuts++;
        stats.weight_cut += sg.sum_multiplicity_non_matching(u);
	sg.delete_non_matching(u);

      }
    }
  }


  //! Apply the factor 2 approximation algorithm to destroy ambiguous paths
  /** this repeatedly finds the endpoint of an ambiguous path with the minimum sum of the weight of the incident edge
   * and deletes edges of it endpoint*/
  void kill_ambiguous_paths_approx(ScaffoldGraph &sg, LinearizationStatistics &stats)
  {
    /* We maintain a list of extremities of remaining ambiguous paths.
     * The list is sorted in descending order according to sum_multiplicities_u*/
    //
    std::vector<ScafVertex> v_ambiguous_paths;

    /* extremities_map contains the positions of the extremities in the list to ensure a constant time access.*/
    std::map<const ScafVertex,std::pair<int,ScafVertex>> extremities_map;
    std::set<ScafVertex> cuts;
    for(auto v_iter = sg.get_vertices(); v_iter; ++v_iter) {
      const ScafVertex &u = *v_iter;
      const ScafVertex &v = sg.matched_with(u);
      if(sg.degree(u)>1 && sg.degree(v)>1) {
        unsigned sum = sg.sum_multiplicity_non_matching(u);
	extremities_map.emplace(u,std::make_pair(sum,v));
	v_ambiguous_paths.push_back(u);
      }
    }
    std::sort(v_ambiguous_paths.begin(), v_ambiguous_paths.end(),
	      [&](const ScafVertex &a, const ScafVertex &b) {
		return std::get<0>(extremities_map[a]) <= std::get<0>(extremities_map[b]);
	      });
    std::cout <<"Ambiguous extremities sorted"<< std::endl;

    for(size_t i =0; i<v_ambiguous_paths.size();i++) {
      ScafVertex &v = v_ambiguous_paths[i];
      if(cuts.count(extremities_map[v].second))
	continue;

      stats.weight_cut += extremities_map[v].first;
      cuts.insert(v);
      for(auto inc_iter = sg.get_incident_non_matching(v);inc_iter;++inc_iter) {
	std::get<0>(extremities_map[sg.target(*inc_iter)])-=sg[*inc_iter].multiplicity;
      }

      sg.delete_non_matching(v);
      std::sort(v_ambiguous_paths.begin()+i+1,v_ambiguous_paths.end(),
		[&](const ScafVertex &a, const ScafVertex &b) {
		return std::get<0>(extremities_map[a]) <= std::get<0>(extremities_map[b]);
	      });
    }
  }

  /**
   * \brief Apply the tree decomposition method to linearize the graph
   */
  void kill_ambiguous_paths_tw(ScaffoldGraph sg, LinearizationStatistics &stats, bool count_edges)
  {
#ifdef has_HTD
    auto sol = DP_LIN::compute_solution(sg, !count_edges,stats.tw);
    stats.nb_cuts= sol.size() < stats.nb_cuts ? sol.size() : stats.nb_cuts;
    DEBUG1(std::cout << "Solution: {";);
    unsigned weight=0;
    for (auto v : sol) {
      weight+=  sg.sum_multiplicity_non_matching(v);
      DEBUG1(std::cout << sg[v].name << " ";);
      sg.delete_non_matching(v);
    }
    stats.weight_cut = weight < stats.weight_cut ? weight : stats.weight_cut;
    DEBUG1(std::cout << "}" << std::endl;);
#else
      std::cout << "You must compile the executable with -DDP_LIN=ON to access this feature" << std::endl;
#endif
  }


#ifdef CPLEX

  struct VarPair {
    IloNumVar remove;
    IloNumVar remove_all_neighbors;

    // construct
    VarPair(const IloEnv &env, const std::string &vertex_name) :
      remove(env, 0, 1, IloNumVar::Bool, (vertex_name + "r").c_str()),
      remove_all_neighbors(env, 0, 1, IloNumVar::Bool, (vertex_name + "rn").c_str())
    {
    }
  };

  typedef unordered_map<const ScafVertex, const VarPair> VertexVarPairMap;
  typedef boost::unordered_map<UnorderedVertexPair<RawScaffoldGraph>, IloNumVar> UEdgeVarMap;

  //! add variables of edges around a given vertex u and their dependency to u.remove
  void add_incident_edge_variables(const ScaffoldGraph &sg,
                                   const ScafVertex &u,
                                   const VarPair &u_vars,
                                   UEdgeVarMap &edge_vars,
                                   const IloEnv &env,
                                   IloRangeArray &constraints,
                                   IloExpr &opt_expr,
                                   const bool use_weight)
  {
    for(auto e_iter = sg.get_incident_non_matching(u); e_iter; ++e_iter) {
      const ScafEdge &e = *e_iter;
      const EdgeName e_name = sg.get_edge_name(e);

      const auto res = edge_vars.emplace(std::piecewise_construct,
                                         std::make_tuple(u, sg.target(e)),
                                         std::make_tuple(env, 0, 1, IloNumVar::Bool,
                                                         (e_name.first + "->" + e_name.second + "(r)").c_str()));

      const IloNumVar &e_var = res.first->second;
      constraints.add(e_var - u_vars.remove >= 0);
      if(res.second)
        opt_expr += (use_weight ? sg[e].multiplicity : 1) * e_var;
    }
  }

#endif

  //! use an ILP to find the least lossy resolution of ambiguous paths
  /** if count_edges is set, then minimize the deleted edges instead of the vertex at which to delete
   * otherwise, minimize the number of vertices that are cleared */
  void kill_ambiguous_paths_ILP(ScaffoldGraph &sg, LinearizationStatistics &stats, const bool count_edges)
  {
#ifdef CPLEX
    // initialize ILP model
    IloEnv env;
    IloModel model(env);
    IloRangeArray constraints(env);
    VertexVarPairMap vars;
    UEdgeVarMap edge_vars;

    timer opt_timer;
    opt_timer.start();
    IloExpr opt_expr(env);
    unsigned count = 0;
    for(auto v_iter = sg.get_vertices(); v_iter; ++v_iter) {
      const ScafVertex &u = *v_iter;
      // note: if u--v is an ambiguous path, then this is the only ambiguous path incient to u & v
      const ScafVertex &v = sg.matched_with(u);
      if(sg[u].index < sg[v].index) {
	++count;
        DEBUG1(
          std::cout << "found ambiguous path #" << count << ": " << sg[u].name << " (NH:";
          for(auto e_it = sg.get_incident(u); e_it; ++e_it) std::cout << " " << sg[sg.target(*e_it)].name;
          std::cout << ")\t--> " << sg[result.first].name << " (NH:";
          for(auto e_it = sg.get_incident(result.first); e_it; ++e_it) std::cout << " " << sg[sg.target(*e_it)].name;
          std::cout << ")" << std::endl;
        );
	
	
        const VarPair &u_vars = vars.DEEP_EMPLACE_TWO(u, env, sg[u].name).first->second;
        const VarPair &v_vars = vars.DEEP_EMPLACE_TWO(v, env, sg[v].name).first->second;
        // in order to destroy this ambiguous path, we have 4 possibilities:
        // 1. remove all incident non-matching edges of u
        // 2. remove all incident non-matching edges of v
        // 3. remove all incident non-matching edges of all vertices in N(u)
        // 3. remove all incident non-matching edges of all vertices in N(v)
        IloExpr uv(env);
        uv += u_vars.remove;
        uv += u_vars.remove_all_neighbors;
        uv += v_vars.remove;
        uv += v_vars.remove_all_neighbors;
        constraints.add(uv >= 1);

        if(count_edges) {
          add_incident_edge_variables(sg, u, u_vars, edge_vars, env, constraints, opt_expr, true);
          add_incident_edge_variables(sg, v, v_vars, edge_vars, env, constraints, opt_expr, true);
        } else
          for(const VarPair &x_vars: {u_vars, v_vars}) opt_expr += x_vars.remove;
      }
    }

    // add consistency constraints: remove_all_neighbors(u) = 1 <==> remove(v) = 1 for all neighbors v of u
    DEBUG3(std::cout << "adding linearization consistency constraints" << std::endl);
    for(const auto &i: vars) {
      const ScafVertex &u = i.first;
      const VarPair &u_vars = i.second;
      assert(sg.degree(u) > 1);

      IloExpr exp(env);
      for(auto e_iter = sg.get_incident_non_matching(u); e_iter; ++e_iter) {
	const auto v_var_iter = vars.find(sg.target(*e_iter));
	assert(v_var_iter != vars.end());
	exp +=v_var_iter->second.remove;
      }
      // if any one of the neighbors of u is not an end of an ambiguous path, then it's better to remove all incident to u instead
      constraints.add(exp - (int) (sg.degree(u) - 1) * u_vars.remove_all_neighbors >= 0);
    }

    DEBUG3(std::cout << "adding constraints and target to the model..." << std::endl);
    model.add(constraints);
    model.add(IloMinimize(env, opt_expr));

    std::ofstream nullStream("/dev/null");
    IloCplex cplex(model);
    cplex.setOut(nullStream);
    
    cplex.solve();

    
    if(!cplex.solve()) {
      env.error() << "Failed to linearize" << std::endl;
      exit(EXIT_FAILURE);
    } else {
      opt_timer.stop();
      //      env.out() << "CPU-seconds spent linearizing: " << opt_timer.seconds_passed() << std::endl;
      DEBUG2(if(count_edges) env.out() << "will remove " << cplex.getObjValue() << " edges: " << std::endl;
             else env.out() << "will clear " << cplex.getObjValue() << " vertices: " << std::endl);

    }

    unsigned weight=0, cuts=0;
    // implement the solution in sg
    for(const auto &i: vars) {
      if(is_true(cplex.getValue(i.second.remove))) {
        DEBUG2(if(count_edges) for(auto e = sg.get_incident_non_matching(i.first); e; ++e) std::cout << sg.get_edge_name(*e) << " ";
               else std::cout << sg[i.first].name << " ");
        ++cuts;
        weight+= sg.sum_multiplicity_non_matching(i.first);
	sg.delete_non_matching(i.first);
      }
    }
    stats.nb_cuts = cuts < stats.nb_cuts ? cuts : stats.nb_cuts;
    stats.weight_cut = weight < stats.weight_cut ? weight : stats.weight_cut;
    DEBUG2(std::cout << std::endl;);
    cplex.end();
    model.end();
    env.end();
#else
    std::cout << "You must compile the executable with -DCPLEX=ON to access this feature"<<std::endl;
#endif

  }

}//namespace
