/*
 * preprocess a given graph for linearization
 */

/* ----------------------------------------------------------------- */

#pragma once

#include <random>

#include "utils/graph_typedefs.hpp"
#include "utils/scaffold_graph.hpp"
#include "utils/aligning.hpp"
#include "utils/scaffolding_typedefs.hpp"


namespace scaffold{
  namespace preprocess {


    void contract_uniform_paths(ScaffoldGraph &g) {
      std::vector<ScafEdge> non_matching_edges;

      for(auto inc_iter = g.get_non_matching_edges(); inc_iter; ++inc_iter)
        non_matching_edges.push_back(*inc_iter);

      for(const ScafEdge& ux: non_matching_edges) {
        const ScafVertex& u = g.source(ux);
        const ScafVertex& x = g.target(ux);
        const ScafEdge uv = g.incident_matching_edge(u);
        const ScafEdge xy = g.incident_matching_edge(x);
        const ScafVertex& v = g.target(uv);
        const ScafVertex& y = g.target(xy);

	if(!g.adjacent(v,y) && (g[ux].multiplicity == g[uv].multiplicity && g[ux].multiplicity == g[xy].multiplicity)){
	  const LengthType xy_len = g[xy].length;
          const LengthType ux_len  = g[ux].length;
          const LengthType uv_len = g[uv].length;
          const MultiType xy_mul = g[xy].multiplicity;
          contract_non_contig(g, v, u, x, y, xy_len + ux_len + uv_len, xy_mul,false); // contract ux
        }
      }
    }

    void remove_isolated_matching(ScaffoldGraph &g) {

      std::vector<ScafEdge> matching_edges;
      for(auto it = g.get_matching_edges(); it;++it)
	matching_edges.push_back(*it);

      for(ScafEdge &e : matching_edges)
	if(g.degree(g.source(e))==1 && g.degree(g.target(e))==1){
	  g.delete_edge(e);
	  g.delete_vertex(g.source(e));
	  g.delete_vertex(g.target(e));
	}
    }

    void add_noise(ScaffoldGraph &g, std::vector<double> &percentages){
      srand(static_cast<unsigned int>(time(nullptr)));

      std::default_random_engine generator;
      std::uniform_real_distribution<double> distribution(0.0,1.0);

      unsigned i=0,j=0;
      for(auto it = g.get_vertices();it;++it){
	for(auto it2 = g.get_vertices();it2;++it2){
	  if(g[*it2].index<=g[*it].index || g.adjacent(*it,*it2)) continue;

	  //std::cout << g.degree(*it) << " | " << g.degree(*it2) << std::endl;
	  float percentage1 = g.degree(*it) > percentages.size() ? percentages.back() : percentages[g.degree(*it)-1];
	  float percentage2 = g.degree(*it2) > percentages.size() ? percentages.back() : percentages[g.degree(*it2)-1];

	  float percentage=std::min(percentage1,percentage2);
	  //std::cout << "percentage: " << percentage1 << "|" << percentage2 << "=>" << percentage <<std::endl;
	
	double random_number=distribution(generator);
	  //std::cout << "tirage: " << random_number << std::endl << std::endl;
	  if(random_number<percentage){
	    if(g.degree(*it)>7 || g.degree(*it2)>7){
	      if(j++==10) exit(EXIT_SUCCESS);
	      std::cout << g[*it].index << "-" << g[*it2].index <<std::endl;
	      std::cout << g.degree(*it) << ":" <<percentage1 << "|"
			<< g.degree(*it2) << ":"<< percentage2 << std::endl;
	      std::cout << random_number<< std::endl;
	    }
	    ScaffoldGraph::EdgeProperty e;
	    e.multiplicity=1;
	    e.length=-1;
	    e.weight=(rand() % 100) + 1;
	    g.add_edge(*it, *it2,e);
	    i++;

	  }
	}
      }
      std::cout << "AAAAAAA " << i <<std::endl;



    }
  } // namespace
} // namespace

