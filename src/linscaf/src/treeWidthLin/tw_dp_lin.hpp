#pragma once

#include <cstdlib>
#include <htd/main.hpp>
#include <map>
#include <memory>
#include <utility>



#include "signature.hpp"
#include "utils/graph_typedefs.hpp"
#include "utils/scaffold_graph.hpp"
#include "utils/scaffolding_typedefs.hpp"
#include "utils/solution.hpp"




namespace scaffold {
  namespace DP_LIN {

    std::atomic<bool> stop_cut(false);
    std::atomic<bool> stop_weight(false);
    
    typedef htd::vertex_t Vertex;
    typedef htd::IMutableGraph Graph;
    typedef SignatureCollection<Vertex> SignatureCollection_v;

    struct tw_data {
      std::unique_ptr<htd::LibraryInstance> manager;
      const ScaffoldGraph &sg;
      htd::ITreeDecompositionAlgorithm *algorithm;
      std::unique_ptr<Graph> g;
      const htd::ITreeDecomposition *t;
      
      bool cut_score;
      
      std::set<std::pair<unsigned,unsigned>> introducedEdges;
      std::map<ScafVertex,unsigned> malus;
      std::map<Vertex,ScafVertex> corr_m;
      std::map<ScafVertex, Vertex> reverse_m;

      std::set<ScafVertex> solution;
      

      tw_data(const ScaffoldGraph &sg, bool cut_score) :  manager(htd::createManagementInstance(htd::Id::FIRST)), sg(sg),
							  algorithm( manager->treeDecompositionAlgorithmFactory().createInstance()),					  g(manager->graphFactory().createInstance()), cut_score(cut_score)
      {
	algorithm->addManipulationOperation(new htd::NormalizationOperation(manager.get(), true, true, true, true));
      }
    };



    SignatureCollection_v hub_dw(tw_data &d, htd::vertex_t bag);


    void printDecomposition(const htd::ITreeDecomposition *t)
      {
        htd::PreOrderTreeTraversal traversal;
        DEBUG1(std::cout << "Root: " << t->root() << std::endl;)

        traversal.traverse(*t, [&](htd::vertex_t vertex,
                                   htd::vertex_t parent,
                                   std::size_t distanceToRoot) {
          for(htd::index_t index=0; index < distanceToRoot; ++index)
            std::cout << " ";
          std::cout << "Node " << vertex << ": " << t->bagContent(vertex)
                    << "-> " << parent;

          if(t->isLeaf(vertex))
            std::cout<<" Leaf ";
          if(t->isIntroduceNode(vertex)) std::cout<<" Introduce ";
          if(t->isJoinNode(vertex)) std::cout<<" Join ";
          if(t->isForgetNode(vertex)) std::cout<<" Forget ";
          if(t->isExchangeNode(vertex)) std::cout << " Exchange ";
          std::cout <<std::endl;



        });
      }


    SignatureCollection_v
    getCollectionSignatureIntroduceBag(tw_data &d,htd::vertex_t bag)
    {

      htd::vertex_t introduced_vertex = d.t->introducedVertexAtPosition(bag, 0);

      DEBUG1(
      std::cout << "Introduce Bag: " 
		<< bag << "-> " << d.t->bagContent(bag) << "(" 
		<< introduced_vertex << ")" << std::endl;);

      // Build neigbour list
      std::vector<std::tuple<Vertex,bool,unsigned>> neighbors;
      ScafVertex u1= d.corr_m[introduced_vertex];

      unsigned malus=0;
      auto it_tmp = d.malus.find(u1);
      if(it_tmp!=d.malus.end()) {
	malus=it_tmp->second;
	it_tmp->second=0;
      }

      
      for(htd::vertex_t v : d.t->bagContent(d.t->childAtPosition(bag, 0))){
	ScafVertex u2= d.corr_m[v];
	if(d.sg.adjacent(u1,u2)){
	  auto edge =std::minmax(introduced_vertex,v);
	  
	  if(d.introducedEdges.find(edge)==d.introducedEdges.end()){
	    d.introducedEdges.insert(std::move(edge));
	    neighbors.push_back(std::make_tuple(v, d.sg.matched_with(u1)==u2,d.sg.edge_multiplicity(u1,u2)));
	  } else neighbors.push_back(std::make_tuple(v, d.sg.matched_with(u1)==u2,0));

	}
      }
      return hub_dw(d, d.t->childAtPosition(bag, 0)).extendIntroduceVertex(introduced_vertex, d.cut_score, neighbors,d.sg[u1].index,malus);
    }


     SignatureCollection_v
    getCollectionSignatureJoinBag(tw_data &d, htd::vertex_t bag)
    {
      DEBUG1(
      std::cout << "Join Bag: " << bag << "-> " << d.t->bagContent(bag) << std::endl;);

      return hub_dw(d, d.t->childAtPosition(bag, 0)).extendJoinNode(hub_dw(d,d.t->childAtPosition(bag, 1)));
      
    }



    SignatureCollection_v
    getCollectionSignatureForgetBag(tw_data &d, htd::vertex_t bag)
    {
      if(d.t->forgottenVertexCount(bag) == 0) {
        // TODO: I don't know why the parent of a join bag can be a forget bag with 0 forgotten vertex
	DEBUG1(
	std::cout << "BUG: " << bag << " -> " << d.t->bagContent(bag) << "(" << "bug" << ")" << std::endl;)
        return hub_dw(d,d.t->childAtPosition(bag, 0));
      }
      htd::vertex_t forgotten_vertex = d.t->forgottenVertexAtPosition(bag, 0);
      DEBUG1(
      std::cout << "Forget Bag: " << bag << " -> " << d.t->bagContent(bag) << "(" << forgotten_vertex << ")" << std::endl;)

	return hub_dw(d,d.t->childAtPosition(bag, 0)).extendForgetVertex(forgotten_vertex, d.cut_score);

    }


    SignatureCollection_v hub_dw(tw_data &d, htd::vertex_t bag)
    {
      if((stop_cut && d.cut_score) || (stop_weight && !d.cut_score))
	return SignatureCollection_v(true);
      
      if(d.t->isLeaf(bag))
        return SignatureCollection_v(true);
      if(d.t->isIntroduceNode(bag)) return getCollectionSignatureIntroduceBag(d, bag);
      if(d.t->isJoinNode(bag)) return getCollectionSignatureJoinBag(d, bag);
      return getCollectionSignatureForgetBag(d, bag);
    }

    void construct_graph(tw_data &d)
    {
      std::set<ScafVertex> to_delete;
      
      for(auto it_v=d.sg.get_vertices();it_v;++it_v){
	if(d.sg.degree(*it_v)==1)
	  {
	    to_delete.insert(*it_v);
	    continue;
	  }
	if(!d.cut_score && d.sg.degree(d.sg.matched_with(*it_v))==1){
	  to_delete.insert(*it_v);
	  
	  for(auto it_e=d.sg.get_incident_non_matching(*it_v);it_e;++it_e){
	    ScafVertex v = *it_v== d.sg.source(*it_e) ? d.sg.target(*it_e) : d.sg.source(*it_e);
	    if(d.malus.find(v)==d.malus.end()) d.malus[v]=0;
	    d.malus[v]+=d.sg[*it_e].multiplicity;
	  }
	}
      }
      
      for(auto it_v=d.sg.get_vertices();it_v;++it_v){
	if(to_delete.find(*it_v)!=to_delete.end()) continue;

	bool b_tmp=!d.cut_score;
	for(auto e=d.sg.get_incident_non_matching(*it_v);e && b_tmp;++e){
	  if(to_delete.find(d.sg.target(*e))==to_delete.end())
	    b_tmp=false;
	}
	for(auto e=d.sg.get_incident_non_matching(d.sg.matched_with(*it_v));e && b_tmp;++e){
	  if(to_delete.find(d.sg.target(*e))==to_delete.end())
	    b_tmp=false;
	}
	if(b_tmp){
	  ScafVertex v1 =*it_v, v2=d.sg.matched_with(v1);
	  to_delete.insert(v1);
	  to_delete.insert(v2);
	  if(d.malus[v1]<d.malus[v2])
	    d.solution.insert(v1);
	  else d.solution.insert(v2);
	  continue;
	}
	
	htd::vertex_t v =d.g->addVertices(1);
	d.reverse_m.insert(std::make_pair(*it_v,v));
	d.corr_m.insert(std::make_pair(v,*it_v));

	ScaffoldGraph::VertexSet nh;
	d.sg.get_neighbors(*it_v, nh);
	for(ScafVertex u : nh){
	  auto it_u = d.reverse_m.find(u);
	  if(it_u!=d.reverse_m.end()) 
	    d.g->addEdge(v, it_u->second);
	  
	}
      }
    }

    std::set<ScafVertex> compute_solution(ScaffoldGraph &sg, bool cut_score, unsigned &tw)
    {
      std::unique_ptr<Graph> graph;
      std::map<ScafVertex,unsigned> malus;
      std::map<htd::vertex_t,ScafVertex> corr_m;

      tw_data d(sg,cut_score);

      construct_graph(d);
      d.t = d.algorithm->computeDecomposition(*d.g);

      tw=d.t->maximumBagSize()-1;

      auto s = hub_dw(d, d.t->root()).getSolution();
      for(auto v : s)
	d.solution.insert(d.corr_m[v]);
      return d.solution;
      
    }
  }
}

