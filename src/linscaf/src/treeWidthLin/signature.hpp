#pragma once

#include <boost/numeric/conversion/converter_policies.hpp>
#include <climits>
#include <iostream>
#include <iomanip>

#include <array>
#include <map>
#include <memory>
#include <set>
#include <utility>
#include <vector>


namespace scaffold {
  namespace DP_LIN {

    enum S_Type {
      Cut, // vertex is cut
      Neighbour_cut, // all neighbour of the vertex
      Uncut // vertex is not cut
    };

    std::ostream &operator<<(std::ostream &os, const S_Type &c)
    {
      switch(c) {
        case Cut: {
          os << "C";
          break;
        }
        case Neighbour_cut: {
          os << "N";
          break;
        }
        case Uncut: {
          os << "U";
          break;
        }
        default:
          break;
      }
      return os;
    }


    template<typename Vertex>
    bool map_less(const std::map<Vertex, S_Type>& lhs, const std::map<Vertex, S_Type>& rhs) {
      auto l_it = lhs.begin();
      auto r_it = rhs.begin();
      while (l_it != lhs.end() && r_it != rhs.end()) {
        if (l_it->first < r_it->first) 
	  return true;
        
        if (r_it->first < l_it->first) 
	  return false;
        
        if (l_it->second < r_it->second) 
	  return true;
        
        if (r_it->second < l_it->second) 
	  return false;
        
        ++l_it;
        ++r_it;
      }
      return false;
}


    
    template<typename Vertex>
    class SignatureCollection : public std::map<std::map<Vertex,S_Type>,std::pair<unsigned,std::set<Vertex>>,bool (*)(const std::map<Vertex,S_Type>&,const std::map<Vertex,S_Type>&)>
    {
      typedef std::pair<Vertex, S_Type> SignatureByte;
      typedef std::pair<std::map<Vertex,S_Type>,std::pair<unsigned,std::set<Vertex>>> sign_p;
      typedef std::map<std::map<Vertex,S_Type>,std::pair<unsigned,std::set<Vertex>>> map_s;

      std::map<Vertex,unsigned> correspond;
      void display_sig(const std::map<Vertex,S_Type> &s,bool single = true)
      {
	std::cout << "[";
        if(single){
	  for(auto b : s)
	    std::cout << b.second;
	} else {
	  for(auto b : s)
	       std::cout<< "(" << correspond[b.first] <<", " <<b.second << ") ";
	}
	std::cout << "]";
      }

      void display_sig(const sign_p &s,bool single = true)
      {
	display_sig(s.first,single);
	std::cout << "=>" << s.second.first << ":{ ";
	for(auto &v : s.second.second)
	  std::cout << correspond[v] << " ";
	std::cout << "}" << s.second.second.size() <<std::endl;
      }


      static sign_p leaf()
      {
	return std::make_pair<std::map<Vertex,S_Type>,std::pair<unsigned,std::set<Vertex>>>(std::map<Vertex,S_Type>(),std::make_pair<unsigned,std::set<Vertex>>(0,std::set<Vertex>()));
      }

      static sign_p introduceVertex(const sign_p &s, SignatureByte v,unsigned malus)
      {
	sign_p new_s(s);
	new_s.first.insert(v);
	if(v.second==Cut){
	  new_s.second.second.insert(v.first);
	  new_s.second.first+=malus;
	}
	return new_s;
      }

      static sign_p forgetVertex(const sign_p &s, Vertex v)
      {
	sign_p new_s(s);
	new_s.first.erase(v);
	return new_s;
      }

      static std::pair<bool,bool>  introduceEdge(const sign_p &s,Vertex u, Vertex v, bool is_matching)
      {
	if(is_matching)
	  return std::make_pair(s.first.at(u)==Uncut && s.first.at(v)==Uncut,false);
	
	return std::make_pair((s.first.at(u)==Neighbour_cut && s.first.at(v)!=Cut) ||
				(s.first.at(v)==Neighbour_cut && s.first.at(u)!=Cut),
			      s.first.at(u)==Cut || s.first.at(v)==Cut);	
      }
      

      
      
    public:
      
            
      SignatureCollection(bool leaf = false) : std::map<std::map<Vertex,S_Type>,std::pair<unsigned,std::set<Vertex>>,bool (*)(const std::map<Vertex,S_Type>&,const std::map<Vertex,S_Type>&)>(map_less)
      {
	if(leaf)
	  this->insert(SignatureCollection<Vertex>::leaf());
      }


      SignatureCollection<Vertex> extendIntroduceVertex(Vertex v, bool cut_score, std::vector<std::tuple<Vertex,bool,unsigned>> & neighbors, unsigned label, unsigned malus)
      {

	correspond[v]=label;
	std::vector<S_Type> s_types = cut_score ?
	  std::vector<S_Type>{Cut,Uncut,Neighbour_cut} : std::vector<S_Type>{Cut,Uncut};

	SignatureCollection<Vertex> tmp;
	tmp.correspond=correspond;
	
	for(auto s : *this){
	  for(auto t : s_types){
	    auto new_sign= SignatureCollection<Vertex>::introduceVertex(s, {v,t},malus);

	    bool introduce=true;
	    for(auto &p : neighbors){
	      auto x = SignatureCollection<Vertex>::introduceEdge(new_sign, v, get<0>(p), get<1>(p));
	      if(x.first) introduce=false;
	      new_sign.second.first+=get<2>(p);
	    }
	    if(introduce)
	      tmp.insert(new_sign);
	  }
	}
	return tmp;
      }

      SignatureCollection<Vertex> extendForgetVertex(Vertex v, bool cut_score)
      {
	SignatureCollection<Vertex> tmp;

	for(auto s : *this){
	  auto new_sign = SignatureCollection<Vertex>::forgetVertex(s, v);
	
	  auto it=tmp.find(new_sign.first); 
	  if(it!=tmp.end()){
	    if((!cut_score && it->second.first > new_sign.second.first) ||
	       (cut_score && it->second.second.size() > new_sign.second.second.size())){
	      it->second.first = new_sign.second.first;
	      it->second.second = std::move(new_sign.second.second);
	    }
	  } else tmp.insert(std::move(new_sign));
	}
	
	return tmp;
      }

      SignatureCollection<Vertex> extendJoinNode(SignatureCollection<Vertex> s_c)
      {
	for(auto it = this->begin(); it!= this->end();){
	  auto it_2 = s_c.find(it->first);
	  if(it_2==s_c.end())
	    it = this->erase(it);
	  else {
	    it->second.first+=it_2->second.first;
	    for(auto v : it_2->second.second)
	      it->second.second.insert(v);
	    ++it;
	  }
	}
	return *this;
      }


      /**
       * \brief    Give the minimum solution of the graph
       *           if and only if the SignatureCollection
       *           is that of the root of the TreeDecomposition
       *
       */
      std::set<Vertex> getSolution() const
      {
	return (*this).begin()->second.second;
      }


    };
  }
}
