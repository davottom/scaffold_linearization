
#pragma once
#include "utils/command_line.hpp"

struct LinearizationOptions {
  //! bool incidating whether we should count deleted edges or cleaned vertices
  bool count_vertices;

  //! bool indicating whether we use the approximation algorithm or not
  bool use_approx;

  //! bool indicating whether we use the brut algorithm or not
  bool use_brute;

  //! bool indicating whether we deconstruct the graph into sequences after linearizing it
  bool deconstruct;

  //! indicating whether we apply the linearization reduction rules
  bool reduction_rules;

  //! filename of a file to export the linearization statistics
  std::string stats_file;

  //! bool indicating whether we use the tree decomposition method or not
  bool tw;

  // bool indicating if we want to compare tw and ilp methods 
  bool compare;

};

//! add options to the global options list
void add_linearization_options(po::options_description& desc)
{
  desc.add_options()
    ("cv", "when linearizing, minimize cleaned vertices instead of deleted edges")
    ("app","when linearizing, use approximation algorithm (default: ILP)")
    ("brut", "when linearizing, use the brutal algorithm (default: ILP)")
    ("dec", "deconstruct the graph into sequences after the linearization process")
    ("rrules", "apply the linearization reduction rules on the graph (for readability purposes)")
    ("lstats", po::value<std::string>(), "file to output linearization stats")
    ("ltw", "when linearizing, use the tree decomposition method")
    ("lcompare", "when linearizing, compare computation time of tw and ilp methods; the computation is made 25 times for each method and each score and the average time is written in the stats file")
            ;
}

//! handle ILP specific options passed by the command-line
LinearizationOptions handle_linearization_options(const po::variables_map& vm)
{
    LinearizationOptions lin_opts;

    lin_opts.count_vertices = vm.count("cv");
    lin_opts.use_approx = vm.count("app");
    lin_opts.use_brute = vm.count("brut");
    lin_opts.deconstruct = vm.count("dec");
    lin_opts.reduction_rules = vm.count("rrules");
    lin_opts.tw = vm.count("ltw");
    lin_opts.compare = vm.count("lcompare");

    lin_opts.stats_file                       = get_option<std::string>(vm, "lstats", "");
    return lin_opts;
}
