#pragma once

#include "utils/graph_typedefs.hpp"
#include "utils/scaffolding_typedefs.hpp"
#include "utils/scaffold_graph.hpp"
#include "utils/instance.hpp"
#include "ambiguous_paths.hpp"
#include "lin_options.hpp"
#include <chrono>

 
using namespace boost;

namespace scaffold{


  SolutionGraphStats compute_stats_solution_graph(const ScaffoldGraph &sg)
  {
    SolutionGraphStats stats;
    stats.sum_weight = sg.sum_multiplicities();

    for(auto it=sg.get_non_matching_edges();it;++it)
      stats.nb_non_matching_edges++;
    unsigned sum_delta=0;

    for(auto v_iter = sg.get_vertices(); v_iter; ++v_iter) {
      const ScafVertex &u = *v_iter;
      const ScafVertex &v = sg.matched_with(u);

      sum_delta += sg.degree(u);
      if(sg.degree(u) > stats.max_degree) stats.max_degree = sg.degree(u);
      if(sg.degree(u) < stats.min_degree) stats.min_degree = sg.degree(u);

      if(sg.degree(u)>1 && sg.degree(v)>1) {
        ++stats.nb_ambiguous_paths;
      } else ++stats.nb_non_ambiguous_paths;
    }
    stats.nb_ambiguous_paths /= 2;
    stats.nb_non_ambiguous_paths /= 2;
    stats.average_degree= (float) sum_delta/sg.num_vertices();

    return stats;
  }

  void avg_time(int nb, std::chrono::microseconds t[25], std::ofstream &ofs)
  {
    int minutes=0, seconds=0, milliseconds=0, microseconds=0;
    std::chrono::microseconds avg(0);
    for(size_t i=0; i<nb;++i)
      avg+=t[i];
    avg/=nb;
    minutes = std::chrono::duration_cast<std::chrono::minutes>(avg).count();
    seconds = std::chrono::duration_cast<std::chrono::seconds>(avg).count() % 60;
    milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(avg).count() % 1000;
    microseconds = avg.count() % 1000;
    if(minutes!=0)
      ofs<<minutes<<"m";
    if(seconds!=0 || minutes!=0)
      ofs<<seconds<<"s";
    if(milliseconds!=0 || minutes!=0 || seconds !=0)
      ofs<<milliseconds<<"ms";
    ofs<<microseconds<<std::endl;
	
  }


  void export_latex_computation(std::string name_instance,const LinearizationOptions &lin_opts, LinearizationStatistics &stats, SolutionGraphStats & sg_stats)
  {
    if(lin_opts.stats_file != "") {
      size_t pos_sep = name_instance.find_last_of("/\\");
      if (pos_sep != std::string::npos) 
        name_instance= name_instance.substr(pos_sep + 1);
      

      std::ofstream ofs;
      ofs.open(lin_opts.stats_file, std::ofstream::out | std::ofstream::app);
      ofs << name_instance << " & " << stats.tw << " & ";
      if(lin_opts.compare){
	int average=0;
	for(const auto &t : stats.duration_ilp_cut)
	  average+=t.count();
	average/=25;
	ofs << stats.nb_cuts << " & " << average << " & ";

	average=0;
	for(const auto &t : stats.duration_tw_cut)
	  average+=t.count();
	average/=25;
	ofs << average << " & ";

	average=0;
	for(const auto &t : stats.duration_ilp_weight)
	  average+=t.count();
	average/=25;
	ofs << stats.weight_cut << " & " << average << " & ";

	average=0;
	for(const auto &t : stats.duration_tw_weight)
	  average+=t.count();
	average/=25;
	ofs << average << " \\\\\n";
	}
      ofs.close();
    }
  }

    void export_latex_stats(std::string name_instance,const LinearizationOptions &lin_opts, LinearizationStatistics &stats, SolutionGraphStats & sg_stats)
  {
    if(lin_opts.stats_file != "") {
      size_t pos_sep = name_instance.find_last_of("/\\");
      if (pos_sep != std::string::npos) 
        name_instance= name_instance.substr(pos_sep + 1);
      

      std::ofstream ofs;
      ofs.open(lin_opts.stats_file, std::ofstream::out | std::ofstream::app);
      ofs << name_instance << " & " << sg_stats.nb_ambiguous_paths << " & "
	  << sg_stats.nb_non_ambiguous_paths << " & " << sg_stats.sum_weight << " & "
	  << sg_stats.average_degree << " & "
	  << sg_stats.min_degree << "/" << sg_stats.max_degree << "\\\\\n ";
      ofs.close();
    }
  }

  




  
  void print_stats_linearization(std::string name_instance,const LinearizationOptions &lin_opts, LinearizationStatistics &stats, SolutionGraphStats & sg_stats)
  {
    if(lin_opts.stats_file != "") {
      DEBUG1(std::cout << "Writing stats in " << lin_opts.stats_file << std::endl);

      size_t pos_sep = name_instance.find_last_of("/\\");
      if (pos_sep != std::string::npos) 
        name_instance= name_instance.substr(pos_sep + 1);
      

      std::ofstream ofs;
      ofs.open(lin_opts.stats_file, std::ofstream::out | std::ofstream::app);


      ofs << std::string(name_instance.length()+20,'*') << std::endl;
      ofs << "*" <<std::string(name_instance.length()+18,' ') << "*" << std::endl;
      ofs << "*         " << name_instance << "         *" << std::endl;
      ofs << "*" <<std::string(name_instance.length()+18,' ') << "*" << std::endl;
      ofs << std::string(name_instance.length()+20,'*') << std::endl;

      if(!lin_opts.compare){
	ofs << "Options: ";
	if(lin_opts.count_vertices) ofs << "Cut Score | ";
	else ofs << "Weight Score |";
	if(lin_opts.tw) ofs << "Tree decomposition" << std::endl;
	else ofs << "ILP" << std::endl;
	ofs << std::string(name_instance.length()+20,'-') << std::endl;
      }
      ofs << "Degree (max|min|avg): " <<sg_stats.max_degree
	  << "|"<<sg_stats.min_degree
	  << "|" << sg_stats.average_degree << std::endl;
      ofs << "#ambiguous|#non-ambiguous: " << sg_stats.nb_ambiguous_paths << "|" << sg_stats.nb_non_ambiguous_paths << std::endl;
      ofs << "Total weight: " << sg_stats.sum_weight << std::endl;
      if(lin_opts.tw || lin_opts.compare)
        ofs << " Treewidth: " << stats.tw << std::endl;

      ofs << std::string(name_instance.length()+20,'-') << std::endl;

      ofs << " Number of cuts: " << stats.nb_cuts << std::endl;
      ofs << " Weight removed: " << stats.weight_cut << std::endl;
      
      //      std::chrono::microseconds average=std::chrono::microseconds(0);

      ofs << std::string(name_instance.length()+20,'-') << std::endl;

      if(lin_opts.compare){
	ofs << "Computation time: \n";
    
	
	ofs << "ILP cut   : ";
	avg_time(1, stats.duration_ilp_cut, ofs);

	ofs << "TW cut    : ";
	avg_time(1, stats.duration_tw_cut, ofs);

	ofs << "ILP weight: ";
	avg_time(1, stats.duration_ilp_weight, ofs);

	ofs << "TW weight : ";
	avg_time(1, stats.duration_tw_weight, ofs);

      }
      ofs << std::string(name_instance.length()+20,'-') << std::endl;
      ofs << std::endl << std::endl << std::endl;
      ofs.close();
    }
  }



  // given a solution to a scaffold graph
  // deconstruct the solution graph into a set of disjoint alternating paths & cycles
#warning TODO: optimize this avoiding all the string copies (maybe use a "fragmented string" class?)
  void deconstruct_solution(ScaffoldGraph& sg, const LinearizationOptions& lin_options)
  {
#warning TODO: use an external mapping of edgenames to contig names. Right now, we copy the contig names each time we copy the graph!

    // step 1: break multiplicities in a RawScaffoldGraph by deleting edges incident to endpoints of ambiguous paths
    //DEBUG4(std::cout << "removing ambiguous paths..."<<std::endl;)
    //kill_ambiguous_paths_brutal(sg);
    //kill_ambiguous_paths_ILP(sg, !lin_options.count_vertices);

    // step 2: deconstruct
    DEBUG4(std::cout << "deconstructing the graph..."<<std::endl;)
    const RawScaffoldGraph& g(sg.get_graph());

    boost::unordered_set<ScafVertex> to_check;
    for(auto range = boost::vertices(g); range.first != range.second; ++range.first)
      to_check.insert(*range.first);

    while(!to_check.empty()){
      const ScafVertex v = *(to_check.cbegin());
      to_check.erase(to_check.cbegin());

      if(boost::degree(v, g) > 1){
        // if u has non-matching edges
        const ScafEdge& uv = sg.incident_matching_edge(v);
        const ScafVertex& u = boost::target(uv, g);
        const ScafEdge vw = *(sg.get_incident_non_matching(v));
        const ScafVertex& w = boost::target(vw, g);
        const unsigned uv_multi = sg[uv].multiplicity;
        const unsigned vw_multi = sg[vw].multiplicity;
        // if v is incident to a non-matching edge vw of different multiplicity than uv, split off vw
        if(uv_multi != vw_multi){
          DEBUG4(std::cout << "detected break point at "<<sg[u].name<<" --"<<sg[uv] <<"--> "<<sg[v].name<<" --x"<<sg[vw]<<"--> "<<sg[w].name<<std::endl;)

          // add new u'-v'-w
          const ScafVertex u_prime = sg.add_vertex(u);
          const ScafVertex v_prime = sg.add_vertex(v);

          const ScafEdge uv_prime = sg.add_matching_edge(u_prime, v_prime, sg[uv]).first;
          DEBUG4(const ScafEdge vw_prime =)
            sg.add_edge(v_prime, w, sg[vw]).first;
          sg[uv].multiplicity -= vw_multi;
          sg[uv_prime].multiplicity = vw_multi;
          DEBUG4(std::cout << "added matching edge "<<sg.get_edge_name(uv_prime)<<" ["<<sg[uv_prime]<<"]" <<std::endl;)
          DEBUG4(std::cout << "added edge "<<sg.get_edge_name(vw_prime)<<" ["<<sg[vw_prime]<<"]" <<std::endl;)

          // force another iteration for v & w
          to_check.insert(v);
          to_check.insert(w);

          // add y-u' if u was not deg-1
          if(sg.degree(u) > 1){
            assert(sg.degree(u) == 2);
            const ScafEdge& uy = *(sg.get_incident_non_matching(u));
            const ScafVertex& y = boost::target(uy, g);
            const ScafEdge yu_prime = sg.add_edge(y, u_prime, sg[uy]).first;
            sg[uy].multiplicity -= vw_multi;
            sg[yu_prime].multiplicity = vw_multi;
            DEBUG4(std::cout << "added edge "<<sg.get_edge_name(yu_prime)<<" ["<<sg[yu_prime]<<"]" <<std::endl;)
            assert(sg[uy].multiplicity == sg[uv].multiplicity);
            // check y next
            to_check.insert(y);
          } // if deg(u) == 2

          // delete vw
          sg.delete_edge(vw);
        }// if uv & vw have different multiplicity
      }// if v has non-matching edges
    }// while there are vertices to check
  }// function
}
