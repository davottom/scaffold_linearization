#include <chrono>
#include <climits>
#include<iostream>
#include <fstream>

#include <thread>
#include <mutex>
#include <future>
#include <condition_variable>

#include "linscaf/src/treeWidthLin/tw_dp_lin.hpp"
#include "utils/command_line.hpp"
#include "utils/instance.hpp"

#include "io/io_handler.hpp"

#include "src/lin_utils.hpp"
#include "src/linearization.hpp"
#include "src/ambiguous_paths.hpp"
#include "src/lin_options.hpp"
#include "utils/scaffold_graph.hpp"



void linearization(ScaffoldGraph & reduced_graph, const LinearizationOptions &lin_opts, LinearizationStatistics &stats)
{
  DEBUG1(std::cout << "deconstructing the solution by destroying ambiguous paths..." << std::endl);
  DEBUG4(std::cout << "removing ambiguous paths..." << std::endl);

  if(lin_opts.compare){
    for(unsigned i=0; i<25;++i){

      ScaffoldGraph copy_tw_weight = reduced_graph;
      auto start = std::chrono::high_resolution_clock::now();
      scaffold::kill_ambiguous_paths_tw(copy_tw_weight, stats, true);
      auto end = std::chrono::high_resolution_clock::now();
      stats.duration_tw_weight[i] = std::chrono::duration_cast<std::chrono::microseconds>(end - start);


      ScaffoldGraph copy_tw_cut = reduced_graph;
      start = std::chrono::high_resolution_clock::now();
      scaffold::kill_ambiguous_paths_tw(copy_tw_cut, stats,false);
      end = std::chrono::high_resolution_clock::now();
      stats.duration_tw_cut[i] = std::chrono::duration_cast<std::chrono::microseconds>(end - start);

      ScaffoldGraph copy_ilp_weight = reduced_graph;
      start = std::chrono::high_resolution_clock::now();
      scaffold::kill_ambiguous_paths_ILP(copy_ilp_weight, stats, true);
      end = std::chrono::high_resolution_clock::now();
      stats.duration_ilp_weight[i] = std::chrono::duration_cast<std::chrono::microseconds>(end - start);

      ScaffoldGraph copy_ilp_cut = reduced_graph;
      start = std::chrono::high_resolution_clock::now();
      scaffold::kill_ambiguous_paths_ILP(copy_ilp_cut, stats, false);
      end = std::chrono::high_resolution_clock::now();
      stats.duration_ilp_cut[i] = std::chrono::duration_cast<std::chrono::microseconds>(end - start);

    }
    return;
  }


  
  if(lin_opts.use_brute)
    scaffold::kill_ambiguous_paths_brutal(reduced_graph, stats);
  else if(lin_opts.use_approx)
    scaffold::kill_ambiguous_paths_approx(reduced_graph, stats);
  else if(lin_opts.tw)
    scaffold::kill_ambiguous_paths_tw(reduced_graph, stats, !lin_opts.count_vertices);
  else
    scaffold::kill_ambiguous_paths_ILP(reduced_graph, stats, !lin_opts.count_vertices);
      
  if(lin_opts.deconstruct) deconstruct_solution(reduced_graph, lin_opts);

  
}


void apply_reduction_rules(ScaffoldGraph &g)
{
  DEBUG1(std::cout << "applying linearization reduction rules..." << std::endl);
  //  preprocess::add_noise(g, 0.15);
  preprocess::contract_uniform_paths(g);
  //  preprocess::remove_isolated_matching(g);
}

void go_linearize(Instance* I, SequenceMap *sequences, Options options, LinearizationOptions linoptions){


    ScaffoldGraph solution_copy(*I);
    LinearizationStatistics stats({UINT_MAX, UINT_MAX});

    SolutionGraphStats sg_stats = compute_stats_solution_graph(solution_copy);
    
    //apply_reduction_rules(solution_copy);

    linearization(solution_copy, linoptions, stats);
    //export_latex_computation(options.general.in_graph_filename,linoptions, stats,sg_stats);
    if(linoptions.compare)
      print_stats_linearization(options.general.in_graph_filename,linoptions, stats,sg_stats);

    io::handle_output_graph(solution_copy, options);
    io::handle_output_seq(solution_copy, sequences, options);

}
