
#pragma once

#include <iostream>
#include <fstream>
#include <string>

#include "utils/exceptions.hpp"
#include "utils/scaffolding_typedefs.hpp"
#include "utils/instance.hpp"
#include "utils/oriented_sequences.hpp"

#define FASTA_MAX_LINELENGTH 79

namespace io {

  // read a fasta file into an associative name_to_seq mapping a contig name to its sequence
  void read_fasta_file(std::istream& input, scaffold::SequenceMap& name_to_seq, const std::string& acceptable_bases = BASES)
  {
    scaffold::SequenceMap::iterator current_entry;
    bool success;
    unsigned line_no = 0;
    std::string line; // input buffer
    
    name_to_seq.clear();
    while(std::getline(input, line).good()){
      ++line_no;
      if(!line.empty()){
        if(line[0] == '>'){
          // if the line starts with '>' it's a contig name
          // step 1: remove leading and trailing whitespaces of the contig name
          std::string trimmed_line(trim(line.erase(0, 1)));
          if(trimmed_line.empty()) throw except::bad_syntax(line_no, "empty contig name");
          // step 2: insert into the unordered_map
          boost::tie(current_entry, success) = name_to_seq.DEEP_EMPLACE(trim(line), "");
          if(!success) throw except::bad_syntax(line_no, (std::string)"repeated contig name: " + line);
        } else {
          // if the line is not empty and does not start with '>', then it's part of the contig sequence
          if(current_entry == name_to_seq.end()) throw except::bad_syntax(line_no, "missing contig name");
          // sanity check
          if(line.find_first_not_of(acceptable_bases) != std::string::npos)
            throw except::bad_syntax(line_no, (std::string)"contains a base that's not in " + acceptable_bases);
          // append the line to the current sequence
          current_entry->second.sequence += line;
        }// if
      }// if line not empty
    }// while file contains data
  }// function

  // write the contigs into a fasta file 
  void write_contigs_as_fasta(std::ostream& out, scaffold::MultiSequenceMap& name_to_seq)
  {
    for(const auto& name_seq: name_to_seq){
      for(unsigned i = 0; i < name_seq.second.counter; ++i){
        out << ">" << name_seq.first;
        // write stars at the end of each sequence indicating its index in the multiplicity (starting with 0 stars)
        for(unsigned j = 0; j < i; ++j) out << '*';
        out << std::endl;

        unsigned pos = 0;
        try{
          while(true){
            const std::string buffer = name_seq.second.sequence.substr(pos, FASTA_MAX_LINELENGTH);
            out << buffer << std::endl;
            pos += FASTA_MAX_LINELENGTH;
          }
        } catch(std::out_of_range& e) {};
      }
    }
  }// function


}// namespace

