
#pragma once


#include <iostream>
#include "utils/instance.hpp"
#include "utils/scaffolding_typedefs.hpp"
#include "utils/command_line.hpp"
#include "utils/alignment_options.hpp"
#include "utils/ilp_options.hpp"
#include "utils/profiling.hpp"
#include "preprocess/negative_length.hpp"
#include "io/dot.hpp"
#include "io/fasta.hpp"

typedef struct {
  GeneralOptions general;
  AlignmentOptions alignment;
  #ifdef CPLEX
    ILPOptions ilp;
  #endif
} Options;


namespace io {

    Instance* read_instance(std::ifstream& infile,
                            const GeneralOptions& options,
                            SequenceMap* sequence_orientations = NULL)
    {
      Instance* result;
      if(infile.is_open()){
        DEBUG1(std::cout << "reading input file" << std::endl);
        // try dot first
        result = io::read_dot(infile, options, sequence_orientations);
        if(result == NULL)
          DEBUG1(std::cout << "Nothing read. Verify that the input file is in DOT format."<<std::endl);
        infile.close();
        return result;
      } else return NULL;
    }

    Instance* read_instance(std::ifstream& infile, SequenceMap* sequences = NULL)
    {
      return read_instance(infile, GeneralOptions(), sequences);
    }

    void write_scaffold(std::ostream& outfile, const ScaffoldGraph& sg)
    {
      write_scaffold_as_dot(outfile, sg.get_graph());
    }


    Instance *get_instance(const Options &options, SequenceMap *sequences)
    {
      timer read_timer(true);
      std::ifstream infile(options.general.in_graph_filename);
      Instance *I_ptr = read_instance(infile, options.general, sequences);
      read_timer.stop();

      if(I_ptr == nullptr) {
        std::cout << "couldn't read the input file " << options.general.in_graph_filename << std::endl;
        exit(EXIT_FAILURE);
      } else std::cout << "read instance (took " << read_timer.seconds_passed() << " seconds)" << std::endl;
      return I_ptr;
    }


    SequenceMap *read_sequences(const std::string &filename)
    {
      if(filename != "") {
        std::ifstream is(filename);
        SequenceMap *sequences = new SequenceMap();
        read_fasta_file(is, *sequences);
        return sequences;
      } else return nullptr;
    }

    SequenceMap *get_sequences(const std::string &in_seq_filename)
    {
      if(in_seq_filename != "") {
        timer read_fa_timer(true);
        SequenceMap *sequences = read_sequences(in_seq_filename);
        read_fa_timer.stop();

        if(sequences == nullptr) {
          std::cout << "couldn't read the sequences from file " << in_seq_filename << std::endl;
          exit(EXIT_FAILURE);
        } else {
          std::cout << "read sequences (took " << read_fa_timer.seconds_passed() << " seconds)" << std::endl;
    #ifdef STATISTICS
          SequenceStatistics in_stat(*sequences);
          in_stat.print();
    #endif
          return sequences;
        }
      } else return nullptr;
    }

    void handle_output_seq(ScaffoldGraph &sg, SequenceMap *sequences, const Options &options)
    {
      MultiSequenceMap output_sequences;
      if(sequences) {
        // step 2: merge contigs with negative-length non-contig edges between them
        DEBUG1(std::cout << "fixing negative non-contig lengths..." << std::endl);
        fix_negative_non_contig_lengths(sg, *sequences, options.alignment);
        // step 3: convert the instance into sequences by tracing paths
        simple_solution_to_sequences(sg, options.alignment, *sequences, output_sequences);


    #ifdef STATISTICS
        // give sequence statistiques
        SequenceStatistics sstat(output_sequences);
        sstat.print();
    #endif
      }
      if(options.general.out_seq_filename != "") {
        std::cout << "writing contigs in fasta format to " << options.general.out_seq_filename << std::endl;
        std::ofstream os(options.general.out_seq_filename);
        write_contigs_as_fasta(os, output_sequences);
      }
    }

    void handle_output_graph(const ScaffoldGraph &sg, const Options &options)
    {
      if(options.general.out_graph_filename != "") {
        std::ofstream os(options.general.out_graph_filename);
        write_scaffold(os, sg);
      }
    }

}// namespace
