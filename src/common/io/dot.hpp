
#pragma once

#include <iostream>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/graphviz.hpp>

#include "utils/graph_typedefs.hpp"
#include "utils/scaffolding_typedefs.hpp"
#include "utils/scaffolding_utils.hpp"
#include "utils/instance.hpp"
#include "utils/command_line.hpp"
#include "utils/oriented_sequences.hpp"

using namespace scaffold;

namespace io {

#warning TODO: find a better way to read sequence orientations! Currently, we just take the vertex whose name is "smaller"
  // orient sequences using the names of the wincident vertices
  void derive_sequence_orientations_from_vertex_names(const ScaffoldGraph& sg, SequenceMap& sequences)
  {
    const RawScaffoldGraph& g = sg.get_graph();
    for(auto range = boost::vertices(g); range.first != range.second; ++range.first){
      const ScafVertex& u = *range.first;
      const ScafEdge uv = sg.incident_matching_edge(u);
      const ScafVertex& v = boost::target(uv, g);
      const VertexName& u_name = sg[u].name;
      const VertexName& v_name = sg[v].name;
      const std::string& contig_name = sg[uv].contig_name;
      std::string& start_vertex = sequences[contig_name].start_vertex;
      if(start_vertex.empty()){
        // try to interprete vertex names as numbers
        try{
          const int u_num = std::stoi(u_name);
          const int v_num = std::stoi(v_name);
          start_vertex = (u_num < v_num) ? u_name : v_name;
        } catch(std::exception& e){
          // if the vertex names cannot be convertex to numbers, use lexicographical compare :/
          start_vertex = (u_name < v_name) ? u_name : v_name;
        }// try
      }// if the start_vertex has not been set
    }// for each vertex of g
  }

  // read the graph g from the instream "in"
  Instance* read_dot(std::istream& in,
                     const GeneralOptions& options = GeneralOptions(),
                     SequenceMap* sequences = nullptr)
  {
    RawScaffoldGraph* g = new RawScaffoldGraph();
    boost::dynamic_properties dp(boost::ignore_other_properties);
    // Step 0: setup the dynamic properties
    dp.property("node_id", boost::get(&ScafVertexProperty::index, *g));
    dp.property("label", boost::get(&ScafVertexProperty::name, *g));
    // attention: edges without "label" attribute are considered contig edges!
    dp.property("label", boost::get(&ScafEdgeProperty::weight, *g));
    dp.property("length", boost::get(&ScafEdgeProperty::length, *g));
    dp.property("meandist", boost::get(&ScafEdgeProperty::length, *g));
    dp.property("cname", boost::get(&ScafEdgeProperty::contig_name, *g));
    if(!options.greedy)
      dp.property("mult", boost::get(&ScafEdgeProperty::multiplicity, *g));
    // Step 0a: setup property for the insert size & standard deviation of read gaps
    typedef boost::unordered_map<RawScaffoldGraph*, unsigned> GraphIntMap;
    GraphIntMap insert_size_map, sd_map;
    boost::associative_property_map<GraphIntMap> insert_size_map_wrapper(insert_size_map);
    boost::associative_property_map<GraphIntMap> sd_map_wrapper(sd_map);
    dp.property("insert_size", insert_size_map_wrapper);
    dp.property("sd", sd_map_wrapper);

    // Step 1: read temporary graph
    try{
      if(boost::read_graphviz(in, *g, dp)){
        // put the insert_size & the std deviation into the graph
        unsigned& insert_size = (*g)[graph_bundle].insert_size;
        unsigned& standard_deviation = (*g)[graph_bundle].standard_deviation;
        if(insert_size_map.empty()){
          std::cerr << "WARNING: no insert-size found in dot, lengths may be inaccurate" << std::endl;
          insert_size = NO_LENGTH;
        } else {
          insert_size = insert_size_map.at(g);
          if(sd_map.empty()){
            std::cerr << "WARNING: no distribution information over the insert-size found in dot, using standard deviation of "<<insert_size/3<<" (1/3 of insert size)" << std::endl;
            standard_deviation = insert_size / 3;
          } else standard_deviation = sd_map.at(g);
        }


        // build the instance
        setup_vertex_names(*g);
        Instance* outI = new Instance(g, options.paths, options.cycles, options.objects);
        // finally, derive sequence orientations
        if(sequences) derive_sequence_orientations_from_vertex_names(*outI, *sequences);
        DEBUG3(std::cout << "read instance: "; outI->print_statistics(); std::cout << std::endl);
        return outI;
      } else {
        DEBUG3(std::cout << "error parsing graphviz file!" << std::endl);
        delete g;
        return nullptr;
      }
    } catch(boost::bad_parallel_edge& ex){
      std::cout << "Parallel-edge exception: "<< ex.what() << " - file not in dot format"<<std::endl;
      delete g;
      return nullptr;
    } catch(boost::directed_graph_error& ex){
      std::cout << "Directed graph exception: "<< ex.what() << " - file not in dot format"<<std::endl;
      delete g;
      return nullptr;
    } catch(boost::undirected_graph_error& ex){
      std::cout << "Undirected graph exception: "<<ex.what() << " - file not in dot format"<<std::endl;
      delete g;
      return nullptr;
    } catch(boost::bad_graphviz_syntax& ex){
      std::cout << "Syntax error: "<< ex.what() << " - file not in dot format"<<std::endl;
      delete g;
      return nullptr;
    }
  } // function




  template <class WhatToWrite>
  struct gviz_property_writer {
    const RawScaffoldGraph& g;

    gviz_property_writer(const RawScaffoldGraph& _g): g(_g) {}

    void write_property(std::ostream& out, const std::string& name, const std::string& value, const std::string& middle = "") const{
      out << name << " = \""<< middle << value <<"\"";
    }
    void write_property(std::ostream& out, const std::string& name, const long& value, const std::string& middle = "") const{
      out << name << " = "<< middle << value;
    }

  };


  struct gviz_simple_graph_writer: public gviz_property_writer<RawScaffoldGraph> {
    using gviz_property_writer<RawScaffoldGraph>::g;
    using gviz_property_writer<RawScaffoldGraph>::write_property;
    using gviz_property_writer<RawScaffoldGraph>::gviz_property_writer;

    void operator()(std::ostream& out) const
    {
      out << "graph [";
      this->write_property(out, "insert_size", g[graph_bundle].insert_size);
      this->write_property(out, ", sd", g[graph_bundle].standard_deviation);
      // strangely, the ";" & endl are taken care of by boost for vertex and edge properties, but not for graph properties :/
      out << "];" << std::endl;
    }

  };

  struct gviz_simple_vertex_writer: public gviz_property_writer<ScafVertex> {
    using gviz_property_writer<ScafVertex>::g;
    using gviz_property_writer<ScafVertex>::gviz_property_writer;

    void write_properties(std::ostream& out, const ScafVertex& u) const
    {
      this->write_property(out, "label", g[u].name);
    }
    void operator()(std::ostream& out, const ScafVertex& u) const
    {
      out << "[";
      write_properties(out, u);
      out << "]";
    }
  };


  struct gviz_simple_edge_writer: public gviz_property_writer<ScafEdge> {
    using gviz_property_writer<ScafEdge>::g;
    using gviz_property_writer<ScafEdge>::gviz_property_writer;

    void write_properties(std::ostream& out, const ScafEdge& e) const{
      const ScafEdgeProperty& prop = g[e];
      if(prop.is_matching_edge()){
        this->write_property(out, "cname", prop.contig_name);
        this->write_property(out, ", length", prop.length);
        this->write_property(out, ", style", "bold");
        this->write_property(out, ", mult", prop.multiplicity);
        if(prop.multiplicity > 1)
          this->write_property(out, ", color", "darkgreen");
      } else {
        this->write_property(out, "label", prop.weight, " ");
        this->write_property(out, ", meandist", prop.length);
      }
    }
    void operator()(std::ostream& out, const ScafEdge& e) const{
      out << "[";
      write_properties(out, e);
      out << "]";
    }
  };

  struct gviz_edge_writer_with_jumps: gviz_simple_edge_writer {
    using gviz_simple_edge_writer::g;
    ScafEdgeSet jump_edges;

    gviz_edge_writer_with_jumps(const RawScaffoldGraph& _g, const std::list<contig_jump>& _jumps):
      gviz_simple_edge_writer(_g),
      jump_edges(default_buckets, _g)
    {
      for(const contig_jump& jump : _jumps){
        jump_edges.insert(jump.contig);
        for(const ScafEdge& e : jump.path) jump_edges.insert(e);
      }
    }
    void write_properties(std::ostream& out, const ScafEdge& e) const{
      if(contains(jump_edges, e)){
        if(g[e].is_matching_edge())
          this->write_property(out, "color", "purple");
        else
          this->write_property(out, "color", "blue");
        out << ", ";
      } else {
        if(g[e].is_matching_edge() && (g[e].length < (int)g[graph_bundle].insert_size)){
          this->write_property(out, "color", "red");
          out << ", ";
        }
      }
      gviz_simple_edge_writer::write_properties(out, e);
    }
    void operator()(std::ostream& out, const ScafEdge& e) const{
      out << "[";
      write_properties(out, e);
      out << "]";
    }
  };

  // write the graph g to the outstream "out"
  template<class EdgeWriter>
  void _write_scaffold_as_dot(std::ostream& out, const RawScaffoldGraph& g, const EdgeWriter& ew)
  {
    const gviz_simple_vertex_writer vw(g);
    const gviz_simple_graph_writer gw(g);
    boost::write_graphviz(out, g, vw, ew, gw, boost::get(&ScafVertexProperty::index, g));
  }

  // write the graph g to the outstream "out"
  void write_scaffold_as_dot(std::ostream& out, const RawScaffoldGraph& g)
  {
    _write_scaffold_as_dot(out, g, gviz_simple_edge_writer(g));
  } // function

  // write the graph g with the given jump set to the outstream "out"
  void write_scaffold_as_dot(std::ostream& out, const RawScaffoldGraph& g, const std::list<contig_jump>& jumps)
  {
    _write_scaffold_as_dot(out, g, gviz_edge_writer_with_jumps(g, jumps));
  } // function
}// namespace
