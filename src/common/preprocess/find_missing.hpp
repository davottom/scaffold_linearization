
#pragma once

#include "utils/alignment_options.hpp"
#include "utils/aligning.hpp"

// the weight of a new edge that is inserted because of an overlap between contigs is (FACTOR * length + OFFSET)
#define OVERLAP_EDGE_WEIGHT_FACTOR 0
#define OVERLAP_EDGE_WEIGHT_OFFSET 0

namespace scaffold{

  void check_overlap(ScaffoldGraph& sg, 
                     const AlignmentOptions& align_opts,
                     const SequenceMap& sequences,
                     const ScafVertex& u,
                     const ScafVertex& v)
  {
    ScafContigAligner aligner(sg, sequences, align_opts, u, v);
    aligner.compute_best_alignment();
    const Alignment best = aligner.get_best_alignment();
    if(best >= aligner.get_threshold_alignment()){
      DEBUG4(std::cout << "found size-"<<best.first<<" overlap, so adding edge ("<<sg[u]<<","<<sg[v]<<")"<<std::endl);
      const MultiType new_multiplicity = std::min(sg[sg.incident_matching_edge(u)].multiplicity, sg[sg.incident_matching_edge(v)].multiplicity);
      const WeightType new_weight = OVERLAP_EDGE_WEIGHT_FACTOR * (-best.first) + OVERLAP_EDGE_WEIGHT_OFFSET;
      sg.add_edge(u, v, ScafEdgeProperty(new_weight, -best.first, new_multiplicity), false);
    }// if there is a sane alignment of u==v & y==z
    else DEBUG5(std::cout << " contigs do not overlap at "<<sg[u]<<" & "<<sg[v]<<std::endl);
  }

  //! find missing links by overlapping all contigs pairwisely
  void find_missing_links(ScaffoldGraph& sg, const AlignmentOptions& align_opts, const SequenceMap& sequences)
  {
    typedef typename ScaffoldGraph::Graph Graph;
    typedef PredicatedVertexIter<Graph> VIter;
    for(VIter u_it = sg.get_vertices(); u_it; ++u_it){
      const ScafVertex& u = *u_it;
      const ScafVertex& x = sg.matched_with(u);
      // get an iterator that skips x
      PredicatedVertexIter<Graph, EqualPredicate<ScafVertex> > v_it(u_it, EqualPredicate<ScafVertex>(x, true));
      while(++v_it) check_overlap(sg, align_opts, sequences, u, *v_it);
    }
  }
}
