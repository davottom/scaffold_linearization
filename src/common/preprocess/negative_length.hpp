
#ifndef NEGATIVE_LENGTH_HPP
#define NEGATIVE_LENGTH_HPP

#include <cmath>
#include "utils/string_utils.hpp"
#include "utils/normal_distribution.hpp"
#include "utils/scaffolding_typedefs.hpp"
#include "utils/aligning.hpp"

#define POSITIVE_MIN_GAP 0
#warning "TODO: if there is no satisfying overlap between the contigs incident to a negative-length edge, we currently fix the edgelength to 0 (disallowing variance). Find something more realistic!"

namespace scaffold{

  // handle non-contig edges of negative length
  void fix_negative_non_contig_lengths(ScaffoldGraph& sg,
                                       SequenceMap& sequences,
                                       const AlignmentOptions& align_opts,
                                       const bool merge_overlapping_contigs = true)
  {
    const ReadDistribution dist(sg.get_graph_property().standard_deviation);

    // we keep a count indicating if a sequence is still needed, so we can delete it if necessary
    StringCount use_count;
    for(auto e_it = sg.get_matching_edges(); e_it.is_valid(); ++e_it){
      const auto used_it = use_count.DEEP_EMPLACE(sg[*e_it].contig_name, 1);
      // if no insertion took place, increase use count instead
      if(!used_it.second) used_it.first->second++;
    }

    for(auto e_it = sg.get_non_matching_edges(); e_it;){
      const ScafEdge& e = *e_it;
      const ScafVertex u = sg.source(e);
      const ScafVertex v = sg.target(e);
      ScafEdgeProperty& e_prop = sg[e];
      LengthType& e_length = e_prop.length;
      if(e_length < 0){
        DEBUG3(std::cout << "found edge "<<sg.get_edge_name(e)<<" with length "<<e_length<<std::endl);
        ScafContigMerger merger(sg, sequences, align_opts, e, false);
        if(merger.sanity_check()){
          merger.compute_best_alignment(merge_overlapping_contigs);
          const Alignment best = merger.get_best_alignment();

          if(best >= merger.get_threshold_alignment()){
            if(merge_overlapping_contigs){
              // if we have a convincing alignment, merge the two contigs using the alignments shift value
              // first, save our iterator
              while(e_it &&
                  ((sg.incident(u, *e_it) || sg.incident(v, *e_it)) ||
                   (sg.incident(sg.matched_with(u), *e_it) && sg.incident(sg.matched_with(v), *e_it)))) ++e_it;
              merger.contract_edge(best.consensus_sequence, &use_count);
            } else {
              // fix the length to the right alignment
              DEBUG3(std::cout << "correcting length to "<<-best.first<<std::endl);
              e_length = -best.first;
              ++e_it;
            }
          } else {
            // no alignment is satisfying, so set the length to 0
            e_length = POSITIVE_MIN_GAP;
            ++e_it;
          }
        } else {
          // xuvy has negative total length
          e_length = NO_LENGTH;
          ++e_it;
        }
      } else ++e_it;
    }// for all non-matching edges of sg
  }


  // treat negative non-contig lengths if we don't have sequences:
  // just merge the contigs and modify the length (unless this length would be negative, in which case do nothing)
  void fix_negative_non_contig_lengths_without_sequences(ScaffoldGraph& sg)
  {
    const RawScaffoldGraph& g = sg.get_graph();
    for(auto e_it = sg.get_non_matching_edges(); e_it.is_valid(); ++e_it){
      const ScafEdge& e = *e_it;
      int& e_length = sg[e].length;
      if(e_length < 0){
        // step 1: get the two incident contigs
        const ScafVertex u = boost::source(e, g);
        const ScafVertex v = boost::target(e, g);
        const ScafEdge ux = sg.incident_matching_edge(u);
        const ScafEdge vy = sg.incident_matching_edge(v);
        const ScafVertex x = boost::target(ux, g);
        const ScafVertex y = boost::target(vy, g);
        const int xu_length = sg[ux].length;
        const int vy_length = sg[vy].length;
        const int xuvy_length = xu_length + vy_length + e_length;

        // only merge contigs if the total length will be positive
        if(xuvy_length > 0){
          contract_non_contig(sg, x, u, v, y, xuvy_length, sg[e].multiplicity);
        } else {
          // otherwise consider the length of e erroneous
          e_length = NO_LENGTH;
        }

      }// if e has negative length
    }// for all non-matching edges of sg
  }

  // treat insane non-contig distances
  // a non-contig length is insane if
  // (a) it is negative and its absolute value is bigger than one of the incident contig lengths
  // (b) it is positive and the probability of picking a value at least this length is below a given threshold
  void fix_implausible_lengths(ScaffoldGraph& sg, const AlignmentOptions& align_opts)
  {
    const ScafGraphProperty& gp = sg.get_graph_property();
    const RawScaffoldGraph& g = sg.get_graph();
    for(auto e_it = sg.get_non_matching_edges(); e_it.is_valid(); ++e_it){
      const ScafEdge& e = *e_it;
      const UncertainLength e_length = sg.get_uncertain_length(e);
      if(e_length.mean < 0){
        const ScafVertex u = boost::source(e, g);
        const ScafVertex v = boost::target(e, g);
        const ScafEdge ux = sg.incident_matching_edge(u);
        const ScafEdge vy = sg.incident_matching_edge(v);
        const LengthType xu_length = sg[ux].length;
        const LengthType vy_length = sg[vy].length;

        const LengthType target_length = align_opts.contig_in_contig ?
                                    -xu_length - vy_length :
                                    -std::min(xu_length, vy_length);
        // a non-contig length is invalid if THE MAX of the two incident contig lengths is too small
        if(e_length.prob_of_picking_at_least(target_length) < align_opts.min_length_probability){
          DEBUG5(std::cout << sg.get_edge_name(e) << " has implausible length:  \t"<<e_length
                           <<" (adjacent contigs have lengths "<<xu_length<<" & "<<vy_length<<")"<<std::endl);
          sg.remove_length(e);
        }
      } else if((unsigned)e_length.mean > gp.insert_size) {
        // e_length > insert_size from here
        // (if 0 < e_length < insert_size, the read might still have length insert_size, but it aligned inside the second contig)
        // the goal is now to check if e_length is maybe too big
        
        const ReadDistribution insert_size_distribution(gp.standard_deviation, gp.insert_size);
        // step 1: get the value x such that picking something that is at least as far as x away from e_length has prob at most min_length_prob
        const float x_quantile = e_length.quantile(align_opts.min_length_probability / 2.0f);
        // step 2: get the probability of picking something at least as far from the insert size as x_quantile
        const float prob = insert_size_distribution.prob_of_picking_at_least(x_quantile) * 2.0f;

        DEBUG5(std::cout << sg.get_edge_name(e) << " has length:  \t"<<e_length<<" (probability "<<prob<<")"<<std::endl);
        if(prob < align_opts.min_length_probability){
          sg.remove_length(e);
          DEBUG5(std::cout << sg.get_edge_name(e) << " has implausible length:  \t"<<e_length<<" (probability "<<prob<<")"<<std::endl);
        }
      }
    }
  }
}


#endif

