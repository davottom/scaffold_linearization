
#pragma once

#include "utils/graph_typedefs.hpp"
#include "utils/scaffolding_typedefs.hpp"
#include "utils/aligning.hpp"
#include "utils/stats.hpp"

namespace scaffold{


  // the jump preprocessing allows "jumps"
  // that is, placing small contigs in the gap between two contigs.
  // To achieve this, we might have to add some edges, for example
  // if u-------v
  //    |
  //    \--x==y
  // we have to add the edge yv to allow xy to be placed in uv.
  // If we do this, however, we need to make sure that, whenever yv
  // is in a solution, so is ux! So, return this dependency as a map
  //
  // also, map all possible jumps uv over xy to the arc xy

  // a queue that gives us the next edge to insert, sorted by length (largest first)
  typedef std::priority_queue<std::pair<int, ScafVertexPair> > InsertQueue;

  // a map of edges of negative length to coordinates in their respective contigs
  typedef boost::unordered_map<ScafVertexPair, std::pair<unsigned, unsigned> > EdgeMarkerMap;

  // given a small contig xy, add edges to support it being placed in appropriate non-contigs
  // keep interesting_edges up to date by adding contigs that might have become interesting again by adding new non-contigs
  // fill to_insert with edges to insert with their appropriate lengths
#warning TODO: when adding an edge yv in the jump preprocessing, there might be many reasons x--u==v for it and we take the one maximizing the resulting length of yv, that is |yv|=|xy|-(|xu|+|uv|). This is safe, but may disregard some jumps. It may be better to have a list of possible lengths for each inserted edge. How about a multigraph that is overlayed over the RawScaffoldGraph?
  //! support placing the contig uv into the non-contig xy by inserting the edge vy, assuming all lengths check out
  void support_jump_3path(const ScaffoldGraph& sg,
                          const ScafVertex& y,
                          const ScafVertex& x,
                          const ScafVertex& u,
                          const ScafVertex& v,
                          const int vy_length,
                          InsertQueue& to_insert,
                          ScafVPairDependencyMap& dependencies
                         )
  {
    // from here, we have y---x---u===v
    // now, mark v---y for addition & register its dependence on x---u
    const ScafVertexPair vy(v,y);
    const ScafVertexPair yv(y,v);
    const ScafVertexPair xu(x,u);
    const ScafVertexPair ux(u,x);
    //yv_in_sg = sg.add_edge(v, y, ScafEdgeProperty(0, vy_length, contig_multi));
    // NOTE: since there might be alot of paths x--u--v suggesting the insertion of v--y,
    //       we will register v--y as depending on x--u for each of them, and its length
    //       will be the maximum of the lengths suggested by these paths
    // NOTE: this strategy of dealing with multiple paths suggesting the insertion is
    //       correct and save, but may disregard some possible jumps!

    to_insert.emplace(vy_length, vy);
    // register dependencies of the new edge on the old edge
    dependencies[yv].emplace_back(ux);
    dependencies[vy].emplace_back(xu);
  }

  //! support placing a contig in xy using the edge xu and its incident matching edge uv
  void support_jump_using(const ScaffoldGraph& sg,
                          const AlignmentOptions& align_opts,
                          const ScafVertex& x,
                          const ScafVertex& y,
                          const UncertainLength& xy_length,
                          const ScafVertex& u,
                          const UncertainLength& xu_length,
                          InsertQueue& to_insert,
                          ScafVPairDependencyMap& dependencies,
                          const SequenceMap* const sequences = NULL)
  {
    assert(xy_length.mean != NO_LENGTH);
    if(sg.matched_with(u) != y){
      const ScafVertexPair ux(u, x);
      const ScafVertexPair xu(x, u);
      if(xu_length.mean != NO_LENGTH){
        const ScafEdge& uv = sg.incident_matching_edge(u);
        const ScafVertex& v = sg.target(uv);
        const LengthType uv_length = sg[uv].length;
        UncertainLength vy_length = xy_length - (xu_length + uv_length);
        // check if vy is already in sg and don't continue if we have no hope of updating its length
        if(!sg.find_edge(v, y).second){
          DEBUG5(std::cout << "next 3-path:  "<< sg[x].name <<"->"<<sg[u].name << "->"<< sg[v].name <<" -- len: "<<xu_length+uv_length<<" previsioned length of "<<sg[v].name<<"->"<<sg[y].name<<": "<<vy_length<<" - prob of picking >0: "<<vy_length.prob_of_picking_at_least(0)*100<<"% "<<std::endl);
          // from here, we have y---x---u===v
          // now, mark v---y for addition & register its dependence on x---u
          if(vy_length.prob_of_picking_at_least(0) >= align_opts.min_length_probability){
            DEBUG5(std::cout << "found edge to insert: "<<sg[v].name<<"->"<<sg[y].name<<" (len "<<vy_length<<" = "<<xy_length<<" (len of "<<sg[x].name<<"->"<<sg[y].name<<") - "<<xu_length<<" (len of "<<sg[x].name<<"->"<<sg[u].name<<") - "<<uv_length<<" (len of "<<sg[u].name<<"->"<<sg[v].name<<"))"<<std::endl);
            support_jump_3path(sg, y, x, u, v, std::max(0, vy_length.mean), to_insert, dependencies);
          } else {
            // if vy has negative length, try aligning uv with yz (where z is matched with y)
            if(sequences){
              ScafContigAligner aligner(sg, *sequences, align_opts, v, y, vy_length);
              if(aligner.sanity_check()){
                aligner.compute_best_alignment();
                const Alignment best = aligner.get_best_alignment();
                if(best >= aligner.get_threshold_alignment()){
                  DEBUG5(std::cout << "found edge for overlap "<<-best.first<<": "<<sg[v].name<<"->"<<sg[y].name<<" (len "<<-best.first<<") by aligning "<<sg[u].name<<"->"<<sg[v].name<<" & "<<sg.get_edge_name(sg.incident_matching_edge(y))<<" with a score of "<<best.second<<std::endl);
                  support_jump_3path(sg, y, x, u, v, -best.first, to_insert, dependencies);
                }// if there is a sane alignment of u==v & y==z
                else DEBUG5(std::cout << " no alignment around overlap "<<vy_length<<" is good"<<std::endl);
              }// if the total length of u==v--y==z is > 0
            }// if sequences are given
          }// if x--u--v--y fits ov
        } else DEBUG5(std::cout << "not inserting "<<sg[v]<<"->"<<sg[y]<<" since it's already there"<<std::endl);
      }// if xu has a length
    }// if uy is not a contig
  }

  void support_jump_directional(const ScaffoldGraph& sg,
                                const AlignmentOptions& align_opts,
                                const ScafVertexPair& xy,
                                const UncertainLength& xy_length,
                                InsertQueue& to_insert,
                                ScafVPairDependencyMap& dependencies,
                                const SequenceMap* const sequences = NULL
                               )
  {
    assert(!sg.is_matching_edge(xy));
    const ScafVertex& x = xy.first;
    const ScafVertex& y = xy.second;

    DEBUG5(std::cout << "checking small 3-paths over "<< sg[x].name << "->"<< sg[y].name <<" (len = "<<xy_length<<")"<<std::endl);
    // prepare a predicate for an iterator that ignores matching edges and edges to y or its matched vertex
    const ScafVertex& z = sg.matched_with(y);
    const TargetContainedPredicate<RawScaffoldGraph> avoid_yz_match({y, z}, sg.get_graph(), true, false);
    // for each edge uv of a neighbor u of x such that xy fits in uv (and v != y) add the edge v--y
    for(auto x_incident = sg.get_incident(x, avoid_yz_match); x_incident; ++x_incident){
      const ScafEdge& xu = *x_incident;
      const ScafVertex& u = sg.target(xu);
      assert(u != y);
      assert(u != z);
      support_jump_using(sg, align_opts, x, y, xy_length, u, sg.get_uncertain_length(xu), to_insert, dependencies, sequences);
    }
  }

  void support_jump(const ScaffoldGraph& sg,
                    const AlignmentOptions& align_opts,
                    const ScafEdge& xy,
                    InsertQueue& to_insert,
                    ScafVPairDependencyMap& dependencies,
                    const SequenceMap* const sequences = NULL
                    )
  {
    const UncertainLength xy_length = sg.get_uncertain_length(xy);
    if(xy_length.mean != NO_LENGTH){
      const ScafVertex& x = sg.source(xy);
      const ScafVertex& y = sg.target(xy);
      // call support_jump_directional for both directions
      support_jump_directional(sg, align_opts, ScafVertexPair(x,y), xy_length, to_insert, dependencies, sequences);
      support_jump_directional(sg, align_opts, ScafVertexPair(y,x), xy_length, to_insert, dependencies, sequences);
    }
  }

  //! register new insertions caused by adding edges
  /** whenever we add a new edge to the graph, this new edge might cause new insertions;
   * this function detects these additions by calling support_jump for the newly inserted edge
   */
  void treat_new_edge(const ScaffoldGraph& sg,
                      const AlignmentOptions& align_opts,
                      const ScafEdge& xy,
                      InsertQueue& to_insert,
                      ScafVPairDependencyMap& dependencies,
                      const SequenceMap* const sequences = NULL
                      )
  {
    DEBUG5(std::cout << "treating insertion of edge "<<sg.get_edge_name(xy)<<" ["<<sg[xy].length<<"]"<<std::endl);
    // step 1: support placing contigs in xy itself
    support_jump(sg, align_opts, xy, to_insert, dependencies, sequences);
    // step 2: support placing contigs in xu using xy
    const ScafVertex& x = sg.source(xy);
    const ScafVertex& y = sg.target(xy);
    const int xy_len = sg[xy].length;
    if(xy_len != NO_LENGTH){
      // use an inverted target-contained predicate that disallows matching edges
      const TargetContainedPredicate<RawScaffoldGraph> avoid_xy_match({x, y}, sg.get_graph(), true, false);

      for(auto xu_it = sg.get_incident(x, avoid_xy_match); xu_it; ++xu_it){
        const int xu_length = sg[*xu_it].length;
        const ScafVertex& u = sg.target(*xu_it);
        if(xu_length != NO_LENGTH)
          support_jump_using(sg, align_opts, x, u , xu_length, y, xy_len, to_insert, dependencies, sequences);
      }
      // step 3: support placing contigs in yv using yx
      for(auto yv_it = sg.get_incident(y, avoid_xy_match); yv_it; ++yv_it){
        const int yv_length = sg[*yv_it].length;
        const ScafVertex& v = sg.target(*yv_it);
        if(yv_length != NO_LENGTH)
          support_jump_using(sg, align_opts, y, v, yv_length, x, xy_len, to_insert, dependencies, sequences);
      }
    }// if xy has a length
  }

  //! preprocess the graph to support contig jumps and return the number of newly inserted edges
  /** return the number of added edges and write the dependency map to "dependencies" */
  unsigned preprocess_jumps(ScaffoldGraph& sg,
                            const AlignmentOptions& align_opts,
                            ScafVPairDependencyMap& dependencies,
#ifdef STATISTICS
                            JumpStatistics& jump_stats,
#endif
                            const SequenceMap* const sequences = NULL)
  {
    // extend the graph to support each small contig being placed in appropriate non-contigs
    InsertQueue to_insert;
    ScafVPairDependencyMap local_dependencies;

    for(auto e = sg.get_non_matching_edges(); e; ++e)
      support_jump(sg, align_opts, *e, to_insert, local_dependencies, sequences);

    STAT(VertexPairToInt<RawScaffoldGraph> count_reasons);
    unsigned result = 0;
    while(!to_insert.empty()){
      const auto longest_in_queue = to_insert.top();
      const int vy_length = longest_in_queue.first;
      const ScafVertexPair vy(longest_in_queue.second);
      to_insert.pop();
      const ScafVertex& v = vy.first;
      const ScafVertex& y = vy.second;
      const ScafVertexPair yv(y,v);
      const ScafEdge& yMy = sg.incident_matching_edge(y);
      const ScafEdge& vMv = sg.incident_matching_edge(v);
      const MultiType multi = std::min(sg[yMy].multiplicity, sg[vMv].multiplicity);
      const auto vy_in_sg = sg.add_edge(v, y, ScafEdgeProperty(0, vy_length, multi));
      // if we newly introduced vy into the graph, mark its dependencies and treat it using treat_new_edge()
      if(vy_in_sg.second){
        STAT(count_reasons[vy] = 1);
        ++result;
        DEBUG5(std::cout << sg.get_edge_name(vy) << " is not in the graph, adding with length "<<vy_length<<" & marking as interesting"<<std::endl);
        assert(!local_dependencies[yv].empty() && !local_dependencies[vy].empty());
        // step 1: handle dependencies
        SPLICE_LISTS(dependencies[vy], local_dependencies[vy]);
        SPLICE_LISTS(dependencies[yv], local_dependencies[yv]);
        // step 2: handle insertions caused by the newly inserted edge
        treat_new_edge(sg, align_opts, vy_in_sg.first, to_insert, local_dependencies, sequences);
      } else {
        // if the edge has been there already, only update its length (unless it was in the input graph originally)
        const auto vy_dep_it = dependencies.find(vy);
        if(vy_dep_it != dependencies.cend()){
          DEBUG5(std::cout << sg.get_edge_name(vy) << " has been added previously, updating length to max("<<vy_length<<","<<sg[vy_in_sg.first].length<<")"<<std::endl);
          // keep statistic
          STAT(++count_reasons[vy]);
          // the edge was not originally in the graph, so update its length & dependencies
          // step 1: update length
          int& vy_in_sg_len = sg[vy_in_sg.first].length;
          vy_in_sg_len = std::max(vy_in_sg_len, vy_length);
          // step 2: handle dependencies
          SPLICE_LISTS(vy_dep_it->second, local_dependencies[vy]);
          SPLICE_LISTS(dependencies[yv], local_dependencies[yv]);
        } else DEBUG5(std::cout << sg.get_edge_name(vy) << " is an original edge, so I'm not modifying it"<<std::endl);
      }// if vy is already in sg or not
      local_dependencies.erase(yv);
      local_dependencies.erase(vy);
    }// while there are interesting edges 
    STAT(jump_stats.num_reasons_for_jump.from_pairs(count_reasons));
    return result;
  }// function

}// namespace



