/*
 * preprocess a given graph using the pendant matching rule
*/

/* ----------------------------------------------------------------- */

#ifndef PP_PENDANT_MATCH_HPP
#define PP_PENDANT_MATCH_HPP

#include <list>
#include <vector>

#include "utils/graph_typedefs.hpp"
#include "utils/instance.hpp"

using namespace boost;

struct edge_component_t{
    //enum{ num = 555 };
    size_t num;
    typedef edge_property_tag kind;
} edge_component;


namespace scaffold{
    namespace preprocess {

        // apply the pendant matching rule to a degree-2 vertex u (with non-matching partner w)
        // return true <=> edges were deleted from g
        bool pp_matching_pendant(Instance& I, const ScafVertex& u, const ScafVertex& w, Solution<>* S = NULL){
          bool change = false;
          const RawScaffoldGraph& g = I.get_graph();
          assert(boost::degree(u, g) == 2);
          assert(I.get_matching().at(u) != w);

          // Step 1: get edges ux incident with u such that x != v   and   keep track of the heaviest edge uy with deg(y)=2
          double max_weight = 0.0;
          ScafOEdgeIter max_edge;
          for(ScafOEdgeIterRange r = boost::out_edges(w, g); r.first != r.second; ++r.first){
            const ScafEdge& uv = *r.first;
            const ScafVertex& v = boost::target(uv, g);
            if((v != w) && (boost::degree(w, g) == 2)){
              const unsigned weight = g[uv].weight;
              if(weight > max_weight) {
                max_weight = weight;
                max_edge = r.first;
              }// if
            }// if
          }// for
          const ScafVertex& max_vertex = boost::target(*max_edge, g);

          // Step 2: delete all edges ux with weight at most w(uy) such that x != y (unless no vertex y with deg-2 was found!)
          if(max_weight != 0.0){
            //std::cout << "largest weight at "<<*uy<<": "<<max_weight<<std::endl;
            for(ScafOEdgeIterRange r = boost::out_edges(w, g); r.first != r.second;){
              const ScafEdge& ux = *r.first;
              const ScafVertex& x = boost::target(ux, g);

              if(x != max_vertex){
                const unsigned weight = g[ux].weight;
                if(weight <= max_weight){
                  // remove *ei (avoid invalidating ei by copying to tmp, advancing tmp and then deleting the edge)
                  DEBUG3(std::cout << "PP: removing "<<I.get_edge_name(ux)<<" of weight "<< g[ux].weight<<std::endl);
                  ScafOEdgeIter tmp = r.first; ++r.first;
                  I.delete_edge(*tmp, S);
                  change = true;
                } else ++r.first;
              } else ++r.first;
            }// for
          } // else std::cout << "no vertex of deg 2 adjacent to " <<u << std::endl;
          return change;
        } // function

        // preprocess g
        // return true <=> edges were deleted from g
        // NOTE: requires edge indices to be set up before
        bool pp_matching_pendant(Instance& I, Solution<>* S = NULL){
          const RawScaffoldGraph& g = I.get_graph();
          const ScafMatching& matching = I.get_matching();
          bool change = false;

          // Step 1: apply the edge version to each non-matching edge uw with deg(u)==2 and uw is not on an alternating cycle
          for(ScafVIterRange r = boost::vertices(g); r.first != r.second; ++r.first) if(boost::degree(*r.first, g) == 2){
              // Step 1a: find the unique non-matching partner w of u
              const ScafVertex& u = *r.first;
              const ScafAdjIter tmp = boost::adjacent_vertices(u, g).first;
              const ScafVertex w = (matching.at(u) == *tmp) ? *std::next(tmp) : *tmp;
              DEBUG3(std::cout << "investigating "<<g[u].name<<" unmatched with "<< g[w].name<<std::endl);

              // Step 1b: find out whether uw is on an alternating cycle
              const bool apply_reduction = !I.is_on_alternating_cycle(u, w);
              DEBUG3(std::cout << "found that ("<<g[u].name<<", "<<g[w].name<<") is "<< (apply_reduction?"not":"   ")<<" on an alteranting cycle (or we're not looking for any cycles)"<<std::endl);

              // Step 1c: apply pp_matching_pendant to uw if it's a bridge
              if(apply_reduction)
                if(pp_matching_pendant(I, u, w, S))
                  change = true;
            } // for
          return change;
        } // function
    } // namespace
} // namespace

#endif
