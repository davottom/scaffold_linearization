

/** \file branching_vector.hpp
 * computation of the branching number of a branching vector
 * using code CC by Joseph, Chuang-Chieh Lin (lincc@cs.ccu.edu.tw or josephcclin@gmail.com)
 */

#pragma once

#include <vector>
#include <cmath>

#define BRANCHING_NUMBER_PRECISION 5

namespace scaffolding{ namespace solv {
  
  //! a branching vector is a vector of numbers corresponding to the "progress" in each branch
  typedef std::vector<unsigned> BranchingVector;

  // branching number code by Joseph, Chuang-Chieh Lin (lincc@cs.ccu.edu.tw or josephcclin@gmail.com)
  inline float CH_POLY(const BranchingVector& BV, const float var){
    float s = 0;
    for(size_t i : BV) s += pow(var, i);
    return 1-s;
  }

  //! compute the branching number of a vector of parameter decreases
  float branching_number(const BranchingVector& BV){
    float temp = 0;
    float poly_result;
    for(byte d = 1; d <= BRANCHING_NUMBER_PRECISION; ++d) { 
      poly_result = 1;
      while (poly_result > 0) {
        temp += pow(0.1, d);
        poly_result = CH_POLY(BV, temp);
      } // while
      temp -= pow(0.1, d);
    } // for
    return (1/temp);
  } // function
}} // namespace

