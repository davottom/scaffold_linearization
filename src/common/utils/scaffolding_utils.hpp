#pragma once

#include "utils/alignment_options.hpp"
#include "utils/graph_typedefs.hpp"
#include "utils/scaffolding_typedefs.hpp"
#include "utils/scaffold_graph.hpp"
#include "utils/instance.hpp"
#include "utils/oriented_sequences.hpp"

using namespace boost;

namespace scaffold{

  // remove all multiplicities from edges
  void remove_multiplicities(ScaffoldGraph& sg)
  {
    for(auto range = boost::edges(sg.get_graph()); range.first != range.second; ++range.first)
      sg[*range.first].multiplicity = 1;
  }


  // derive multiplicities for non-contig edges from the multiplicities of incident contig edges
  void fix_non_matching_multiplicities(ScaffoldGraph& sg)
  {
    const RawScaffoldGraph& g = sg.get_graph();
    for(auto range = boost::edges(g); range.first != range.second; ++range.first){
      const ScafEdge& uv = *range.first;
      if(!sg[uv].is_matching_edge()){
        const ScafVertex& u = boost::source(uv, g);
        const ScafVertex& v = boost::target(uv, g);
        const ScafEdge ux = sg.incident_matching_edge(u);
        const ScafEdge vy = sg.incident_matching_edge(v);
        sg[uv].multiplicity = std::min(sg[ux].multiplicity, sg[vy].multiplicity);

        DEBUG5(std::cout << "fixing multiplicity of "<<sg.get_edge_name(uv)<<" (len "<<sg[uv].length<<") to the min of "<<sg.get_edge_name(ux)<<" ("<<sg[ux].multiplicity<<") & "<<sg.get_edge_name(vy)<<" ("<<sg[vy].multiplicity<<")"<<std::endl);
      }
    }// for all non-contig edges of sg
  }

  // cut all non-contigs whose weight is below a certain threshold, but don't decrease the degree below min_deg
  void cut_off_threshold(ScaffoldGraph& sg, const unsigned threshold, const unsigned min_degree = 0)
  {
    if(!threshold) return;
    const RawScaffoldGraph& g = sg.get_graph();
    for(ScafEdgeIterRange r = boost::edges(g); r.first != r.second;){
      const ScafEdge& uv = *r.first;
      const ScafEdgeProperty& uv_info = g[uv];
      if(!uv_info.is_matching_edge()){
        const ScafVertex& u = boost::source(uv, g);
        const ScafVertex& v = boost::target(uv, g);
        const unsigned uv_weight = g[uv].weight;
        if(uv_weight < threshold){
          if((min_degree < 2) || ((boost::degree(u, g) > min_degree) && (boost::degree(v, g) > min_degree))){
            const ScafEdgeIter to_del = r.first;
            ++r.first;
            sg.delete_edge(*to_del);
          } else ++r.first;
        } else ++r.first;
      } else ++r.first;
    }// for
  }

  // divide each weight by the # of times this edge can be in any solution
  // NOTE: assumes that non-matching edges have multiplicities!
  // if this is not the case, you should call fix_non_matching_multiplicities before calling this function!
  void scale_weights(ScaffoldGraph& sg)
  {
    const RawScaffoldGraph& g = sg.get_graph();
    for(auto er = boost::edges(g); er.first != er.second; ++er.first) {
      const ScafEdge& uv = *er.first;
      ScafEdgeProperty& uv_info = sg[uv];
      if(!uv_info.is_matching_edge()){
        const unsigned uv_multi = uv_info.multiplicity;
        DEBUG5(std::cout << "scaling "<<sg.get_edge_name(uv)<<" of multiplicity "<<uv_multi<<std::endl);
#warning TODO: either use float as weight or scale up everything by the max multiplicity?
        if(uv_multi != 1) uv_info.weight /= uv_multi;
      }// if uv is not a matching edge
    }// for all edges
  }// function

  // set up vertex names as integers by their indices
  void setup_vertex_names(RawScaffoldGraph& g)
  {
    for(ScafVIterRange r = boost::vertices(g); r.first != r.second; ++r.first) {
      ScafVertexProperty& v_prop = g[*r.first];
      if(v_prop.name.empty()) v_prop.index_to_name();
    }
  }


  // clear all non-matching edges incident to a given vertex
  void clear_nonmatching(const ScafVertex& u, RawScaffoldGraph& g)
  {
    for(auto e_range = boost::out_edges(u, g); e_range.first != e_range.second;){
      const ScafEdge e = *e_range.first;
      ++e_range.first;
      if(!g[e].is_matching_edge())
        boost::remove_edge(e, g);

    }// for all vertices
  }// function

  // return whether u is the start of a contig
  bool is_sequence_start(const ScafVertex& u, const RawScaffoldGraph& g){
    // u is a contig start if its degree is 1 or at least 3
    if(boost::degree(u, g) == 2) {
      // if the degree of u is 2, then it depends on the multiplicity of the incident edges
      auto e_iter = boost::out_edges(u, g).first;
      const ScafEdge& e1 = *(e_iter++);
      const ScafEdge& e2 = *e_iter;
      return (g[e1].multiplicity != g[e2].multiplicity);
    } else return true;
  }

  // forward declaration, see details below
  void get_alternating_paths_of_max_length(const ScaffoldGraph& sg,
                                          const ScafVertex& u,
                                          const unsigned max_length,
                                          std::list<AlternatingPath>& result,
                                          const bool start_with_matched,
                                          ScafVertexSet* const _forbidden = NULL);

  // get alternating paths of length max_length starting with the edge uv & avoiding the forbidden vertices
  // (see the next function for details)
  void get_alternating_paths_of_max_length(const ScaffoldGraph& sg,
                                          const ScafEdge& uv,
                                          const ScafVertex& v,
                                          const unsigned max_length,
                                          std::list<AlternatingPath>& result,
                                          const bool start_with_matched,
                                          ScafVertexSet* const forbidden)
  {
    assert(forbidden);
    if(!contains(*forbidden, v)){
      std::list<AlternatingPath> paths_from_v;
      get_alternating_paths_of_max_length(sg, v, max_length, paths_from_v, start_with_matched, forbidden);

      // prepend uv to all paths from v (create a single empty path if there are none)
      if(paths_from_v.empty()) paths_from_v.emplace_back(AlternatingPath());
      for(auto& p: paths_from_v) p.emplace_front(uv);

      // add the resulting paths to the result
      SPLICE_LISTS(result, paths_from_v);
    }
  }

  // compute all alternating paths starting in u such that the total contig length does not exceed max_length
  // if start_with_matched is set, consider only paths starting with a matched edge
  // do not consider paths going to vertices in "forbidden"
  void get_alternating_paths_of_max_length(const ScaffoldGraph& sg,
                                          const ScafVertex& u,
                                          const unsigned max_length,
                                          std::list<AlternatingPath>& result,
                                          const bool start_with_matched,
                                          ScafVertexSet* const _forbidden)
  {
    ScafVertexSet* const forbidden = (_forbidden ? _forbidden : new ScafVertexSet());
    forbidden->emplace(u);
    const ScafVertex& u_match = sg.matched_with(u);
    if(start_with_matched){
      const ScafEdge uv = sg.find_edge(u, u_match).first;
      const ScafEdgeProperty& uv_info = sg[uv];
      const unsigned uv_length = ( start_with_matched ? uv_info.length : 0);
      if(uv_length <= max_length)
        get_alternating_paths_of_max_length(sg, uv, u_match, max_length - uv_length, result, false, forbidden);
    } else {
      const RawScaffoldGraph& g = sg.get_graph();
      for(auto range = boost::out_edges(u, g); range.first != range.second; ++range.first){
        const ScafEdge uv = *range.first;
        const ScafVertex& v = boost::target(uv, g);
        if(v != u_match)
          get_alternating_paths_of_max_length(sg, uv, v, max_length, result, true, forbidden);
      }// for all incident with u
    }
    // clean up the forbidden set if we created it
    if(!_forbidden) delete forbidden;
  }// function


  // turn a path or cycle into an OrientedSequence; remove endpoints from keep_updated; return the multiplicity of the path
  unsigned path_to_sequence(ScaffoldGraph& sg,
                        const ScafVertex& x,
                        const AlignmentOptions& align_opts,
                        const SequenceMap& sequences,
                        NamedOrientedSequence& out,
                        ScafVertexSet* keep_updated = NULL,
                        const unsigned NO_LENGTH_num_N = 0)
  {
    DEBUG4(std::cout << "writing down sequence starting at "<<sg[x].name<<": "<<std::endl);
    assert(sg.degree(x) > 0);

    // grow a name & sequence along the path/cycle
    std::string& current_name = out.first;
    std::string& current_seq = out.second.sequence;
    out.second.start_vertex = sg[x].name;

    // move along the path & collect the sequences into the output map
    if(keep_updated) keep_updated->erase(x);
    ScafVertex u = x;
    // start with the matching edge incident with u
    ScafEdge uv = sg.incident_matching_edge(u);
    const unsigned path_multi = sg[uv].multiplicity;
    while(true){
      // step 1: find the next vertex along uv
      const ScafVertex v = sg.target(uv);
      std::string uv_contig_name = sg[uv].contig_name;

      // step 2: write down the sequence of uv
      const OrientedSequence& os = sequences.at(uv_contig_name);
      //std::cout << "mark 4 accessed "<<uv_contig_name<<std::endl;
      DEBUG4(std::cout << uv_contig_name << " (@"<<os.start_vertex<<") "<<std::flush);
      if(os.start_vertex != sg[u].name){
        assert(os.start_vertex == sg[v].name);
        current_seq += named_reverse_complement(os.sequence, uv_contig_name, align_opts.uracil);
      } else current_seq += os.sequence;
      current_name += uv_contig_name;

      // step 4: advance along the path, writing down 'N's
      const unsigned v_deg = sg.degree(v);
      sg.delete_vertex(u, false, false);
      assert(v_deg <= 2);
      if(v_deg == 2){
        const ScafEdge vw = *(sg.get_incident_non_matching(v));
        const int vw_length = sg[vw].length;
        assert(sg[vw].multiplicity == path_multi);

        // add 'N's unless vw has NO_LENGTH
        if(vw_length != NO_LENGTH){
          assert(sg[vw].length >= 0);
          current_seq += std::string(sg[vw].length, 'N');
        } else current_seq += std::string(align_opts.NO_LENGTH_num_N, 'N');

        // advance along the path
        u = sg.target(vw);
        out.first += '+';
        uv = sg.incident_matching_edge(u);
        assert(sg[uv].multiplicity == path_multi);
      } else {
        sg.delete_vertex(v, false, false);
        if(keep_updated) keep_updated->erase(v);
        break;
      }
    }// loop
    DEBUG4(std::cout << std::endl);
    return path_multi;
  }

  // turn a simple solution graph (only paths & cycles) into a collection of named sequences with a multiplicity counter
  // NOTE: this function destroys the given ScaffoldGraph
  void simple_solution_to_sequences(ScaffoldGraph& sg,
                                    const AlignmentOptions& align_opts,
                                    const SequenceMap& sequences,
                                    MultiSequenceMap& out,
                                    const unsigned NO_LENGTH_num_N = 0)
  {
    const RawScaffoldGraph& g = sg.get_graph();
    ScafVertexSet deg1;
    for(auto range = boost::vertices(g); range.first != range.second; ++range.first)
      if(sg.degree(*range.first) == 1)
        deg1.insert(*range.first);

    // handle paths first
    while(!deg1.empty()){
      NamedOrientedSequence current;
      const ScafVertex u = *deg1.begin();
      const unsigned path_multi = path_to_sequence(sg, u, align_opts, sequences, current, &deg1, NO_LENGTH_num_N);
      // register the name & sequence in the output map, making sure that we're not overwriting a sequence
      while(!out.emplace(std::piecewise_construct, std::make_tuple(current.first), std::make_tuple(current.second, path_multi)).second)
        current.first += '\'';
    }// while there are degree-one vertices
    while(sg.num_edges()){
      NamedOrientedSequence current;
      const ScafVertex u = *(boost::vertices(sg.get_graph()).first);
      if(sg.degree(u) > 0){
        const unsigned path_multi = path_to_sequence(sg, u, align_opts, sequences, current, NULL, NO_LENGTH_num_N);
        current.first += CYCLIC_SEQUENCE_INDICATOR;
        // register the name & sequence in the output map, making sure that we're not overwriting a sequence
        while(!out.emplace(std::piecewise_construct, std::make_tuple(current.first), std::make_tuple(current.second, path_multi)).second)
          current.first += '\'';
      } else sg.delete_vertex(u, false, false);
    }// while there are degree-one vertices

  }// function


} // namespace
