
#pragma once

#include <iostream>
#include <list>
#include <initializer_list>
#include <boost/graph/properties.hpp>

#include "utils/normal_distribution.hpp"
#include "utils/string_utils.hpp"
#include "utils/graph_typedefs.hpp"
#include "utils/graph_infos.hpp"
#include "utils/adv_iters.hpp"

#define NO_WEIGHT UINT_MAX
#define NO_LENGTH INT_MIN

#define WeightType uint32_t
#define LengthType int32_t
#define MultiType uint16_t
#define IndexType uint32_t
#define BoolType byte

namespace scaffold {

  typedef NormalDistribution<int> UncertainLength;

  struct ScafVertexProperty {
    IndexType index;
    std::string name;

    //! indicate the sum of the incident multiplicities of non-ambiguous paths after applying the reduction rules
    int multiplicity;

    /** indicate if the function get_inner_mult_once has already been called for the vertex.
     *  Parameter used in the tree decomposition algorithm for the linearization */
    bool multiplicity_call;

    ScafVertexProperty(const unsigned _index = 0): index(_index), multiplicity(0), multiplicity_call(false) {}
    // copy the vertex index as its new name
    void index_to_name()
    {
      name = std::to_string(index);
    }

    friend std::ostream& operator<<(std::ostream& os, const ScafVertexProperty& info)
    {
      if(info.name.empty())
        return os << info.index;
      else
        return os << info.name;
    }
  };
  struct ScafEdgeProperty {
    WeightType weight = NO_WEIGHT;
    LengthType length = NO_LENGTH;
    MultiType multiplicity = 1;
    std::string contig_name;
    bool removed =false; //for greedy algorithm

    ScafEdgeProperty() {}
    ScafEdgeProperty(const unsigned _weight, const int _length, const unsigned _multi, const std::string& _contig_name = ""):
      weight(_weight), length(_length), multiplicity(_multi), contig_name(_contig_name) {}
    ScafEdgeProperty(const ScafEdgeProperty& ep):
      weight(ep.weight), length(ep.length), multiplicity(ep.multiplicity), contig_name(ep.contig_name), removed(ep.removed) {}

    // non-positive lengths are considered certain
    bool is_certain() const
    {
      return (length <= 0);
    }
    // an edge is considered a matching edge if it does not have a weight assigned
    bool is_matching_edge() const 
    {
      return (weight == NO_WEIGHT);
    }

    friend std::ostream& operator<<(std::ostream& os, const ScafEdgeProperty& info)
    {
      if(info.is_matching_edge())
        return os << "l: " << info.length << " m: " << info.multiplicity << " c: '" << info.contig_name<<"'";
      else
        return os << "l: " << info.length << " m: " << info.multiplicity << "w: "<< info.weight;
    }

  };
  struct ScafGraphProperty {
    unsigned insert_size;
    unsigned standard_deviation;

    std::pair<int,float> get_distribution_parameters() const
    {
      return {insert_size, standard_deviation};
    }

  };


  typedef boost::adjacency_list<
      boost::hash_setS, // OutEdgeList
      boost::hash_setS,     // VertexList
      boost::undirectedS, // (bi)directed ?
      ScafVertexProperty,
      ScafEdgeProperty,
      ScafGraphProperty
    > RawScaffoldGraph;

  typedef Vertex<RawScaffoldGraph> ScafVertex;
  typedef VertexSet<RawScaffoldGraph> ScafVertexSet;
  typedef VertexPair<RawScaffoldGraph> ScafVertexPair;
  typedef UnorderedVertexPair<RawScaffoldGraph> UnorderedScafVPair;
  typedef VertexPairList<RawScaffoldGraph> ScafVertexPairList;
  typedef VertexPairSet<RawScaffoldGraph> ScafVertexPairSet;
  typedef VPairDependencyMap<RawScaffoldGraph> ScafVPairDependencyMap;
  typedef VertexIter<RawScaffoldGraph> ScafVIter;
  typedef VertexIterRange<RawScaffoldGraph> ScafVIterRange;
  typedef AdjIter<RawScaffoldGraph> ScafAdjIter;
  typedef AdjIterRange<RawScaffoldGraph> ScafAdjIterRange;

  typedef typename boost::property_map<RawScaffoldGraph, unsigned ScafVertexProperty::*>::type ScafVIndexMap;
  typedef typename boost::property_map<RawScaffoldGraph, unsigned ScafVertexProperty::*>::const_type cScafVIndexMap;
  typedef typename boost::property_map<RawScaffoldGraph, unsigned ScafEdgeProperty::*>::type ScafEIndexMap;
  typedef typename boost::property_map<RawScaffoldGraph, unsigned ScafEdgeProperty::*>::const_type cScafEIndexMap;

  typedef Edge<RawScaffoldGraph> ScafEdge;
  typedef EdgeSet<RawScaffoldGraph> ScafEdgeSet;
  typedef EdgeList<RawScaffoldGraph> ScafEdgeList;
  typedef EdgeIter<RawScaffoldGraph> ScafEdgeIter;
  typedef EdgeIterRange<RawScaffoldGraph> ScafEdgeIterRange;
  typedef OEdgeIter<RawScaffoldGraph> ScafOEdgeIter;
  typedef OEdgeIterRange<RawScaffoldGraph> ScafOEdgeIterRange;
  typedef IEdgeIter<RawScaffoldGraph> ScafIEdgeIter;
  typedef IEdgeIterRange<RawScaffoldGraph> ScafIEdgeIterRange;

  typedef Matching<RawScaffoldGraph> ScafMatching;
  typedef std::list<ScafEdge> AlternatingPath;
  typedef boost::unordered_map<ScafVertexPair, ScafVertexPairSet> ScafJumpMap;
  typedef boost::unordered_map<ScafVertexPair, int> ScafVPairWeights;

  // a contig jump is a subgraph consisting of a short (shorter than insert size) contig and a 3-path of non-contig edges,
  // together forming a 4-cycle; this occurs if reads (corresponding to the middle edge of the 3-path) "span over" the short contig
  struct contig_jump{
    const ScafEdge contig;
    std::list<ScafEdge> path;

    contig_jump(const ScafEdge& _contig, const std::initializer_list<ScafEdge>& L):
      contig(_contig),
      path(L)
    {}

  };


} // namespace

