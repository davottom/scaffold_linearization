

/** \file graph_typedefs.hpp
 * collection of abbreviations in context of graphs, for example,
 * "Vertex<Graph>" is much more readable than "typename boost::graph_traits<Graph>::vertex_descriptor"
 */

#ifndef GRAPH_TYPEDEFS_HPP
#define GRAPH_TYPEDEFS_HPP

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>

#include <iostream>
#include <queue>
#include <string>

#include "utils/utils.hpp"
#include "utils/unordered_pair.hpp"

// translate an edge given: the old edge e, the old graph g, the new graph h, and a translate map t from old to new
#define translate_edge(e,g,h,t) (boost::edge(t.at(boost::source(e, g)), t.at(boost::target(e, g)), h).first)

//! a type to indicate direction for traversing directed edges
typedef enum {dir_rev, dir_fwd, dir_fwdrev} Direction;

using VertexName = std::string;
using EdgeName = std::pair<VertexName, VertexName>;


#define NONAME "%"

namespace std{
  string to_string(const EdgeName& en){
    return "(" + en.first + "," + en.second + ")";
  }

}

//! a struct for a pair of unsigneds, representing a number of paths and a number of cycles
/** this could have been a simple std::pair, but bla.p and bla.c is more readable than bla.first and bla.second */
struct PathsAndCycles
{
  unsigned p;
  unsigned c;

  PathsAndCycles(const unsigned _p = 0, const unsigned _c = 0): p(_p), c(_c) {}

  operator size_t() const
  {
    size_t hash = p;
    boost::hash_combine(hash, c);
    return hash;
  }

  PathsAndCycles operator+(const PathsAndCycles& pc) const
  {
    return {p + pc.p, c + pc.c};
  }
  PathsAndCycles& operator+=(const PathsAndCycles& pc)
  {
    p += pc.p;
    c += pc.c;
    return *this;
  }
  PathsAndCycles operator-(const PathsAndCycles& pc) const
  {
    return {p - pc.p, c - pc.c};
  }
  PathsAndCycles& operator-=(const PathsAndCycles& pc)
  {
    p -= pc.p;
    c -= pc.c;
    return *this;
  }

  bool operator<=(const PathsAndCycles& pc) const
  {
    return (p <= pc.p) && (c <= pc.c);
  }
  bool operator<(const PathsAndCycles& pc) const
  {
    return (p < pc.p) && (c < pc.c);
  }
  bool operator==(const PathsAndCycles& pc) const
  {
    return (p == pc.p) && (c == pc.c);
  }
  bool operator!=(const PathsAndCycles& pc) const
  {
    return !operator==(pc);
  }

  friend std::ostream& operator<<(std::ostream& os, const PathsAndCycles& pc)
  {
    return os << "("<<pc.p<<","<<pc.c<<")";
  }
};
//! hashing for PathsAndCycles
size_t hash_value(const PathsAndCycles& pc)
{
  return pc;
}

template<class Graph>
using Vertex = typename boost::graph_traits<Graph>::vertex_descriptor;
template<class Graph>
using VertexProperty = typename Graph::vertex_bundled;
template<class Graph>
using VertexPair = typename std::pair<Vertex<Graph>, Vertex<Graph> >;
template<class Graph>
using UnorderedVertexPair = typename std::unordered_pair<Vertex<Graph>, Vertex<Graph> >;
template<class Graph>
using VertexPairSet = typename boost::unordered_set<VertexPair<Graph> >;
template<class Graph>
using VertexIter = typename boost::graph_traits<Graph>::vertex_iterator;
//! a pair of vertex iterators, as is returned by boost::vertices() for example
template<class Graph>
using VertexIterRange = typename std::pair<VertexIter<Graph>, VertexIter<Graph> >;
template<class Graph>
using AdjIter = typename boost::graph_traits<Graph>::adjacency_iterator;
//! a pair of adjacency iterators, as is returned by boost::adjacent_vertices() for example
template<class Graph>
using AdjIterRange = typename std::pair<AdjIter<Graph>, AdjIter<Graph> >;
//! a map of vertices to their indices which disallows changing the indices
template<class Graph>
using cVertexIndexMap = typename boost::property_map<Graph, unsigned VertexProperty<Graph>::*>::const_type;
//! a map of vertices to their indices which allows changing the indices
template<class Graph>
using VertexIndexMap = typename boost::property_map<Graph, unsigned VertexProperty<Graph>::*>::type;

template<class Graph>
using Edge = typename boost::graph_traits<Graph>::edge_descriptor;
template<class Graph>
using EdgeProperty = typename Graph::edge_bundled;
template<class Graph>
using EdgeIter = typename boost::graph_traits<Graph>::edge_iterator;
//! a pair of edge iterators, as is returned by boost::edges() for example
template<class Graph>
using EdgeIterRange = typename std::pair<EdgeIter<Graph>, EdgeIter<Graph> >;
template<class Graph>
using OEdgeIter = typename boost::graph_traits<Graph>::out_edge_iterator;
//! a pair of out-edge iterators, as is returned by boost::out_edges() for example
template<class Graph>
using OEdgeIterRange = typename std::pair<OEdgeIter<Graph>, OEdgeIter<Graph> >;
template<class Graph>
using IEdgeIter = typename boost::graph_traits<Graph>::in_edge_iterator;
//! a pair of in-edge iterators, as is returned by boost::in_edges() for example
template<class Graph>
using IEdgeIterRange = typename std::pair<IEdgeIter<Graph>, IEdgeIter<Graph> >;

//! a map of vertices of Graph1 to vertices of Graph2
/** useful for translating vertices of some graph to vertices of the next,
 * for example, after making a copy g' of a graph g, we can access the representation of any vertex of g in g'
 */
template<class Graph1, class Graph2>
using VTranslateMap = typename boost::unordered_map<Vertex<Graph1>, Vertex<Graph2> >;
//! a matching is a mapping between vertices of the same graph
template<class Graph>
using Matching = VTranslateMap<Graph, Graph>;

//! a vertex bundled wtih a number, for example its degree
template<class Graph>
struct VertexAndNum : public std::pair<Vertex<Graph>, unsigned>
{
  using std::pair<Vertex<Graph>, unsigned>::second;
  using std::pair<Vertex<Graph>, unsigned>::pair;

  bool operator<(const VertexAndNum& vn) const
  {
    return second < vn.second;
  }
};
//! a synonym for VertexAndNum to improve readability
template<class Graph>
using VertexAndDegree = VertexAndNum<Graph>;

//! a map of edges (represented as vertex pairs) to lists of edges (represented as vertex pairs)
/** this is to store dependencies created by the jump-preprocessing (see preprocess_jumps()) */
template<class Graph>
using VPairDependencyMap = typename boost::unordered_map<VertexPair<Graph>, std::list<VertexPair<Graph> > >;


//! hashing for (directed) edges
/** for directed edges, (uv & vu get different hash values), set Pair = std::pair<VertexName, VertexName> (see DirectedEdgeHasher)
 * for undirected edges (uv & vu get the same hash value) set Pair = std::unordered_pair<VertexName, VertexName>
 * (see unordered_pair.hpp)
 */
template <class Graph, class Pair = std::unordered_pair<VertexName, VertexName> >
struct EdgeHasher
{
  const Graph& g;

  EdgeHasher(const Graph& _g) : g(_g) {}

  size_t operator()(const Edge<Graph>& e) const {
    const Vertex<Graph>& u = boost::source(e, g);
    const Vertex<Graph>& v = boost::target(e, g);
    const std::unordered_pair<VertexName, VertexName> name_pair(g[u].name, g[v].name);
    // use the pair hasher in utils/utils.hpp
    return hash_value(name_pair);
  }
};

//! more readable hashing for directed edges
template <class Graph>
using DirectedEdgeHasher = EdgeHasher<Graph, std::pair<VertexName, VertexName> >;

//! a comparator based on vertex degree (to find the min/max degree vertex)
/** to prefer higher degrees instead of lower, set Compare = std::greater<unsigned> */
template <class Graph, class Compare = std::less<unsigned> >
struct degree_cmp{
  const Graph& g;
  const Compare compare;

  degree_cmp(const Graph& _g, const Compare& _compare = Compare()): g(_g), compare(_compare) {}

  bool operator()(const Vertex<Graph>& u, const Vertex<Graph>& v) const
  {
    return compare(boost::degree(u, g), boost::degree(v, g));
  }
};


template<class Graph>
using EdgeList   = std::list<Edge<Graph> >;
template<class Graph>
using EdgeSet   = boost::unordered_set<Edge<Graph>, EdgeHasher<Graph> >;
template<class Graph>
using DirectedEdgeSet   = boost::unordered_set<Edge<Graph>, DirectedEdgeHasher<Graph> >;
template<class Graph>
using VertexToInt = boost::unordered_map<Vertex<Graph>, unsigned>;
template<class Graph>
using VertexPairToInt = boost::unordered_map<VertexPair<Graph>, unsigned>;
template<class Graph>
using VertexSet   = boost::unordered_set<Vertex<Graph> >;
template<class Graph>
using VertexList   = std::list<Vertex<Graph> >;
template<class Graph>
using VertexPairList   = std::list<VertexPair<Graph> >;
template<class Graph>
using VertexQueue   = std::queue<Vertex<Graph> >;
template<class Graph>
using ComponentMap = VertexToInt<Graph>;
//! a map of bridges (represented as unordered pairs of vertices) to unsigneds, indicating the score of that bridge
template<class Graph, class EdgeClass = UnorderedVertexPair<Graph> >
using BridgeMap = boost::unordered_map<EdgeClass, unsigned>;
template<class Graph, class EdgeClass = UnorderedVertexPair<Graph> >
using BridgeIter = typename BridgeMap<Graph, EdgeClass>::iterator;
template<class Graph, class EdgeClass = UnorderedVertexPair<Graph> >
using cBridgeIter = typename BridgeMap<Graph, EdgeClass>::const_iterator;

namespace scaffold {
  //! get the name of a vertex
  template<class Graph>
  const VertexName get_name(const Vertex<Graph>& u, const Graph& g)
  {
    return g[u].name;
  }
  //! get the name of a pair of vertices
  template<class Graph>
  const EdgeName get_name(const VertexPair<Graph>& uv, const Graph& g)
  {
    return EdgeName(g[uv.first].name, g[uv.second].name);
  }
  //! get the name of an edge
  template<class Graph>
  const EdgeName get_name(const Edge<Graph>& e, const Graph& g)
  {
    const Vertex<Graph> u = boost::source(e, g);
    const Vertex<Graph> v = boost::target(e, g);
    return EdgeName(g[u].name, g[v].name);
  }
  //! output an edge name
  std::ostream& operator<<(std::ostream& os, const EdgeName& en){
    return os << std::to_string(en);
  }

  //! output container of vertex/vertex pair/edge
  template<class Graph, typename Stuff = VertexSet<Graph> >
  using StuffAndGraph = std::pair<const Stuff&, const Graph&>;
  template<class Graph, typename Stuff = VertexSet<Graph> >
  std::ostream& operator<<(std::ostream& os, const StuffAndGraph<Graph, Stuff>& to_output){
    const Graph& g = to_output.second;
    for(const auto& u: to_output.first) os << get_name(u, g) << " ";
    return os;
  }
} // namespace

#endif
