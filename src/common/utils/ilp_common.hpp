
#pragma once


#define is_true(x) (lround(x))

// if LAZY_ADD_ALL_CYCLES is defined, then all cycles in the solution returned by CPLEX are added by the lazy callback
// otherwise, just the first cycle is added
#define LAZY_ADD_ALL_CYCLES

#include <iostream>
#include <sstream>
#include <boost/bimap.hpp>
#include <boost/bimap/unordered_set_of.hpp>

#include <ilcplex/ilocplex.h>

#include "utils/utils.hpp"
#include "utils/graph_typedefs.hpp"
#include "utils/instance.hpp"
#include "utils/stats.hpp"



namespace scaffold { 

  // a mapping of vertices to variables
  using VertexVarMap = boost::unordered_map<ScafVertex, IloNumVar>;
  using VertexVarIter = typename VertexVarMap::iterator;
  using VertexAndVar = typename VertexVarMap::value_type;

  // a mapping of vertices to expressions
  using VertexExpMap = boost::unordered_map<ScafVertex, IloExpr>;
  using VertexExpIter = typename VertexExpMap::iterator;
  using VertexAndExp = typename VertexExpMap::value_type;

  // a mapping of edges to variables
  using EdgeVarMap = boost::unordered_map<ScafVertexPair, IloNumVar>;
  using EdgeVarIter = typename EdgeVarMap::iterator;
  using EdgeVarCIter = typename EdgeVarMap::const_iterator;
  using EdgeAndVar = typename EdgeVarMap::value_type;

  // a mapping of edges to expressions
  using EdgeExpMap = boost::unordered_map<ScafVertexPair, IloExpr>;
  using EdgeExpIter = typename EdgeExpMap::iterator;
  using EdgeExpCIter = typename EdgeExpMap::const_iterator;
  using EdgeAndExp = typename EdgeExpMap::value_type;

  // a contig-jump network manages contigs that can be jumped by reads due to their small length
  // each directed edge uv is assigned an EdgeVarMap for each time it might occur in the solution (multiplicity of uv)
  using ContigJumpVars = boost::unordered_map<ScafVertexPair, std::list<EdgeVarMap> >;


  struct var_collection{
    const Instance& I;
    EdgeVarMap times_used, times_used_undir, contig_jumps;
    VertexVarMap from_source, to_path_sink, to_cycle_sink;

    var_collection(const Instance& _I): I(_I) {}

    // return the number of times that an arc uv has been jumped in a solution
    unsigned num_jumped(const ScafVertexPair& uv, const IloCplex& cplex) const
    {
      const auto jump_uv = contig_jumps.find(uv);
      return (jump_uv != contig_jumps.cend()) ? lround(cplex.getValue(jump_uv->second)) : 0;
    }

    // return number of paths in a solution
    unsigned num_paths(const IloCplex& cplex) const
    {
      unsigned result = 0;
      for(const auto &u : to_path_sink) result += lround(cplex.getValue(u.second));
      return result;
    }

    // return number of paths in a solution
    unsigned num_cycles(const IloCplex& cplex) const
    {
      unsigned result = 0;
      for(const auto &u : to_cycle_sink) result += lround(cplex.getValue(u.second));
      return result;
    }

    // teach a model to respect jump dependecies
    void respect_jump_dependencies(IloModel& model, const IloEnv& env, const ScafVPairDependencyMap& dependencies) const
    {
      // the dependency map maps each VertexPair p to a list of VertexPairs on which they depend, that is,
      // p cannot be taken more times than the sum of its dependencies
      IloRangeArray c(env);
      DEBUG5(std::cout << "adding contig-jump-shortcut constraints: if we take an added shortcut, we should also take its dependency"<<std::endl);
      for(const auto& dlist: dependencies){
        const ScafVertexPair& uv = dlist.first;
        const auto& uv_dep_list = dlist.second;
        // build the constraint
        IloExpr expr(env);
        IloExpr rev_expr(env);
        for(const auto& uv_dep_edge: uv_dep_list){
          expr += times_used.at(uv_dep_edge);
          rev_expr += times_used.at(reverse(uv_dep_edge));
        }

        expr -= times_used.at(uv);
        rev_expr -= times_used.at(reverse(uv));
        c.add(expr >= 0);
        c.add(rev_expr >= 0);
      }
      model.add(c);
    }

    // print contig jumps from a ContigJumpMap
    void print_contig_jumps(const IloCplex& cplex, std::ostream& os = std::cout) const;

  };

  struct var_collection_multi : public var_collection {
    using var_collection::var_collection;
    EdgeVarMap used_at_all;
  };



  // auxiliary graph for callback construction
  template<typename VertexProp = no_property, typename EdgeProp = no_property>
  using AuxiliaryGraph = adjacency_list<hash_setS,
    listS,
    bidirectionalS,
    VertexProp,
    EdgeProp
  >;


  template<class GraphA, class GraphB>
  using TranslateBiMap = boost::bimap< boost::bimaps::unordered_set_of<Vertex<GraphA> >, boost::bimaps::unordered_set_of<Vertex<GraphB> > >;

  // find root LP relaxation as SOLVECallback
  class RelaxCallback: public IloCplex::SolveCallbackI{
    CallbackStatistics& stats;
    bool root;
  public:

    IloCplex::CallbackI* duplicateCallback() const{
      return (new (getEnv()) RelaxCallback(getEnv(), stats));
    }

    RelaxCallback(IloEnv env, CallbackStatistics& _stats):
      IloCplex::SolveCallbackI(env),
      stats(_stats),
      root(false)
    {}
    
    void main(){
      if(!root){
        std::cout<<"Relaxation solution in root has value "<<getBestObjValue()<<std::endl;
        stats.root_relax = getBestObjValue();
        root = true;
        //masteropt = getObjValue();
      }
    }
  };
  IloCplex::Callback GetRelaxCallback(IloEnv env, CallbackStatistics& stats){
    return (new (env) RelaxCallback(env, stats));
  }
 

  // print variables set to true in a mapping something->variables, print the prefix only if there are true variables
  void print_true_map_vars(const IloCplex& cplex,
                           const Instance& I,
                           const EdgeVarMap& vars,
                           const std::string& prefix = "",
                           std::ostream& os = std::cout)
  {
    unsigned true_count = 0;
    for(const auto& v_pair: vars)
      if(is_true(cplex.getValue(v_pair.second))){
        if(true_count == 0)
          os << prefix;
        ++true_count;
        os << " "<< I.get_edge_name(v_pair.first);
      }
    os << " ["<<true_count<<" / "<<vars.size()<<"]"<<std::endl;
  }


  // add a scaled variable to the expression at u in a Vertex-Expression Map
  void add_to_VExpr(const IloEnv& env,
                    VertexExpMap& expr_map,
                    const ScafVertex& u,
                    const IloNumVar& x,
                    const double scale)
  {
    expr_map.DEEP_EMPLACE(u, env).first->second += (x * scale);
  }

  void var_collection::print_contig_jumps(const IloCplex& cplex, std::ostream& os) const
  {
    print_true_map_vars(cplex, I, contig_jumps, "", os);
  }




}// namespace

