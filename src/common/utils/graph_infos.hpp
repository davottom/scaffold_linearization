#pragma once

/** \file graph_infos.hpp
 * compute a variety of graph properties such as max degree,
 * number of connected components, ...
 */


#include <vector>

#include "utils/utils.hpp"
#include "utils/exceptions.hpp"
#include "utils/graph_typedefs.hpp"
#include "utils/graph_parameters.hpp"
#include "utils/scaffolding_typedefs.hpp"
#include "utils/predicates.hpp"
#include "utils/scoring.hpp"

namespace scaffold{ namespace graph_infos{
  using namespace predicates;
  using namespace scoring;
 
  //! prototype class for structural graph properties
  template<class Graph, class EdgeClass, class Information>
  class StructuralInfo
  {
  protected:
    const Graph& g;           //!< a reference to the graph
    Information payload;      //!< the actual graph property
    bool up_to_date = false;  //!< indicate whether the property needs to be recomputed due to changes in the graph
    static constexpr const char* description = "some information"; //!< a description of the contained information (unfortunately, c++ forbids static virtual members)
  public:
    //! constructor
    StructuralInfo(const Graph& _g): g(_g) {}
    //! constructor
    StructuralInfo(const Graph& _g, const Information& _payload): g(_g), payload(_payload), up_to_date(true) {}

    //! every property must have a function to update itself
    /** updates can be refused (for example if the property is already up-to-date), but
     * this refute can be overwritten with the "force" flag 
     */
    virtual void update(const bool force = false) = 0;

    //! set the property to "not up-to-date"
    void invalidate()
    {
      up_to_date = false;
    }

    //! return whether the property is up-to-date
    bool is_valid() const
    {
      return up_to_date;
    }

    //! get the current value of the property, updating if necessary
    const Information& get()
    {
      if(!up_to_date) update();
      return payload;
    }

    //! get the current value of the property without updating
    /** This will thow an "info_not_up_to_date()" exception if the property is not up-to-date */
    const Information& get_const() const
    {
      if(!up_to_date) throw except::info_not_up_to_date(description);
      return payload;
    }
  };

  
  template<class Graph, class EdgeClass = VertexPair<Graph> >
  using ComponentsAndNum = std::pair<ComponentMap<Graph>, unsigned>;

  //! a graph property for the number of connected components and a map of vertices to components (represented by unsigneds)
  template<class Graph, class EdgeClass = VertexPair<Graph> >
  class ComponentInfo : public StructuralInfo<Graph, EdgeClass, ComponentsAndNum<Graph> >{
  protected:
    using Parent = StructuralInfo<Graph, EdgeClass, ComponentsAndNum<Graph> >;
    using Parent::g;
    using Parent::up_to_date;
    using Parent::payload;
    using Parent::StructuralInfo::StructuralInfo;
    bool num_up_to_date = false;  //!< indicates whether the number of components is up-to-date, independent of whether the component map is
    bool consolidated = false;    //!< indicates whether the component map is consolidated, that is, uses the first c integers (starting with 0)
    static constexpr const char* description = "connected components";

    //! merge a given component map into ours, assuming ours is consolidated
    /** for each pair (v,c) of the source map, translate v (if v_translate != NULL)
     * and translate c such that the resulting component map is consolidated
     * (assuming our component map was consolidated before).
     * Return num_comps + the number of newly used component numbers (normally, this is equal
     * to the new number of components in our component map)
     *
     * This function can be used to consolidate our own component map by giving 
     * our own component map, offset = 0, and v_translate = NULL.
     *
     * Note: if source is not our own component map, then our component map must be consolidated before
     * and either the number of connected components must be specified as offset or it must be up to date.
     */
    unsigned merge_from(const ComponentMap<Graph>& source,
                        unsigned num_comps = payload.second,
                        const Matching<Graph>* const vert_translate = NULL)
    {
      boost::unordered_map<unsigned, unsigned> comp_translate;
      for(const auto& i: source){
        // i is a pair (v,c) such that c is the component number of v in source
        const Vertex<Graph>& v = i.first;
        unsigned comp_of_v = i.second;
        // step1: get the component translation of comp_of_v, creating a new translation to num_comps if necessary
        const auto emplace_result = comp_translate.emplace(comp_of_v, num_comps);
        // if the emplace was performed, we'll have a bigger number of components in our component map
        if(emplace_result.second) ++num_comps;
        // step2: update the component of v
        comp_of_v = emplace_result.first->second;
        // step3: translate v if requested
        const Vertex<Graph> translated_v = vert_translate ? vert_translate->at(v) : v;
        // step4: insert the new mapping of v to its component
        payload.first[translated_v] = comp_of_v;
      }// for each source vertex
      return num_comps;
    }

    //! consolidate the component map such that we use exactly the first c integers as component numbers (starting with 0)
    /** add offset to each component number, facilitating disjoint unions */
    void consolidate(const unsigned offset = 0)
    {
      if(!consolidated || (offset != 0) || !num_up_to_date){
        assert(up_to_date);
        payload.second = merge_from(payload.first, offset, NULL);
        num_up_to_date = true;
        consolidated = true;
      }
    }

    //! change the component of v to comp, avoiding vertices of component avoid
    void change_component(const Vertex<Graph>& v, const unsigned comp, const unsigned avoid = UINT_MAX)
    {
      ComponentMap<Graph>& components = payload.first;
      VertexQueue<Graph> next;
      next.push(v);
      while(!next.empty()){
        while(components.at(next.front()) == avoid) next.pop();
        const Vertex<Graph>& w = next.front();
        components[w] = comp;
        for(auto range = boost::adjacent_vertices(w, g); range.first != range.second; ++range.first){
          const Vertex<Graph>& x = *range.first;
          const unsigned x_comp = components.at(x);
          // don't go to vertices of "avoid" or vertices that we've seen before
          if((x_comp != avoid) && (x_comp != comp)) next.push(x);
        }
        next.pop(); // pop w
      }
    }

  public:

    //! update the connected components
    void update(const bool force = false)
    {
      if(force || !up_to_date){
        if(boost::num_vertices(g) == 0){
          payload.first.clear();
          payload.second = 0;
        } else payload.second = graph_parameters::num_connected_components<Graph>(g, payload.first);
        up_to_date = true;
        num_up_to_date = true;
        // NOTE: the consolidation flag depends on boost outputting consolidated component maps,
        // which the version I was using (1.61) does 
        // (see http://www.boost.org/doc/libs/1_61_0/boost/graph/connected_components.hpp)
        consolidated = true;
      }
    }

    //! read from translated information
    /** using this function, we can copy the graph property value when copying a graph
     * without having to recompute it.
     */
    void read_from_infos(const ComponentInfo<Graph>& infos, const Matching<Graph>& translate)
    {
      payload.first.clear();
      
      // if the new info is up to date, then copy its component map, translating the vertices
      up_to_date = infos.up_to_date;
      if(up_to_date)
        for(const auto& i: infos.payload.first) payload.first.emplace(translate.at(i.first), i.second);
      
      num_up_to_date &= infos.num_up_to_date;
      if(num_up_to_date)
        payload.second = infos.payload.second;
    }

    //! react to splitting off a connected component of the graph g
    /** we just decrease the number of connected components */
    void read_from_split_off_component(const ComponentInfo<Graph>& info)
    {
      if(num_up_to_date) --payload.second;
      consolidated = false;
    }

    //! react to forming the disjoint union of g with a graph h, given the corresponding property of h
    void update_disjoint_union(const ComponentInfo<Graph>& info, const Matching<Graph>& translate)
    {
      up_to_date &= info.up_to_date;
      // if the component map of both infos is up to date, we can consolidate & merge
      if(up_to_date){
        consolidate();

        assert(num_up_to_date);
        // merge the component maps & update the component number
        payload.second = merge_from(info.payload.first, payload.second, &translate);
        num_up_to_date = true;
        consolidated = true;
      }
      num_up_to_date &= info.num_up_to_date;
      if(num_up_to_date && info.num_up_to_date)
        payload.second += info.payload.second;
    }

    //! react to the addition of a single vertex u to g
    void add_vertex(const Vertex<Graph>& u)
    {
      if(up_to_date && num_up_to_date && consolidated){
        payload.first[u] = payload.second;
      } else {
        up_to_date = false;
        consolidated = false;
      }
      ++payload.second;
    }

    //! react to the addition of the edge uv to g
    void add_edge(const Vertex<Graph>& u, const Vertex<Graph>& v)
    {
      // if u & v are in the same component, then adding the edge uv does not disturb the component setup
      if(up_to_date){
        ComponentMap<Graph>& components = payload.first;
        const unsigned u_comp = components.at(u);
        if(u_comp != components.at(v)){
          // update the component map from v, avoiding vertices of component u_comp
          change_component(v, u_comp, u_comp);
          // we'll have 1 less connected component
          --payload.second;
        }
      } else num_up_to_date = false;
    }

    //! react to the deletion of the edge uv, depending on whether it was a bridge or not
    void delete_edge(const EdgeClass& uv, const bool is_bridge)
    {
      // if e is a bridge, then its deletion increases the number of cc's
      if(is_bridge){
        if(consolidated)
          change_component(uv.second, payload.second);
        else
          up_to_date = false;

        ++payload.second;
      }
    } // function

    //! return whether the number of connected components is up to date
    bool num_is_valid() const
    {
      return num_up_to_date;
    }

    //! return the number of connected components, updating if necessary
    const unsigned get_num()
    {
      if(!num_up_to_date) update();
      return payload.second;
    }

    //! return the number of connected components without updating
    const unsigned get_num_const() const
    {
      if(!num_up_to_date) throw except::info_not_up_to_date();
      return payload.second;
    }

  };


  //! a graph property for the maximum degree in g
  template<class Graph, class EdgeClass = VertexPair<Graph> >
  class MaxDegreeInfo: public StructuralInfo<Graph, EdgeClass, VertexAndDegree<Graph> >{
    using Parent = StructuralInfo<Graph, EdgeClass, VertexAndDegree<Graph> >;
    using Parent::g;
    using Parent::up_to_date;
    using Parent::payload;
    using Parent::StructuralInfo::StructuralInfo;
    static constexpr const char* description = "maximum degree vertex";

    //! update the maximum degree if u has bigger degree than was previously known
    void update_from_vertex(const Vertex<Graph>& u){
      const unsigned u_deg = boost::degree(u, g);
      if(u_deg > payload.second)
        payload = VertexAndDegree<Graph>(u, u_deg);
    }
  public:
    //! update the max degree
    void update(const bool force = false)
    {
      if(force || !up_to_date){
        if(boost::num_edges(g) == 0){
          payload.second = 0;
          if(boost::num_vertices(g))
            payload.first = *(boost::vertices(g).first);
        } else payload = graph_parameters::get_max_degree(g);
        up_to_date = true;
      }
    }

    //! read from translated information
    /** using this function, we can copy the graph property value when copying a graph
     * without having to recompute it.
     */
    void read_from_infos(const MaxDegreeInfo<Graph>& infos, const Matching<Graph>& translate)
    {
      // if the new info is up to date, then copy its component map, translating the vertices
      up_to_date = infos.up_to_date;
      if(up_to_date){
        payload.first = translate.at(infos.payload.first);
        payload.second = infos.payload.second;
      }
    }

    //! read from given infos, assuming that g is a single connected component that has been split off
    void read_from_split_off_component(const MaxDegreeInfo<Graph>& info, const Matching<Graph>& translate)
    {
      // if the max-degree vertex is in the component that has been split off, translate it and store it
      const typename Matching<Graph>::const_iterator i = translate.find(info.payload.first);
      if(i != translate.cend()){
        payload.first = i->second;
        payload.second = info.payload.second;
        up_to_date = true;
      } else up_to_date = false;
    }

    //! update infos, assuming that g is the disjoint union of a graph with our infos and a graph with the given infos
    void update_disjoint_union(const MaxDegreeInfo<Graph>& info, const Matching<Graph>& translate)
    {
      up_to_date &= info.up_to_date;
      if(up_to_date){
        if(info.payload.second > payload.second)
          payload = VertexAndDegree<Graph>(translate.at(info.payload.first), info.payload.second);
      }
    }

    //! react to addition of the edge uv
    void add_edge(const Vertex<Graph>& u, const Vertex<Graph>& v)
    {
      if(up_to_date){
        update_from_vertex(u);
        update_from_vertex(v);
      }
    }

    //! react to the deletion of the edge uv
    void delete_edge(const Vertex<Graph>& u, const Vertex<Graph>& v)
    {
      add_edge(u, v);
    } // function

  };


  //! a graph property for the set of bridges in g
  template<class Graph, class EdgeClass = VertexPair<Graph> >
  class BridgeInfo: public StructuralInfo<Graph, EdgeClass, BridgeMap<Graph, EdgeClass> >{
    using Parent = StructuralInfo<Graph, EdgeClass, BridgeMap<Graph, EdgeClass> >;
    using Parent::g;
    using Parent::up_to_date;
    using Parent::payload;
    using Parent::StructuralInfo::StructuralInfo;
    static constexpr const char* description = "bridges";
  public:
    typedef std::pair<EdgeClass, unsigned> WeightedBridge ;
    typedef std::list<WeightedBridge> WBridgeList;

    //! update bridges
    void update(const bool force = false)
    {
      if(force || !up_to_date){
        if(boost::num_edges(g)){
          DEBUG5(std::cout << "running bridge finder"<<std::endl);
          payload.clear();
          mark_bridges(g, payload);
          up_to_date = true;
        } else {
          payload.clear();
          up_to_date = true;
        }
      }     
    }

    //! read from translated information
    /** using this function, we can copy the graph property value when copying a graph
     * without having to recompute it.
     */
    void read_from_infos(const BridgeInfo<Graph, EdgeClass>& infos, const Matching<Graph>& translate)
    {
      payload.clear();
      // if the new info is up to date, then copy its component map, translating the vertices
      up_to_date = infos.up_to_date;
      update_disjoint_union(infos, translate);
    }

    //! read from given infos, assuming that g is a single connected component that has been split off
    void read_from_split_off_component(const BridgeInfo<Graph, EdgeClass>& info, const Matching<Graph>& translate)
    {
      up_to_date = false;
    }

    //! update infos, assuming that g is the disjoint union of a graph with our infos and a graph with the given infos
    void update_disjoint_union(const BridgeInfo<Graph, EdgeClass>& info, const Matching<Graph>& translate)
    {
      up_to_date &= info.up_to_date;
      if(up_to_date)
        for(const auto& i: info.payload) {
          const EdgeClass translated_bridge(translate.at(i.first.first), translate.at(i.first.second));
          payload.emplace(translated_bridge, i.second);
        }
    }

    //! react to the addition of the edge uv to g
    void add_edge(const Vertex<Graph>& u, const Vertex<Graph>& v)
    {
      up_to_date = false;
    }

    //! react to the deletion of the edge uv
    void delete_edge(const EdgeClass& uv)
    {
      if(up_to_date){
        const typename BridgeMap<Graph, EdgeClass>::iterator uv_iter = payload.find(uv);
        // if uv is a bridge, then deleting it does not change the bridge property of anyone else
        if(uv_iter != payload.cend())
          payload.erase(uv_iter);
        else // if uv is not a bridge, then anyone could become a bridge after deleting uv
          up_to_date = false;
      }
    }

    //! return whether the edge uv is a bridge, updating the infos if necessary
    bool is_bridge(const Vertex<Graph>& u, const Vertex<Graph>& v)
    {
      update();
      return contains(payload, EdgeClass(u, v)) || contains(payload, EdgeClass(v, u));
    }
    //! return whether the edge uv is a bridge, avoiding updating the bridge map
    bool is_bridge_const(const Vertex<Graph>& u, const Vertex<Graph>& v) const
    {
      if(!up_to_date) throw except::info_not_up_to_date();
      return contains(payload, EdgeClass(u, v)) || contains(payload, EdgeClass(v, u));
    }

  protected:
    //! store all non-bridges with their weights in the parameter
    /** if a predicate is given, only include the bridges for which the predicate evaluates to true */
    template<class EdgePredicate = TruePredicate<Edge<Graph> > >
    void _get_non_bridges(WBridgeList& non_bridges, const EdgePredicate& predicate) const
    {
      for(EdgeIterRange<Graph> er = boost::edges(g); er.first != er.second; ++er.first){
        const Edge<Graph>& e = *er.first;
        if(predicate(e)){
          const EdgeProperty<Graph>& e_info = g[e];
          const Vertex<Graph>& u = boost::source(e, g);
          const Vertex<Graph>& v = boost::target(e, g);
          const EdgeClass uv(u, v);
          if(!contains(payload, uv) && !contains(payload, EdgeClass(v, u)))
            non_bridges.emplace_back(uv, e_info.weight);
        }
      }// for
      DEBUG5(std::cout << "found "<<non_bridges.size()<<" non-bridges"<<std::endl);
    }// function
  public:

    //! return the set of non-bridges in g, updating the bridge map if necessary
    template<class EdgePredicate = TruePredicate<Edge<Graph> > >
    void get_non_bridges(WBridgeList& non_bridges, const EdgePredicate& predicate = EdgePredicate())
    {
      if(!up_to_date) update();
      _get_non_bridges(non_bridges, predicate);
    }
    
    //! return the set of non-bridges without updating the bridge map
    template<class EdgePredicate = TruePredicate<Edge<Graph> > >
    void get_non_bridges_const(WBridgeList& non_bridges, const EdgePredicate& predicate = EdgePredicate()) const
    {
      if(!up_to_date) throw except::info_not_up_to_date();
      _get_non_bridges(non_bridges, predicate);
    }


  protected:
    //! return the non-bridge of least weight
    /** if a predicate is given, consider only non-bridges for which the predicate evaluates to true */
    template<class EdgePredicate = TruePredicate<Edge<Graph> > >
    const WeightedBridge _get_cheapest_nonbridge(const EdgePredicate& predicate) const
    {
      WBridgeList non_bridges;
      _get_non_bridges(non_bridges, predicate);

      assert(!non_bridges.empty());

      return *(std::min_element(non_bridges.cbegin(), non_bridges.cend()));
    }
  public:

    //! return the lightest non-bridge, updating the bridge map if necessary
    /** the "weight" of a bridge is the number of feedback-edges
     * in one of the two components that remain after deleting the bridge
     * (see mark_bridges())
     */
    template<class EdgePredicate = TruePredicate<Edge<Graph> > >
    const WeightedBridge get_cheapest_nonbridge(const EdgePredicate& predicate = EdgePredicate())
    {
      if(!up_to_date) update();
      return _get_cheapest_nonbridge(predicate);
    }

    //! return the lightest non-bridge without updating the bridge map (see get_cheapest_nonbridge())
    template<class EdgePredicate = TruePredicate<Edge<Graph> > >
    const WeightedBridge get_cheapest_nonbridge(const EdgePredicate& predicate = EdgePredicate()) const
    {
      if(!up_to_date) throw except::info_not_up_to_date();
      return _get_cheapest_nonbridge(predicate);
    }

  protected:
    //! return whether a vertex has an incident nonbridge
    bool _has_incident_nonbridge(const Vertex<Graph>& u) const 
    {
      for(AdjIterRange<Graph> r = boost::adjacent_vertices(u, g); r.first != r.second; ++r.first){
        const Vertex<Graph>& v = *r.first;
        if(!contains(payload, EdgeClass(u, v)) && !contains(payload, EdgeClass(v, u))) return true;
      }// for
      return false;
    }
  public:

    //! return whether a vertex has an incident nonbridge, updating the bridge map if necessary
    bool has_incident_nonbridge(const Vertex<Graph>& u) 
    {
      if(!up_to_date) update();
      return _has_incident_nonbridge(u);
    }

    //! return whether a vertex has an incident nonbridge without updating the bridge map
    bool has_incident_nonbridge_const(const Vertex<Graph>& v) const 
    {
      if(!up_to_date) throw except::info_not_up_to_date();
      return _has_incident_nonbridge(v);
    }

  protected:
    //! get non-matching bridge that minimizes the given scoring function
    template<class Scoring = IdentityScore<EdgeClass > >
    const WeightedBridge* _get_best_bridge(const Scoring& score) const 
    {
      // get the best bridge
      WeightedBridge* best_b = NULL;
      unsigned best_score = UINT_MAX;
      for(auto bi = payload.cbegin(); bi != payload.cend(); ++bi){
        const EdgeClass& b = bi->first;
        const unsigned new_score = score(bi->second);
        if(new_score < best_score){
          best_score = new_score;
          best_b = bi;
        }// if
      }// for
      return best_b;
    }
  public:

    //! get non-matching bridge that minimizes the given scoring function, updating the bridge map if necessary
    template<class Scoring = IdentityScore<EdgeClass > >
    const WeightedBridge* get_best_bridge(const Scoring& score = Scoring())
    {
      if(!up_to_date) update();
      return _get_best_bridge(score);
    }

    //! get non-matching bridge that minimizes the given scoring function without updating the bridge map
    template<class Scoring = IdentityScore<EdgeClass > >
    const WeightedBridge* get_best_bridge(const Scoring& score = Scoring()) const
    {
      if(!up_to_date) throw except::info_not_up_to_date();
      return _get_best_bridge(score);
    }

  };






  //! an accumulator class for different graph properties
  template<class Graph, class EdgeClass = VertexPair<Graph> >
  struct GraphInfos{
    // keep a reference to the graph
    const Graph& g;
    ComponentInfo<Graph, EdgeClass> comps;
    MaxDegreeInfo<Graph, EdgeClass> max_deg;
    BridgeInfo<Graph, EdgeClass> bridges;

    //! constructor: needs a graph to initalize the EdgeHasher of the bridgs; updates all values on construction of the infos
    GraphInfos(const Graph& _g, const bool update_values = false): g(_g), comps(_g), max_deg(_g), bridges(_g)
    {
      if(update_values) update_all();
    }

    //! constructor
    GraphInfos(const Graph& _g, const GraphInfos& infos, const Matching<Graph>& translate):
      g(_g),
      comps(g, infos.comps, translate),
      max_deg(g, infos.max_deg, translate),
      bridges(g, infos.bridges, translate)
    {
    }


    //! invalidate all infos
    void invalidate_all()
    {
      comps.invalidate();
      max_deg.invalidate();
      bridges.invalidate();
    }

    // =============== update routines =====================

    //! update all values
    void update_all(const bool force = false)
    {
      // TODO: all 3 can be updated at the same time with a slight modification of the bridge finder
      comps.update(force);
      max_deg.update(force);
      bridges.update(force);
    }
   
    //! react to addition of vertex u
    void add_vertex(const Vertex<Graph>& u)
    {
      // adding a vertex changes only the component infos
      comps.add_vertex(u);
    }

    //! react to addition of edge uv
    void add_edge(const Vertex<Graph>& u, const Vertex<Graph>& v)
    {
      comps.add_edge(u, v);
      max_deg.add_edge(u, v);
      bridges.add_edge(u, v);
    }

    //! react to deletion of edge uv
    void delete_edge(const EdgeClass& uv)
    {
      max_deg.delete_edge(uv.first, uv.second);
      bridges.delete_edge(uv);
      // note: if the bridges are still valid, uv is a bridge; otherwise, no conclusion on the components can be made
      comps.delete_edge(uv, bridges.is_valid());
    } // function

    //! update the infos from the graph h of which g has just been copied
    /** using this function, we can copy the graph property value when copying a graph
     * without having to recompute it.
     *
     * translate has to contain a map such that, for each vertex u of h, translate[u]
     * is the representation of u in g
     */
    void read_from_infos(const GraphInfos<Graph>& infos, const Matching<Graph>& translate)
    {
      max_deg.read_from_infos(infos.max_deg, translate);
      comps.read_from_infos(infos.comps, translate);
      bridges.read_from_infos(infos.bridges, translate);
    }

    //! react to splitting off this connected component g from a graph h
    /** translate has to contain a map such that, for each vertex u of h, translate[u]
     * is the representation of u in g
     */
    void read_from_split_off_component(const GraphInfos<Graph>& infos, const Matching<Graph>& translate)
    {
      max_deg.read_from_split_off_component(infos.max_deg, translate);
      comps.read_from_split_off_component(infos.comps);
      bridges.read_from_split_off_component(infos.bridges, translate);
    }

    //! react to forming a disjoint union with g with another graph h, given the infos of h and a translate map
    /** translate has to contain a map such that, for each vertex u of h, translate[u]
     * is the representation of u in g
     */
    void update_disjoint_union(const GraphInfos<Graph>& infos, const Matching<Graph>& translate)
    {
      max_deg.update_disjoint_union(infos.max_deg, translate);
      comps.update_disjoint_union(infos.comps, translate);
      bridges.update_disjoint_union(infos.bridges, translate);
    }

    // ================== get values =====================

  protected:
    //! get the number of paths and cycles in the graph, assuming its max degree is 2
    PathsAndCycles _get_paths_and_cycles(const unsigned cc)
    {
      if(max_deg.get().second <= 2){
        const unsigned V = boost::num_vertices(g);
        const unsigned E = boost::num_edges(g);
        const unsigned cycles = E + cc - V;
        const unsigned paths  = V - E;
        return PathsAndCycles(paths, cycles);
      } else throw except::invalid_assumption("graph is not degree-2");
    }
  public:

    //! get the number of paths and cycles in the graph, assuming its max degree is 2, updating infos if necessary
    PathsAndCycles get_paths_and_cycles()
    {
      return _get_paths_and_cycles(comps.get_num());
    }// function

    //! get the number of paths and cycles in the graph, assuming its max degree is 2, without updating the infos
    PathsAndCycles get_paths_and_cycles_const() const
    {
      return _get_paths_and_cycles(comps.get_num_const());
    }// function

    //! return the feedback edge set number of the graph, updating its infos if necessary
    unsigned get_FES()
    {
      return compute_FES(boost::num_vertices(g), boost::num_edges(g), comps.get_num());
    }

    //! return the feedback edge set number of the graph without updating its infos
    unsigned get_FES_const() const 
    {
      return compute_FES(num_vertices(g), num_edges(g), comps.get_num_const());
    }

    //! return whether the graph is acyclic, updating its infos if necessary
    bool is_acyclic() 
    {
      if(comps.num_is_valid()) 
        return (get_FES_const() == 0);
      else
        return bridges.get().size() == boost::num_edges(*g);
    }// function

    //! return whether the graph is acyclic without updating its infos
    bool is_acyclic_const() const 
    {
      if(comps.num_is_valid())
        return (get_FES_const() == 0);
      else 
        return bridges.get_const().size() == boost::num_edges(*g);
    }

  protected:
    //! return whether g has maximum degree 2 using only queries to |V| and |E|
    bool is_max_deg_two_easy_cases() const
    {
      const unsigned E = boost::num_edges(*g);
      // if g has no edges, it's solved
      if(E == 0) return true;
      // if g contains only the matching, it's solved
      const unsigned V = boost::num_vertices(*g);
      if(V >= 2 * E) return true;
      return false;
    }
  public:
    //! return whether the graph has max degree 2, updating its max-degree info if necessary
    bool is_max_deg_two() 
    {
      if(is_max_deg_two_easy_cases()) return true;
      return (max_deg.get().second < 3);
    }// function

    //! return whether the graph has max degree 2 without updating infos
    bool is_max_deg_two_const() const 
    {
      if(is_max_deg_two_easy_cases()) return true;
      return (max_deg.get_const().second < 3);
    }


  }; // class

}} // namespace

