

/** \file graph_utils.hpp
 * collection of utilities working with graphs
 */

#ifndef GRAPH_UTILS_HPP
#define GRAPH_UTILS_HPP


#include <stack>
#include <boost/unordered_set.hpp>
#include <boost/graph/max_cardinality_matching.hpp>
//#include <boost/graph/copy.hpp>
#include "utils/copy.hpp"

#include "utils.hpp"
#include "graph_typedefs.hpp"

using namespace boost;

namespace boost{
  // strangely, boost::filtered_graph misses degree()
  template<class Graph, typename EPred, typename VPred>
  size_t degree(const Vertex<filtered_graph<Graph,EPred,VPred> >& v, const filtered_graph<Graph,EPred,VPred>& g)
  {
    return out_degree(v, g);
  }
}

namespace scaffold{

  //! remove a matching pair, given only one of its two vertices
  /** return whether u was found and removed */
  template<class Graph>
  bool remove_matching_pair(const Vertex<Graph>& u, Matching<Graph>& match)
  {
    const typename Matching<Graph>::iterator uv = match.find(u);
    if(uv != match.end()){
      const typename Matching<Graph>::iterator vu = match.find(uv->second);
      match.erase(uv);
      match.erase(vu);
      return true;
    } else return false;
  }


  //! set up internal vertex indices by order of occurance in vertices(g)
  template<class Graph>
  void setup_vertex_indices(Graph& g, unsigned first_index = 0)
  {
    for(VertexIterRange<Graph> r = boost::vertices(g); r.first != r.second;++r.first) 
      g[*r.first].index = first_index++;
  }// function

  //! compute the feedback edge set number from a given number of vertices, edges, and connected components
  unsigned compute_FES(const unsigned v, const unsigned e, const unsigned cc){
    return e + cc - v;
  }

  //! split off a component of g and return it
  /** while the remainder of g might contain more components, the split-off graph is connected! */
  template<class Graph>
  Graph* split_off_component(Graph& g,
                             Vertex<Graph> source = NULL,
                             Matching<Graph>* _translate = NULL,
                             Matching<Graph>* _translate_back = NULL){
    typedef typename Graph::vertex_bundled VertexInfo;
    Matching<Graph>* translate = _translate ? _translate : new Matching<Graph>();
    Matching<Graph>* translate_back = _translate_back ? _translate_back : new Matching<Graph>();

    assert(boost::num_vertices(g));
    if(source == NULL) source = *boost::vertices(g).first;
    Graph* h = new Graph;

    // Step 1: copy the component of the first vertex of g
    DEBUG4(std::cout << "copying component of "<<  g[source].name<<std::endl);
    boost::copy_component(g, source, *h, orig_to_copy(boost::associative_property_map<Matching<Graph> >(*translate)).vertex_index_map(boost::get(&VertexInfo::index, g)));
    // if g didn't have at least 2 components, return NULL
    if(boost::num_vertices(g) == boost::num_vertices(*h)){ delete h; return NULL;}
    // Step 1a: invert the translation
    for(auto& i : *translate) translate_back->emplace(i.second, i.first);
    // Step 2: delete all translations of vertices of the copy
    for(VertexIterRange<Graph> r = vertices(*h); r.first != r.second; ++r.first){
      const Vertex<Graph> u = translate_back->at(*r.first);
      boost::clear_vertex(u, g);
      boost::remove_vertex(u, g);
    }// for
    if(!_translate) delete translate;
    if(!_translate_back) delete translate_back;
    // update indices and infos
    setup_vertex_indices(g);
    setup_vertex_indices(*h);
    return h;
  }


  //! set a map that assigns a number x to an edge e iff e is a bridge splitting away a component containing x feedback edges
  /** implements a modification of Tarjan's bridge finding algorithm
   * NOTE: this assumes that g is connected
   */
  template<class Graph, class EdgeClass>
  void mark_bridges(const Graph& g, BridgeMap<Graph, EdgeClass>& bridge_map){
    // set up infrastructure for Tarjan's algorithm
    struct TarjanInfos{ // modified Tarjan infos for a vertex v
      size_t index,             // DFS index of v
             lowest_seen_index, // the lowest index that is at most 1 away from the DFS subtree rooted at v
             subtree_size,      // the number of vertices in the DFS subtree rooted at v
             subtree_edges;     // the number of edges in the DFS subtree rooted at v
      const Vertex<Graph> parent;    // the parent of v in the DFS tree
      // setup Tarjan infos with just an index and subtree size 1
      TarjanInfos(const size_t _index, const Vertex<Graph>& _parent):
        index(_index), lowest_seen_index(_index), subtree_size(1), subtree_edges(0), parent(_parent) {}
    };
    // prepare a DFS vertex stack
    struct StackElement {
      Vertex<Graph> v;
      AdjIterRange<Graph> adj;
      // constructor
      StackElement(const Vertex<Graph>& _v, const AdjIterRange<Graph>& _adj):
        v(_v),
        adj(_adj) {}
    };

    // keep a map of tarjan infos of visited vertices
    unordered_map<Vertex<Graph>, TarjanInfos> tarjan_infos;
    // if there are no edges, there are also no bridges
    if(!num_edges(g)) return;
    // DFS root will be the first vertex of g
    VertexIter<Graph> root = vertices(g).first;
    std::stack<StackElement > adj_stack;
    // give the root an index and mark it visited
    tarjan_infos.emplace(std::piecewise_construct, std::make_tuple(*root), std::make_tuple(0, *root));
    // add the root on top of the stack
//    adj_stack.emplace(std::piecewise_construct, std::make_tuple(*root, adjacent_vertices(*root, g), false));
    adj_stack.emplace(*root, adjacent_vertices(*root, g));
    // assert that the root is not isolated
    assert(adj_stack.top().adj.first != adj_stack.top().adj.second);

    // walk through the graph in DFS, calculating indices on the way down and the other Tarjan Infos on the way back up
    size_t current_index = 0;
    while(!adj_stack.empty()){
      // get (v, r) from the stack
      StackElement& e = adj_stack.top();
      const Vertex<Graph>& v = e.v;
      AdjIterRange<Graph>& r = e.adj;
      TarjanInfos& v_infos(tarjan_infos.at(v));

      // check whether we're going up or down the DFS tree
      if(contains(tarjan_infos, *r.first)){
        const TarjanInfos& u_infos(tarjan_infos.at(*r.first));
        // we're coming up from u
        DEBUG5(std::cout << "coming up from "<<g[*r.first].name<<" who has seen "<<u_infos.lowest_seen_index<<" and his subtree has "<<u_infos.subtree_size<<" verts and "<<u_infos.subtree_edges<<" edges (we are at "<<g[v].name<<" with index "<<v_infos.index<<")"<<std::endl);
        // if the lowest seen index of u is smaller than the index of v, then uv is a bridge splitting u's subtree size
        if(u_infos.lowest_seen_index > v_infos.index){
          // compute the FES that is split away
          const size_t FES = compute_FES(u_infos.subtree_size, u_infos.subtree_edges, 1);
          // insert the bridge
          bridge_map.emplace(UnorderedVertexPair<Graph>(v, *r.first), FES);
          DEBUG5(std::cout<<"found bridge ("<<g[v].name<<","<<g[*r.first].name<<") splitting away "<<u_infos.subtree_size<<" vertices and "<<u_infos.subtree_edges<<" edges (FES "<<FES<<")"<<std::endl);
        } else // otherwise, update v's lowest seen index
          v_infos.lowest_seen_index = std::min(v_infos.lowest_seen_index, u_infos.lowest_seen_index);
        // update subtree infos of v
        v_infos.subtree_size += u_infos.subtree_size;
        v_infos.subtree_edges += u_infos.subtree_edges + 1;
        // skip to the next unvisited vertex, updating lowest_seen_index of v with the index of the neighbor and updating the #edges
        ++r.first;
        for( ; (r.first != r.second) && contains(tarjan_infos, *r.first); ++r.first){
          const TarjanInfos& w_infos(tarjan_infos.at(*r.first));
          // don't update smallest seen index if w is our parent or w is below us
          if((*r.first != v_infos.parent) && (w_infos.index < v_infos.index))
            v_infos.lowest_seen_index = std::min(v_infos.lowest_seen_index, w_infos.index);
          // if w is below us, then update the edges in our subtree
          if(w_infos.index > v_infos.index) ++v_infos.subtree_edges;
        }
      } // if
      if(r.first != r.second){
        // we're going down to *r.first, which is guaranteed to not have been visited at this point
        const Vertex<Graph>& u = *r.first;
        DEBUG5(std::cout << "going down to unvisited "<<g[u].name<<" from "<<g[v].name<<std::endl);
        // set up the tarjan infos of u
        TarjanInfos& u_infos = tarjan_infos.emplace(std::piecewise_construct, std::make_tuple(u), std::make_tuple(++current_index, v)).first->second;
        // before pushing, skip through u's neighbors so that the first one is unvisited, keeping lowest seen index up to date
        AdjIterRange<Graph> u_adj = adjacent_vertices(u, g);
        for( ; (u_adj.first != u_adj.second) ? contains(tarjan_infos, *u_adj.first) : false; ++u_adj.first)
          if(*u_adj.first != v) // don't update for v
            u_infos.lowest_seen_index = std::min(u_infos.lowest_seen_index, tarjan_infos.at(*u_adj.first).index);
        // if u is not a leaf of the DFS tree, then push u to the stack, otherwise, continue with v on the stack as "coming up from u"
        if(u_adj.first != u_adj.second) adj_stack.push(StackElement(u, u_adj));
      } else adj_stack.pop(); // if we arrived at the end, then pop v from the stack
    } // while
    DEBUG4(std::cout << "done finding "<<bridge_map.size()<<" bridges"<<std::endl);
  } // function


  //! return the number of isolated paths & cycles making up g, provided that the maximum degree is 2
  template<class Graph>
  PathsAndCycles get_num_pc(const Graph& g){

    PathsAndCycles result;
    VertexSet<Graph> marked;

    // first, find all paths
    for(VertexIterRange<Graph> i = vertices(g); i.first != i.second; ++i.first){
      const Vertex<Graph>& v = *(i.first);
      if(degree(v, g) <= 1 && !contains(marked, v)){
        // found another path
        result.p++;
        marked.insert(v);

        if(degree(v, g) == 1) {
          // if we have a non-singleton path, go through it, marking the vertices
          Vertex<Graph> u = v;
          Vertex<Graph> last = u;
          do{
            AdjIter<Graph> nxt = adjacent_vertices(u, g).first;
            if(*nxt == last) ++nxt;
            last = u;
            u = *nxt;
            marked.insert(u);
          } while(degree(u, g) == 2);
          DEBUG5(std::cout << "finished path at "<<g[u].name<<" with degree "<<degree(u, g)<<std::endl);
          assert(degree(u, g) == 1);
        } // if
      } // if
    } // if/for

    // second, find all cycles
    for(VertexIterRange<Graph> i = vertices(g); i.first != i.second; ++i.first) if(!contains(marked, *(i.first))){
      result.c++;
      Vertex<Graph> u = *(i.first);
      Vertex<Graph> last = u;
      DEBUG3(std::cout << "found cycle starting at "<< g[u].name<<":");
      while(!contains(marked, u)){
        assert(degree(u, g) == 2);
        marked.insert(u);
        
        AdjIter<Graph> nxt = adjacent_vertices(u, g).first;
        if(*nxt == last) ++nxt;
        last = u;
        u = *nxt;
        DEBUG3(std::cout <<  g[u].name<<" ");
      }// while
      DEBUG3(std::cout << std::endl);
    }// if/for
    return result;
  }

  //! use a translate map to translate a matching
  template<class Graph>
  void copy_matching(const Matching<Graph>& from, Matching<Graph>& to, const VTranslateMap<Graph,Graph>& translate)
  {
    for(auto i : from) to[translate.at(i.first)] = translate.at(i.second);
  }


  //! return whether a vertex pair equals an edge
  template<class Graph>
  bool is_equal(const VertexPair<Graph>& p, const Edge<Graph>& e, const Graph& g)
  {
    const Vertex<Graph>& u = boost::source(e, g);
    const Vertex<Graph>& v = boost::target(e, g);
    return ((u == p.first) && (v == p.second)) || ((u == p.second) && (v == p.first));
  }

  //! remove all vertices from a graph that are reachable from a container of vertices
  /** if d is dir_rev, then remove all vertices from which any vertex in the container is reachable
   * if d is dir_fwdrev, then remove all vertices that are reachable from any vertex in the container in the underlying undirected graph */
  template<class Graph, class Container>
  void remove_reachable(Graph& g, const Container& U, const Direction& d = dir_fwd)
  {
    VertexSet<Graph> pending(U.begin(), U.end());
    while(!pending.empty()){
      const Vertex<Graph> u = *(pending.cbegin());
      pending.erase(pending.cbegin());
      // step 1: add all neighbors of u to the list of next pending vertices
      if((d == dir_fwd) || (d == dir_fwdrev))
        for(auto r = boost::out_edges(u, g); r.first != r.second; ++r.first)
          pending.insert(boost::target(*r.first, g));
      if((d == dir_rev) || (d == dir_fwdrev))
        for(auto r = boost::in_edges(u, g); r.first != r.second; ++r.first)
          pending.insert(boost::source(*r.first, g));
      // step 2: remove u from the aux graph
      boost::clear_vertex(u, g);
      boost::remove_vertex(u, g);
    }// while
  }// function

} // namespace boost

#endif


