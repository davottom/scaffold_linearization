
/** \file scaffold_graph.hpp
 * implementation of the main graph class, managing everything related to the underlying graph and its matching
 */

#pragma once

#include <cmath>
#include "utils/predicates.hpp"
#include "utils/normal_distribution.hpp"
#include "utils/graph_typedefs.hpp"
#include "utils/scaffolding_typedefs.hpp"
#include "utils/graph_infos.hpp"
#include "utils/alternating_path.hpp"

#define FLAT_DISTRIBUTION(x) (ReadDistribution(1e+4, x))

namespace scaffold{


  using namespace graph_infos;
  using namespace predicates;

  typedef NormalDistribution<LengthType> ReadDistribution;
  typedef std::tuple<int,ScafVertex,ScafVertex> Extremity_Ambiguous_Path;


  //! the main graph class
  /** the ScaffoldGraph class is the main graph class, providing infrastructure around a boost::adjacency_list graph, such as
   * managing the matching, getting incident edges, and managing graph properties (max degree, components, ...)
   */
  class ScaffoldGraph
  {
    public:
      typedef RawScaffoldGraph Graph;
    protected:
      Graph* const _the_graph;  //!< the underlying boost graph
      Matching<Graph> matched;  //!< a perfect matching in _the_graph
      /** although edge properties have is_matching_edge(), this allows finding the matched vertex in O(1) average time */
      GraphInfos<Graph> infos;  //!< a set of graph properties of the underlying graph
      ReadDistribution read_dist; //!< the expected distribution of read lengths, used to calculate the probability of a given read-length
    public:

      typedef typename boost::graph_traits<Graph>::vertex_descriptor  Vertex;
      typedef typename boost::graph_traits<Graph>::edge_descriptor    Edge;
      typedef typename Graph::vertex_bundled  VertexProperty;
      typedef typename Graph::edge_bundled    EdgeProperty;
      typedef typename Graph::graph_bundled   GraphProperty;
      typedef typename boost::property_map<Graph, unsigned VertexProperty::*>::const_type cVertexIndexMap;
      typedef typename boost::property_map<Graph, unsigned VertexProperty::*>::type VertexIndexMap;
      typedef typename boost::unordered_set<Vertex> VertexSet;

    protected:
      //! outsource common constructor work: copy a given graph, construct a matching from the translate map, and update infos if requested
      void copy_and_init_matching(const ScaffoldGraph& g, Matching<Graph>* const translate, const bool update_infos)
      {
        // if translate is given as NULL, then allocate a translation map
        Matching<Graph>* const t = (translate ? translate : new Matching<Graph>());
        // copy the graph
        boost::copy_graph(*g._the_graph, *_the_graph,
            orig_to_copy(associative_property_map<Matching<Graph> >(*t)).vertex_index_map(g.get_index_map()));
        // copy the matching
        DEBUG4(std::cout << "initializing matching of the copy"<<std::endl);
        copy_matching<Graph>(g.matched, matched, *t);
        // update infos if neccessary
        if(update_infos) infos.read_from_infos(g.infos, *t);
        // clean up the translation map if we allocated it
        if(!translate) delete t;
      }

      //! compute the matching by parsing the properties of all edges
      void parse_matching_from_graph()
      {
        DEBUG1(std::cout << "parsing matching"<<std::endl);
        matched.clear();
        for(EdgeIterRange<Graph> r = boost::edges(*_the_graph); r.first != r.second; ++r.first){
          const Edge& e = *r.first;
          const EdgeProperty& e_info = (*_the_graph)[e];
          if(e_info.is_matching_edge()){
            const Vertex u = source(e);
            const Vertex v = target(e);
            matched.emplace(u, v);
            matched.emplace(v, u);
          }// if
        }// for
        // sanity check: matched should have an entry for each vertex, since each vertex should be matched
        if(matched.size() != boost::num_vertices(*_the_graph))
          std::cerr << "WARNING: not all vertices are matched in the input graph ("<<matched.size()<<" of "<<boost::num_vertices(*_the_graph)<<" matched), do expect problems"<<std::endl;
      }// function

      //! remove the indication that uv is a matching edge
      void delete_matching(const Vertex& u)
      {
        remove_matching_pair<Graph>(u, matched);
      }

    public:
      //! constructor initializing an empty graph
      ScaffoldGraph():
        _the_graph(new Graph()), infos(*_the_graph)
    {}

      //! constructor initializing a copy of the given graph g
      ScaffoldGraph(const Graph& g):
        _the_graph(new Graph(g)), infos(*_the_graph), read_dist((*_the_graph)[boost::graph_bundle].get_distribution_parameters())
    {
      parse_matching_from_graph();
    }

      //! constructor initializing using the given graph pointer; no copy is performed
      /** be extra cautious with this, as this leaves you with a direct pointer to _the_graph wich, among others,
       * should never be freed manually (the destructor of the ScaffoldGraph will free it)! */
      ScaffoldGraph(Graph* const g):
        _the_graph(g), infos(*_the_graph), read_dist((*_the_graph)[boost::graph_bundle].get_distribution_parameters())
    {
      //update_vertex_indices();
      parse_matching_from_graph();
    }

      //! constructor initializing a copy of the given graph g and returning the translate map in translate; update infos if requested
      /** the translate map will translate vertices of g into their representation in _the_graph */
      ScaffoldGraph(const ScaffoldGraph& g, Matching<Graph>* const translate = NULL, const bool update_infos = true) :
        _the_graph(new Graph()), infos(*_the_graph), read_dist(g.get_graph()[boost::graph_bundle].get_distribution_parameters())
    {
      copy_and_init_matching(g, translate, update_infos);
    }

      //! destructor deleting _the_graph
      ~ScaffoldGraph() { delete _the_graph; }

      //! return the number of vertices
      size_t num_vertices() const
      {
        return boost::num_vertices(*_the_graph);
      }
      //! return the number of edges
      size_t num_edges() const
      {
        return boost::num_edges(*_the_graph);
      }

      //! get the read-length distribution to compute probabilities of read lengths
      const ReadDistribution& get_read_distribution() const
      {
        return read_dist;
      }

      //! return the probability that the length of the given edge is actually at least len
      float prob_length_greater(const ScafEdge& e, const int len)
      {
        return read_dist.prob_distance_to_mean_at_least(std::abs(len - operator[](e).length));
      }
      //! return the probability that the total length of the given edge set is at least len
      float prob_length_greater(const ScafEdgeSet& E, const int len)
      {
        // we use the fact that the sum of two independent normally distributed random variables X & Y
        // is a normally distributed random variable Z
        ReadDistribution sum_dist(0,0);
        int fixed_part = 0;
        for(const ScafEdge& e: E){
          // note that negative lengths are verified using overlaps and, thus, they are not subject to distributions!
          const int e_len = operator[](e).length;
          if(e_len > 0)
            sum_dist += ReadDistribution(read_dist.variance, e_len, T_VARIANCE);
          else
            fixed_part += e_len;
        }
        return sum_dist.prob_of_picking_at_least(len - fixed_part);
      }

      //! return the length of an edge taking uncertainty into account
      /** if consider_negative_certain is true, then negative lengths are considered certain **/
      UncertainLength get_uncertain_length(const ScafEdge& e, const bool consider_negative_certain = true) const
      {
        const ScafEdgeProperty& e_prop = (*_the_graph)[e];
        const float variance = (consider_negative_certain && e_prop.is_certain()) ? 1e-8 : read_dist.variance;
        return UncertainLength(variance, e_prop.length, T_VARIANCE);
      }

      UncertainLength get_uncertain_length(const ScafVertex& u, const ScafVertex& v, const bool consider_negative_certain = true) const
      {
        const auto e_pair = find_edge(u, v);
        assert(e_pair.second);
        return get_uncertain_length(e_pair.first, consider_negative_certain);
      }

      //! remove the length attribute of an edge (if it has been deemed wrong for example)
      void remove_length(const ScafEdge& e)
      {
        (*_the_graph)[e].length = NO_LENGTH;
      }

      //! recompute vertex indices; useful after deleting vertices or uniting scaffold graphs
      void update_vertex_indices(const unsigned first_index = 0)
      {
        setup_vertex_indices(*_the_graph, first_index);
      }

      //! get an index map to use in various boost algorithms
      const VertexIndexMap get_index_map() const
      {
        return boost::get(&VertexProperty::index, *_the_graph);
      }

      //! find an edge in _the_graph, given its endpoints
      /** if uv is found in _the_graph, return (uv, true), otherwise return a pair of garbage and false */
      std::pair<Edge, bool> find_edge(const Vertex& u, const Vertex& v) const
      {
        return boost::edge(u, v, *_the_graph);
      }

      //! find an edge in _the_graph, given its endpoints see find_edge()
      std::pair<Edge, bool> find_edge(const VertexPair<Graph>& uv) const
      {
        return find_edge(uv.first, uv.second);
      }

      //! if the edge uv exist, return its multiplicity, otherwise return -1
      int edge_multiplicity(const Vertex& u, const Vertex& v) const
      {
	auto p = find_edge(u,v);
	if(!p.second) return -1;
	return (*this)[p.first].multiplicity;
      }

      void add_inner_multiplicity(ScafVertex &u, int addition)
      {
	(*_the_graph)[u].multiplicity += addition;
      }

      /** if the function has not been already called for u, return the inner multiplicity, 
       *  otherwise return 0. */ 
      int get_inner_multiplicity_once(const ScafVertex &u) const
      {
	if((*_the_graph)[u].multiplicity_call)
	  return 0;
	(*_the_graph)[u].multiplicity_call=true;
	return (*_the_graph)[u].multiplicity;
      }

      //! remove all non-matching edges incident with u
      void clear_vertex(const Vertex& u)
      {
        for(auto r = get_incident_non_matching(u); r;) delete_edge(*(r++));
      }


      //! remove a vertex and all its incident edges
      /** If update_indices is false, then vertex indices are not updated, you'll have to update them before running certain boost algorithms.
       * If update_infos is false, then graph infos will not be updated, but just invalidated.
       */
      void delete_vertex(const Vertex& u, const bool update_indices = true, const bool update_infos = true)
      {
        // step 0: remove u from the graph
        if(update_infos)
          while(degree(u))
            delete_edge(*boost::out_edges(u, *_the_graph).first, true);
        else boost::clear_vertex(u, *_the_graph);
        boost::remove_vertex(u, *_the_graph);


        // step 1: remove u from the matching
        delete_matching(u);

        // step 2: update indices
        if(update_indices) update_vertex_indices();
      }


      //! get a const reference to the vertex properties of u
      const VertexProperty& operator[](const Vertex& u) const
      {
        return (*_the_graph)[u];
      }
      //! get a reference to the vertex properties of u
      VertexProperty& operator[](const Vertex& u)
      {
        return (*_the_graph)[u];
      }
      //! get a const reference to the edge properties of e
      const EdgeProperty& operator[](const Edge& e) const
      {
        return (*_the_graph)[e];
      }
      //! get a reference to the edge properties of e
      EdgeProperty& operator[](const Edge& e)
      {
        return (*_the_graph)[e];
      }
      //! get a const reference to the edge properties of uv, throwing invalid_assumption if uv does not exist
      const EdgeProperty& operator[](const VertexPair<Graph>& uv) const
      {
        const std::pair<Edge, bool> e = boost::edge(uv.first, uv.second, *_the_graph);
        if(e.second)
          return (*_the_graph)[e.first];
        else
          throw except::invalid_assumption("edge does not exist");
      }
      //! get a reference to the edge properties of uv, throwing invalid_assumption if uv does not exist
      EdgeProperty& operator[](const VertexPair<Graph>& uv)
      {
        const std::pair<Edge, bool> e = boost::edge(uv.first, uv.second, *_the_graph);
        if(e.second)
          return (*_the_graph)[e.first];
        else
          throw except::invalid_assumption("edge does not exist");
      }
      //! return a const reference to the graph properties
      const GraphProperty& get_graph_property() const
      {
        return (*_the_graph)[boost::graph_bundle];
      }

      //! get a copy of the source vertex of e
      Vertex source(const Edge& e) const
      {
        return boost::source(e, *_the_graph);
      }
      //! get a copy of the target vertex of e
      Vertex target(const Edge& e) const
      {
        return boost::target(e, *_the_graph);
      }

      //! get a const reference to the graph infos
      const GraphInfos<Graph>& get_infos() const
      {
        return infos;
      }

      //! get a const reference to the underlying graph
      const Graph& get_graph() const
      {
        return *_the_graph;
      }
      //! get a const reference to the perfect matching in _the_graph
      const Matching<Graph>& get_matching() const
      {
        return matched;
      }
      //! get a const reference to the vertex matched with v
      const Vertex& matched_with(const Vertex& v) const
      {
        DEBUG5(if(!contains(matched, v)) std::cout << (*_the_graph)[v].name << " is unmatched" << std::endl);
        assert(contains(matched, v));
        return matched.at(v);
      }
      //! return whether the given VertexPair is matched
      /** compared to operator[](find_edge(u, v)), we save an edge lookup */
      const bool is_matching_edge(const VertexPair<Graph>& uv) const
      {
        return (uv.first == matched_with(uv.second));
      }
      //! return the matching edge that is incident with v
      const Edge incident_matching_edge(const Vertex& v) const
      {
        return boost::edge(v, matched_with(v), *_the_graph).first;
      }
      //! return the degree of v
      const unsigned degree(const Vertex& v) const
      {
        return boost::degree(v, *_the_graph);
      }

      //! return whether u & v are adjacent
      bool adjacent(const Vertex& u, const Vertex& v) const
      {
        return boost::edge(u, v, *_the_graph).second;
      }
      //! return whether two edges are adjacent, that is, share an endpoint
      bool incident(const Vertex& x, const Edge& uv) const
      {
        return (x == source(uv)) || (x == target(uv));
      }
      //! return whether two edges are adjacent, that is, share an endpoint
      bool adjacent(const Edge& uv, const Edge& xy) const
      {
        return incident(source(uv), xy) || incident(target(uv), xy);
      }

      //! put all neighbors of u into nh
      void get_neighbors(const Vertex& u, VertexSet& nh) const
      {
        for(auto r = boost::adjacent_vertices(u, *_the_graph); r.first != r.second; ++r.first)
          nh.emplace(*r.first);
      }

      //! compute the maximum multiplicity occuring in g
      MultiType get_max_multiplicity() const
      {
        MultiType result = 0;
        for(auto r = boost::edges(*_the_graph); r.first != r.second; ++r.first)
          result = std::max(result, operator[](*r.first).multiplicity);
        return result;
      }

      //! return a non-contig edge of maximum length in the graph
      std::pair<Edge, int> get_max_non_contig_length() const
      {
        Edge _max_e;
        int _max = -1;
        for(EdgeIterRange<Graph> r = boost::edges(*_the_graph); r.first != r.second; ++r.first){
          const Edge& e = *r.first;
          const ScafEdgeProperty& e_info = (*_the_graph)[e];
          if(!e_info.is_matching_edge())
            if(e_info.length > _max){
              _max = e_info.length;
              _max_e = e;
            }
        }// for each edge e
        return {_max_e, _max};
      }


      //! delete an edge e and update infos if requested
      /** if no update of the infos is requested, they will be invalidated to stay consistent */
      void delete_edge(const Edge& e, const bool update_infos = true)
      {
        const Vertex& u = boost::source(e, *_the_graph);
        const Vertex& v = boost::target(e, *_the_graph);

        // update the infos with what we know about the to-be-deleted edge
        if(update_infos) {
          infos.delete_edge(VertexPair<Graph>(u, v));
        } else {
          infos.invalidate_all();
        }
        if(is_matching_edge({u,v})) {
          matched.erase(u);
          matched.erase(v);
        }
        boost::remove_edge(e, *_the_graph);
        DEBUG5(std::cout<<"deleted edge "<<(*_the_graph)[u].name<<"-"<<(*_the_graph)[v].name<<std::endl);
      }// function

      //! delete an edge e and update infos if requested; return whether the edge had existed before or not
      bool delete_edge(const Vertex& u, const Vertex& v)
      {
        const std::pair<Edge, bool> e_pair = find_edge(u, v);
        if(e_pair.second){
          delete_edge(e_pair.first);
          return true;
        } else return false;
      }
      //! convenience function to delete pairs of vertices
      bool delete_edge(const VertexPair<Graph>& uv)
      {
        return delete_edge(uv.first, uv.second);
      }

      //! delete all non-matching edges incident to a given vertex u
      void delete_non_matching(const Vertex& u)
      {
        auto inc_iter = get_incident_non_matching(u);
        while(inc_iter) delete_edge(*(inc_iter++));
      }


      //! delete all non-matching edges incident to a given vertex u
      void delete_non_matching(const Vertex& u, std::list<Extremity_Ambiguous_Path>& list_ambiguous_paths,
                               std::map<const ScafVertex, std::list<Extremity_Ambiguous_Path>::iterator>& extremities_map)
      {
        auto inc_iter = get_incident_non_matching(u);
        while(inc_iter) {
          const ScafEdge e = *inc_iter;
          const ScafVertex x = target(e);
          assert(x != u);

          if(contains(extremities_map, x)) {
            /* Update the multiplicity sum of vertex x
             * and change its position in list_ambiguous_paths such that the list remain sorted*/
            //std::get<0>(*(extremities_map[x]))-=(*this)[e].multiplicity;
            Extremity_Ambiguous_Path a=std::make_tuple(std::get<0>(*(extremities_map[x]))-(*this)[e].multiplicity,
                                                       std::get<1>(*(extremities_map[x])),
                                                       std::get<2>(*(extremities_map[x])));

            extremities_map[x]=list_ambiguous_paths.insert(std::lower_bound(list_ambiguous_paths.begin(),
                                                                            list_ambiguous_paths.erase(extremities_map[x]), a),
                                                           std::move(a));

          }
          delete_edge(*(inc_iter++));
        }
      }

      //! return the sum of the weight of non matching edges incident to u
      unsigned sum_multiplicity_non_matching(const Vertex& u) const
      {
        unsigned sum = (*this)[u].multiplicity;
        for(auto v_iter = get_incident_non_matching(u); v_iter; ++v_iter)
          sum += (*this)[*v_iter].multiplicity;
        return sum;
      }

      //! find a vertex given its name
      /** NOTE: avoid if possible as this scans all vertices, which is _highly_ inefficient */
      Vertex const * find_vertex_by_name(const VertexName& name) const
      {
        for(VertexIterRange<Graph> r = boost::vertices(*_the_graph); r.first != r.second; ++r.first)
          if((*_the_graph)[*r.first].name == name)
            return &(*r.first);
        return NULL;
      }

      //! find edge by name
      /** NOTE: avoid if possible as this uses find_vertex_by_name() which is _highly_ inefficient */
      Edge const * find_edge_by_name(const EdgeName& name) const
      {
        // step 1: find the first vertex
        DEBUG5(std::cout<<"finding ("<<name.first<<","<<name.second<<")"<<std::endl);
        Vertex const * u = find_vertex_by_name(name.first);
        if(u){
          // step 2: find the second vertex among the ones adjacent with the first vertex 
          for(OEdgeIterRange<Graph> er = boost::out_edges(*u, *_the_graph); er.first != er.second; ++er.first){
            const Edge& e = *er.first;
            const Vertex& v = boost::target(e, *_the_graph);
            if((*_the_graph)[v].name == name.second)
              return &e;
          }// for all incident edges of u
        }// if u exists
        // if either the first vertex or the edge has not been found, return failure
        return NULL;
      }

      //! add a new vertex with no properties and return it
      const Vertex add_vertex()
      {
        return boost::add_vertex(VertexProperty(num_vertices()), *_the_graph);
      }

      //! add a new vertex and return it
      const Vertex add_vertex(const VertexProperty& p, const bool update_infos = true)
      {
        const Vertex u = boost::add_vertex(p, *_the_graph);
        if(update_infos) infos.add_vertex(u);
        return u;
      }
      //! add a new vertex and return it; use the given name & index
      /** if no index is given, the old number of vertices is used as index */
      const Vertex add_vertex(const std::string name, const bool update_infos = true, const unsigned index = UINT_MAX)
      {
        VertexProperty vp;
        vp.index = (index != UINT_MAX ? index : boost::num_vertices(*_the_graph));
        vp.name = name;
        return add_vertex(vp, update_infos);
      }
      //! add a new vertex and return it; copy the name from vertex u
      /** the index of the new vertex will be the old number of vertices */
      const Vertex add_vertex(const Vertex& u, const bool update_infos = true)
      {
        return add_vertex(operator[](u).name, update_infos);
      }

      //! add a new edge unless it is already there; return it and whether an insertion took place
      /** the edge will have the given EdgeProperties */
      std::pair<Edge, bool> add_edge(const Vertex& u,
          const Vertex& v,
          const EdgeProperty& ep = EdgeProperty(),
          const bool update_infos = true)
      {
        std::pair<Edge, bool> e = boost::add_edge(u, v, *_the_graph);
        if(e.second) {
          (*_the_graph)[e.first] = ep;
          if(update_infos) infos.add_edge(u, v);
        }
        return e;
      }

      //! add a matching edge unless the edge already exists; return the edge and whether an insertion took place
      /** if the edge is already present, it will still be marked as a matching edge */
      std::pair<Edge, bool> add_matching_edge(const Vertex& u,
          const Vertex& v,
          const EdgeProperty& ep = EdgeProperty(),
          const bool update_infos = true)
      {
        std::pair<Edge, bool> e = add_edge(u, v, ep, update_infos);
        if(e.second) {
          // recall that an edge is considered matching edge if it has no weight
          if(ep.weight != NO_WEIGHT) (*_the_graph)[e.first].weight = NO_WEIGHT;
#ifdef NDEBUG
          matched.emplace(u, v);
          matched.emplace(v, u);
#else
          assert(matched.emplace(u, v).second);
          assert(matched.emplace(v, u).second);
#endif
        }
        return e;
      }

      //! get a copy of the name of the edge uv; independent of its presence
      EdgeName get_edge_name(const VertexPair<Graph>& uv) const
      {
        return EdgeName((*_the_graph)[uv.first].name, (*_the_graph)[uv.second].name);
      }

      //! get a copy of the name of an edge
      EdgeName get_edge_name(const Edge& e) const
      {
        const Vertex& u = boost::source(e, *_the_graph);
        const Vertex& v = boost::target(e, *_the_graph);
        return EdgeName((*_the_graph)[u].name, (*_the_graph)[v].name);
      }

      //! return whether the given vertex pair constitutes a bridge, updating infos if necessary
      bool is_bridge(const ScafVertex& u, const ScafVertex& v)
      {
        return infos.bridges.is_bridge(u, v);
      }

      //! return whether the given vertex pair constitutes a bridge without updating the infos
      bool is_bridge_const(const ScafVertex& u, const ScafVertex& v) const
      {
        return infos.bridges.is_bridge_const(u, v);
      }

      //! return whether the given edge is on an alternating cycle
      bool is_on_alternating_cycle(const ScafVertex& u, const ScafVertex& v) const
      {
        // first, if uv is not on any cycle, then it's also not on any alternating cycles
        if(infos.bridges.is_valid())
          if(infos.bridges.is_bridge_const(u, v)) return false;
        // otherwise, check whether there is an alternating u-v-path starting & ending with a non-matching edge
        return edge_on_alternating_cycle(u, v, *_the_graph, matched);
      }

      //! return the total weight of the graph
      unsigned weight() const
      {
        unsigned accu = 0;
        // iterate through the edges, summing their weight
        for(ScafEdgeIterRange er = boost::edges(*_the_graph); er.first != er.second; ++er.first)
          accu += (*_the_graph)[*er.first].weight;
        return accu;
      }

      //! return the sum of the multiplicities of the graph
      unsigned sum_multiplicities() const
      {
        unsigned accu = 0;
        // iterate through the edges, summing their weight
        for(ScafEdgeIterRange er = boost::edges(*_the_graph); er.first != er.second; ++er.first)
          accu += (*_the_graph)[*er.first].multiplicity;
        return accu;
      }

      //! get all vertices satisfying a predicate
      template<class Predicate = TruePredicate<Vertex> >
      PredicatedVertexIter<Graph, Predicate> get_vertices(const Predicate& pred = Predicate()) const
      {
        return PredicatedVertexIter<Graph, Predicate>(*_the_graph, boost::vertices(*_the_graph), pred);
      }

      //! get all edges satisfying a predicate
      template<class Predicate = TruePredicate<Edge> >
      PredicatedEdgeIter<Graph, Predicate, EdgeIter<Graph> > get_edges(const Predicate& pred = Predicate()) const
      {
        return PredicatedEdgeIter<Graph, Predicate, EdgeIter<Graph> >(*_the_graph, boost::edges(*_the_graph), pred);
      }

      //! convenience fuction to get all matching edges
      PredicatedEdgeIter<Graph, MatchingPredicate<Graph>, EdgeIter<Graph> > get_matching_edges() const
      {
        return get_edges(MatchingPredicate<Graph>(*_the_graph, false));
      }

      //! convenience fuction to get all non-matching edges
      PredicatedEdgeIter<Graph, MatchingPredicate<Graph>, EdgeIter<Graph> > get_non_matching_edges() const
      {
        return get_edges(MatchingPredicate<Graph>(*_the_graph, true));
      }

      //! get incident edges of u satisfying a predicate
      template<class Predicate = TruePredicate<Edge> >
      PredicatedEdgeIter<Graph, Predicate> get_incident(const Vertex& u, const Predicate& pred = Predicate()) const
      {
        return PredicatedEdgeIter<Graph, Predicate>(*_the_graph, boost::out_edges(u, *_the_graph), pred);
      }

      //! convenience function to get all incident non-matching edges of u
      PredicatedEdgeIter<Graph, MatchingPredicate<Graph>> get_incident_non_matching(const Vertex& u) const
      {
        return get_incident(u, MatchingPredicate<Graph>(*_the_graph, true));
      }

      //! if _the_graph is disconnected, split off & return a connected compoenent, otherwise return NULL
      Graph* const split_off_component(const ScafVertex u = NULL)
      {
        if(infos.comps.get_num() > 1){
          // prepare tools:  new ScaffoldGraph & translation maps
          ScafMatching translate, translate_back;
          // Step 1: split off a component
          Graph* const h = scaffold::split_off_component<Graph>(*_the_graph, u, &translate, &translate_back);

          // update indices and infos
          setup_vertex_indices(*h);
          setup_vertex_indices(*_the_graph);

          // Step 2: construct the new matching
          parse_matching_from_graph();

          infos.read_from_split_off_component(infos, translate);
          return h;
        } else return NULL;
      } // function


      //! merge g into our graph
      void disjoint_union(const ScaffoldGraph& g)
      {
        ScafMatching translate;
        // merge g into ourself
        copy_and_init_matching(g, &translate, false);
        update_vertex_indices();
        // update infos
        infos.update_disjoint_union(g.infos, translate);
      }



      //! print various statistics
      void print_statistics()
      {
        std::cout << boost::num_vertices(*_the_graph) << " vertices, "
          << boost::num_edges(*_the_graph)<<" edges, "
          << " insert size: "<<get_graph_property().insert_size
          << " std deviation: "<<get_graph_property().standard_deviation;
      }

      //! remove all contig edges from the graph
      /** useful to determine graph parameters relating to this graph */
      void remove_contigs()
      {
        std::cout << "removing contig edges..."<<std::endl;
        for(const VertexPair<Graph>& c: matched) delete_edge(c);
        matched.clear();
      }
    
    //! sort non_matching edges by decreasing weight
    std::vector<ScafEdge>  weight_ordered_edges()
    { 
      std::vector<ScafEdge > v;
      for(auto e = this->get_non_matching_edges(); e; e++)
	v.push_back(*e);
    
      sort(v.begin(),v.end(), [&](const ScafEdge &a,const ScafEdge &b)
			      {return (*this)[a].weight > (*this)[b].weight;}); //  and apply sort by weight (parallel !)*/
      return v;
    }


  };

}// namespace

