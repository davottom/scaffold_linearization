
/** \file unordered_pair.hpp
 * implementation of an unordered pair (whose main difference to a normal pair is operator== and hash_value)
 */


#ifndef UNORDERED_PAIR
#define UNORDERED_PAIR

#include <utility>
#include <functional>

namespace std{
  
  //! an unordered pair
  template<class ElementA, class ElementB>
  struct unordered_pair: public pair<ElementA, ElementB>
  {
    using pair<ElementA, ElementB>::pair;

    //! two unordered pairs {u,v} and {x,y} are equal if (u=x & v=y) or (u=y & v=x)
    bool operator==(const unordered_pair<ElementA, ElementB>& up) const
    {
      return ((this->first == up.first) && (this->second == up.second))
        || ((this->first == up.second) && (this->second == up.first));
    }
  };

  //! compute the hash value of an unordered pair by adding the hash values of its elements
  template<class ElementA, class ElementB>
  size_t hash_value(const unordered_pair<ElementA, ElementB>& up)
  {
    return std::hash<ElementA>{}(up.first) + std::hash<ElementB>{}(up.second);
  }


};

#endif

