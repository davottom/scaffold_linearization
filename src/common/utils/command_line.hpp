

/** \file command_line.hpp
 * command-line interface
 * handling parameters and options
 */

#pragma once

#include <string>

#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

#include "utils/utils.hpp"
#include "utils/exceptions.hpp"


namespace po = boost::program_options;

//! options that configure the scaffolder
struct GeneralOptions{
    //! filename of the input graph
    std::string in_graph_filename;

    //! filename of the input sequence file in fasta format
    std::string in_seq_filename;

    //! filename of a file to export the cplex model to
    std::string out_model_filename;

    //! filename of a file to export the solution graph to
    std::string out_graph_filename;

    //! filename of a file to export solution sequences to
    std::string out_seq_filename;

    //! cut-off threshold: remove all links between contigs whose weight is strictly below this threshold
    unsigned cut_off;

    //! maximum number of objects (paths or cycles) that a solution graph should consist of
    unsigned objects;
    //! maximum number of paths that a solution graph should consist of
    unsigned paths;
    //! maximum number of cycles that a solution graph should consist of
    unsigned cycles;

    //! skip scaffolding process and procede to the linearization of a solution graph.
    bool skip_scaffolding;

    //! use greedy for scaffolding
    bool greedy;

    //! Type of completion used in greedy algorithm
    enum {Complete, Bridge, Block, None} greedy_type;

    //! population size for population based metaheuristics
    int pop_size;

    //! to activate fast convergence algorithm and get a good solution faster
    bool augmented_fct_obj;

    //! To define the time as the stopping criterion
    bool time;

    //! To specify how much second you want the algorithm to run
    unsigned time_amount;

    //! To define the number of generation as the stopping criterion
    bool generation;

    //! specify how many generation you wanna wait before the algorithm stop running
    unsigned generation_amount;

    //! bool incidating whether multiplicities should be taken into account
    bool ignore_multiplicities;

    //! pairwise overlap all contigs to find non-contig edges that have been missed
    bool find_missed_pairs;

    //! return true iff the arguments make sense
    /** f.ex. we need a fasta file with contig sequences if we want to output the sequences of the resulting contigs */
    void sanity_check() const
    {
      if((in_graph_filename == "") || !file_exists(in_graph_filename))
        throw except::invalid_options("I have no input file to work with, use --fn <file> to give me a scaffold graph.");
      if(out_seq_filename != "" && !file_exists(in_seq_filename))
        throw except::invalid_options("I want to output fasta sequences to '" + out_seq_filename + "' but I cannot open the sequence database '" + in_seq_filename + "' (given with --is) for reading");
      if(find_missed_pairs && !file_exists(in_seq_filename))
        throw except::invalid_options("I want to pairwise overlap contigs but I cannot open the sequence database '" + in_seq_filename + "' (given with --is) for reading");

    }
};

//! return the value of an option or the given default if the value has not been supplied
template<typename T>
const T& get_option(const po::variables_map& vm, const std::string& option_name, const T& default_value){
  if(vm.count(option_name))
    return vm[option_name].as<T>();
  else
    return default_value;
}

po::options_description initialize_options(const char* const program_name)
{
  // Declare the supported options.
  po::options_description desc((std::string)"syntax: " + program_name + " [options] --fn <scaffold graph file>\nwhere valid options are");
  desc.add_options()
          ("help", "produce help message")
          ("fn", po::value<std::string>(), "file to read scaffold graph from")
          ("is", po::value<std::string>(), "file to read contig sequences from (required for --os)")
          ("os", po::value<std::string>(), "file to output solution sequences to (fasta format) (requires --is)")
          ("og", po::value<std::string>(), "file to output solution graph to (dot format)")
          ;
  return desc;
}

void add_scaffolding_options(po::options_description& desc) {
    desc.add_options()
            ("o", po::value<unsigned>(), "allowed number of objects (paths or cycles) (default: 6)")
            ("p", po::value<unsigned>(), "allowed number of paths (default: #objects (see --o))")
            ("c", po::value<unsigned>(), "allowed number of cycles (default: #objects (see --o))")
            ("co", po::value<unsigned>(), "cut off all edges whose weight is strictly below the argument (default: 0)")
            ("fo",
             "before scaffolding, scan contigs to find overlaps and insert missing non-contig ""edges accordingly (default: false)")
            ("nm",
             "ignore multiplicities, if the input graph is a solution graph, then all unclean matching edges are ambiguous");
}



po::variables_map parse_arguments(const int argc, const char** argv, const po::options_description& desc)
{
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);
  return vm;
}

//! process the program options given on the command line
GeneralOptions handle_options(const po::variables_map& vm, po::options_description& desc)
{
  if(vm.count("help")) { std::cout << desc << "\n"; exit(EXIT_SUCCESS); }

  GeneralOptions options;
  options.in_graph_filename  = get_option<std::string>(vm, "fn", "");
  options.out_graph_filename = get_option<std::string>(vm, "og", "");
  options.in_seq_filename    = get_option<std::string>(vm, "is", "");
  options.out_seq_filename   = get_option<std::string>(vm, "os", "");
  options.out_model_filename = get_option<std::string>(vm, "om", "");

  options.objects         = get_option<unsigned>(vm, "o", 6);
  options.paths           = get_option<unsigned>(vm, "p", options.objects);
  options.cycles          = get_option<unsigned>(vm, "c", options.objects);
  options.cut_off         = get_option<unsigned>(vm, "co", 0);

  options.find_missed_pairs = vm.count("fo");

  options.skip_scaffolding = vm.count("ln");

  options.ignore_multiplicities = vm.count("nm");

  auto opt_greedy = get_option<std::string>(vm, "greedy", "");
  options.greedy_type= GeneralOptions::None;
  options.greedy=false;
  if(opt_greedy!="") {
    options.greedy=true;
    if(opt_greedy=="complete") options.greedy_type = GeneralOptions::Complete;
    else if(opt_greedy=="bridge") options.greedy_type = GeneralOptions::Bridge;
    else if(opt_greedy=="block") options.greedy_type = GeneralOptions::Block;
    else options.greedy=false;
  }

  options.pop_size = get_option<unsigned>(vm, "pop", 50);
  options.time_amount = get_option<unsigned>(vm, "time", 1200);
  options.generation_amount = get_option<unsigned>(vm, "gen", 30);
  options.augmented_fct_obj = vm.count("afct");
  options.generation = vm.count("gen");
  options.time = vm.count("time");

 if(!options.time && !options.generation)
     options.generation = true;




  try{
    options.sanity_check();
  } catch(except::invalid_options& e){
    std::cout << "Command line option error: "<<e.what() << std::endl;
    std::cout << desc << std::endl;
    exit(EXIT_FAILURE);
  }

  return options;
}
