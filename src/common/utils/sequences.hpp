
#pragma once

#define CYCLIC_SEQUENCE_INDICATOR "(c)"
#define REVERSE_SEQUENCE_INDICATOR "(rev)"

#define BASES "ABCDEFGHIJKLMNOPQRSTUVWXYZ*-"

// define bases such that COMPLEMENTARY_BASES[i] is the complement of COMPLEMENTARY_BASES[i+1]
const char* COMPLEMENTARY_BASES = "ATAUCGRYMK";


namespace scaffold {

  // return the index behind the real name if name is "(real_name)(rev)" and 0 otherwise
  unsigned is_reversed_sequence(const std::string& name)
  {
    unsigned start_of_rev_indicator = name.length() - strlen(REVERSE_SEQUENCE_INDICATOR);
    const unsigned end_of_name = start_of_rev_indicator - 1;
    if(name[0] != '(') return 0;
    if(name[end_of_name] != ')') return 0;
    if(name.substr(start_of_rev_indicator) != REVERSE_SEQUENCE_INDICATOR) return 0;
    // check that the bracket at index 0 really does belong to the one at end_of_name
    unsigned level = 1;
    const char* name_cptr = name.c_str();
    for(unsigned i = 1; i != end_of_name; ++i){
      const char c = name_cptr[i];
      switch(c){
        case '(': ++level; break;
        case ')': --level; break;
      }
      if(level == 0) return 0;
    }
    return start_of_rev_indicator;
  }

  // change name as to indicate that it's referring to a reversed sequence
  void indicate_reversal(std::string& name)
  {
    // if the sequence is already reversed, just remove the reverse indicator, otherwise, add a reverse indicator
    const unsigned start_of_rev_indicator = is_reversed_sequence(name);
    if(start_of_rev_indicator){
      name.erase(start_of_rev_indicator-1);
      name.erase(0, 1);
    } else name = '(' + name + ')' + REVERSE_SEQUENCE_INDICATOR;
  }

  //! get the reversed version of the contig name 'name'
  std::string get_reversed_name(const std::string& name)
  {
    std::string result = name;
    indicate_reversal(result);
    return result;
  }

  // get the complement of the given base, or 'N' if it does not have a complement
  char get_complement(const char base, const bool use_uracil = false)
  {
    const char* complement_index = std::strchr(COMPLEMENTARY_BASES, base);
    if(complement_index != NULL){
      const int offset = (complement_index - COMPLEMENTARY_BASES);
      assert(offset >= 0);
      const char result = COMPLEMENTARY_BASES[(offset % 2) ? offset - 1 : offset + 1];
      if((result == 'T') && use_uracil) return 'U';
      return result;
    } else return 'N';
  }

  // reverse complement a given string in place
  void reverse_complement_inplace(std::string& sequence, const bool use_uracil = false)
  {
    const int seq_len = sequence.length();
    const int last_index = seq_len - 1;
    for(int i = 0; 2 * i < last_index; ++i){
      const char swap_base = sequence[i];
      sequence[i] = get_complement(sequence[last_index - i], use_uracil);
      sequence[last_index - i] = get_complement(swap_base, use_uracil);
    }
    // reverse the middle if the sequence length is even
    if(seq_len % 2) sequence[last_index / 2] = get_complement(sequence[last_index / 2], use_uracil);
  }

  // return the reverse complement of a sequence
  std::string reverse_complement(const std::string& sequence, const bool use_uracil = false)
  {
    std::string out(sequence);
    reverse_complement_inplace(out, use_uracil);
    return out;
  }


  // return the reverse complement of a sequence, modifying its name to account for it
  std::string named_reverse_complement(const std::string& sequence, std::string& name, const bool use_uracil = false)
  {
    std::string out(sequence);
    reverse_complement_inplace(out, use_uracil);
    indicate_reversal(name);
    return out;
  }


}
