

#pragma once

#include "utils/sequences.hpp"
#include "utils/graph_typedefs.hpp"

namespace scaffold {

  // an oriented sequence is a sequence that starts at the vertex with the designated name
  struct OrientedSequence {
    std::string sequence;
    VertexName start_vertex;

    OrientedSequence() {}
    OrientedSequence(const OrientedSequence& os):
      sequence(os.sequence), start_vertex(os.start_vertex) {}
    OrientedSequence(const std::string& _sequence, const std::string& _start = VertexName()):
      sequence(_sequence), start_vertex(_start) {}


    void reverse_complement(const VertexName& new_start_vertex = VertexName(), const bool use_uracil = false)
    {
      start_vertex = new_start_vertex;
      reverse_complement_inplace(sequence, use_uracil);
    }
  };

  // an oriented sequence with a use counter
  struct UseCountedOrientedSequence : public OrientedSequence {
    unsigned counter = 1;

    using OrientedSequence::OrientedSequence;
    UseCountedOrientedSequence(const OrientedSequence& os, const unsigned count = 1):
      OrientedSequence(os), counter(count) {}

    UseCountedOrientedSequence(const std::string& seq, const VertexName& start, const unsigned count):
      OrientedSequence(seq, start), counter(count) {}
  };

  // a SequenceMap assigns each contig name a sequence of base pairs
  typedef boost::unordered_map<std::string, OrientedSequence> SequenceMap;
  typedef boost::unordered_map<std::string, UseCountedOrientedSequence> MultiSequenceMap;
  typedef std::pair<std::string, std::string> NamedSequence;
  typedef std::pair<std::string, OrientedSequence> NamedOrientedSequence;
  typedef std::list<NamedSequence> NamedSequenceList;





  struct SequenceStatistics {
    typedef std::list<unsigned> LenList;

    unsigned num_scaffolds = 0;
    unsigned total_length = 0;
    LenList lengths;

    // comptue Nx for any x (for example x = 50 ^^)
    unsigned Nx(const float x) const {
      unsigned accu = 0;
      LenList::const_iterator i = lengths.begin();

      assert(total_length > 0);
      assert((0.0f < x) && (x < 100.0f));
      const unsigned target = (x / 100.0f) * (float)total_length;
      while(accu < target) accu += *i++;
      std::advance(i, -1);
      assert(i != lengths.end());
      return *i;
    }
    
    unsigned largest_scaffold() const { 
      assert(num_scaffolds > 0); 
      return *lengths.rbegin(); 
    }

    void analyze(const SequenceMap& seq_map)
    {
      num_scaffolds = seq_map.size();
      
      // compute total length and largest scaffold
      total_length = 0;
      for(const auto& named_seq: seq_map){
        const std::string& seq = named_seq.second.sequence;
        const unsigned length = seq.size();
        lengths.push_back(length);
        total_length += length;
      }
      lengths.sort();
    }

    void analyze(const MultiSequenceMap& seq_map)
    {
      // compute total length and largest scaffold
      total_length = 0;
      num_scaffolds = 0;
      for(const auto& named_seq: seq_map){
        const std::string& seq = named_seq.second.sequence;
        const unsigned multi = named_seq.second.counter;
        const unsigned length = seq.size();
        for(unsigned i = 0; i < multi; ++i) lengths.push_back(length);
        total_length += multi * length;
        num_scaffolds += multi;
      }
      lengths.sort();
    }


    SequenceStatistics() {}
    SequenceStatistics(const SequenceMap& seq_map) { analyze(seq_map); }
    SequenceStatistics(const MultiSequenceMap& seq_map) { analyze(seq_map); }

    void print(std::ostream& out = std::cout) const
    {
      out << "sequence statistics:\n\t#scaffolds:\t"<<num_scaffolds
              <<"\n\ttotal len:\t"<<total_length
              <<"\n\tlargest:\t"<<largest_scaffold()
              <<"\n\tN50:\t\t"<<Nx(50)<<std::endl;
      if(num_scaffolds < 20){
        out << "lengths:\t{";
        for(const unsigned len: lengths)
          out << len << " ";
        out << "}"<<std::endl;
      }
    }
  };

} // namespace

