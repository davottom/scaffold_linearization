#pragma once

#include <cmath>
#include "utils/utils.hpp"
#include "utils/hamming_distance.hpp"
#include "utils/levenstein_distance.hpp"
#include "utils/alignment_options.hpp"
#include "utils/normal_distribution.hpp"
#include "utils/scaffolding_typedefs.hpp"
#include "utils/scaffold_graph.hpp"




namespace scaffold{

  // an alignment has an overlap and a score (its probability)
  class Alignment : public std::pair<int, float>
  {
    using std::pair<int, float>::pair;
  public:
    OrientedSequence consensus_sequence;

    bool operator>(const Alignment& worse) const
    {
      return second > worse.second;
    }
    bool operator>=(const Alignment& worse) const
    {
      return second >= worse.second;
    }

  };

#warning "TODO: find a good alignment score! For now, we are using an adaptation of FLASH (http://ccb.jhu.edu/software/FLASH/): MAXIMIZE (1 - average Levenstein distance) * (probability of the overlap (assuming gaussian distribution around the mean overlap))"

  // compute the score of an overlap, given the length of the overlap and the distance
  float compute_overlap_score(const LengthType& overlap, const float& string_distance, const UncertainLength& len)
  {
    assert(overlap >= 0);
    // we weight the probability of picking from [-overlap -.5, -overlap + .5]
    return len.prob_of_picking_around(-overlap) * (1.0f - (string_distance / overlap));
  }

  float theoretical_max_score(const UncertainLength& len)
  {
    return compute_overlap_score(-len.mean, 0, len);
  }


  // a container of alignment bounds in the contig strings
  struct alignment_bounds
  {
    const unsigned c1_len;
    const unsigned c2_len;
    unsigned lower_index;
    unsigned upper_index;
    unsigned max_distance;

    alignment_bounds(const unsigned _c1_len, const unsigned _c2_len, const LengthType& overlap, const AlignmentOptions& opts):
      c1_len(_c1_len),
      c2_len(_c2_len)
    {
      assert(overlap < 0);
      // the center index of c1 around which to shift c2
      int _center = _c1_len + overlap;
      if(_center < 0) _center = c1_len / 2;
      
      int _lower_index = _center - opts.max_overlap_radius;
      int _upper_index = _center + opts.max_overlap_radius;

      // correct the lower and upper indices
      _lower_index = std::max(_lower_index, 0);
      _upper_index = std::min(_upper_index, (int)(c1_len - opts.min_overlap + 1));

      // makes no sence to check an index from which all alignments must exceed the max. mismatch percentage
      _lower_index = std::max(_lower_index, (int)(c1_len - c2_len * (1 + opts.max_mismatch_percentage)));
        
      assert((_upper_index >= 0) && (_lower_index >= 0));
      upper_index = _upper_index;
      lower_index = _lower_index;
      max_distance = opts.max_mismatch_percentage * std::min(upper_index - lower_index, c2_len);

      DEBUG4(std::cout << "aligning strings of length "<<c1_len<<" and "<<c2_len<<" around overlap of "<<overlap<<" - max dist "<<max_distance<<" & window "<<lower_index<<":"<<upper_index<<" (="<<upper_index - lower_index<<")"<<std::endl);
    }

  };

  // get the best alignment of the two strings c1 & c2 that are supposed to overlap near the mean of "suggested":
  // ====c1======
  //          =====c2====
  // --> overlap = 3
  // we try shifting c2 around an offset of -overlap and return the best scoring Alignment
  template<class Distance_Calculator>
  Alignment get_best_overlap(const std::string& c1,
                             const std::string& c2,
                             const UncertainLength& suggested,
                             const AlignmentOptions& opts,
                             const bool compute_consensus = false)
  {
    assert(suggested.mean < 0);
    if(c1.length() < opts.min_overlap) return Alignment(0, 0);
    const alignment_bounds bounds(c1.length(), c2.length(), suggested.mean, opts);

    // compute scores for each index of c1 where to put c2
    Distance_Calculator dist_calc(c1, c2, bounds.lower_index, bounds.upper_index, bounds.max_distance);
    // index is the current index of c1
    Alignment best(0, 0);
    for(unsigned index = bounds.lower_index; index < bounds.upper_index; ++index){
      const unsigned overlap = bounds.c1_len - index;
      const unsigned length = std::min(overlap, bounds.c2_len);
#warning "TODO: iterate only over non-infinite distances using an iterator that we get from dist_calc"
      const unsigned distance = dist_calc.get_distance(index);
      
      if(distance < length){
        // update the best scoring alignment
        Alignment tmp(overlap, compute_overlap_score(length, distance, suggested));
        if(tmp > best) best = tmp;

        DEBUG4(std::cout << "alignment w/ overlap "<<overlap<<": ");
        DEBUG6(std::cout <<std::endl <<c1<<std::endl;
          for(unsigned i = 0; i < index; ++i) std::cout << "_"; std::cout << c2<<std::endl);
        DEBUG4(std::cout << std::setprecision(4) << " average distance "<<((float)distance)/length<<" (="<<distance<<"/"<<length<<") and thus scores "<<tmp.second<<std::endl);
      }
    }
    if(compute_consensus && (best.second > 0))
      best.consensus_sequence.sequence = dist_calc.get_consensus(bounds.c1_len - best.first);
    return best;
  }


  // contract a non-matching edge uv, merging the contigs ux & vy & return the merged contig edge
  const ScafEdge contract_non_contig(ScaffoldGraph& sg,
                                     const ScafVertex& x,
                                     const ScafVertex& u, 
                                     const ScafVertex& v,
                                     const ScafVertex& y,
                                     const int total_length,
                                     const unsigned uv_multi,
                                     const bool update_indices = true)
  {
#warning "TODO: care for the case that ux & vy are to be merged and xy is already a non-contig edge!!!"
    if(sg.adjacent(x, y)) sg.delete_edge(x,y);
    
    sg.delete_vertex(u, false);
    sg.delete_vertex(v, update_indices);
    const auto add_edge_result = sg.add_matching_edge(x, y, ScafEdgeProperty(NO_WEIGHT, total_length, uv_multi));
    assert(add_edge_result.second);
    return add_edge_result.first;
  }


  // merge two oriented sequences seq1 & seq2 AFTER reorienting them according to outer1 & outer2
  // NOTE: outer1 has to be an endpoint of the contig corresponding to named_seq1 (and the same for 2)
  // such that the result is FROM outer1 TO outer2
  void reorient_and_merge(const std::pair<std::string, OrientedSequence>& named_seq1,
                          const std::pair<std::string, OrientedSequence>& named_seq2,
                          const VertexName& outer1,
                          const VertexName& outer2,
                          const unsigned overlap,
                          std::string& new_seq,
                          std::string& new_seq_name,
                          const bool use_uracil)
  {
    // step 1: add the name & (reversed) sequence of named_seq1
    OrientedSequence seq1(named_seq1.second);
    if(seq1.start_vertex != outer1) {
      seq1.reverse_complement(outer1, use_uracil);
      new_seq_name = get_reversed_name(named_seq1.first);
    } else new_seq_name = named_seq1.first;

    new_seq_name += "+";
    
    // step 1: add the name & (reversed) sequence of named_seq2
    OrientedSequence seq2(named_seq2.second);
    if(seq2.start_vertex == outer2) {
      seq2.reverse_complement(VertexName(), use_uracil);
      new_seq_name += get_reversed_name(named_seq2.first);
    } else new_seq_name += named_seq2.first;
    DEBUG5(std::cout << "merging seqeunces "<<named_seq1.first<<" (len "<< named_seq1.second.sequence.length() <<") & "<< named_seq2.first<< " (len "<< named_seq2.second.sequence.length()<<") with overlap "<<overlap<<std::endl;)
    
    new_seq = merge_strings(seq1.sequence, seq2.sequence, overlap);
    
    DEBUG5(std::cout << "merged successfully  to "<<new_seq_name<<" (len "<< new_seq.length()<<")"<<std::endl;)
  }


  //! an aligner that takes a path x====u-----v=====y where uv has negative length and aligns x===u & v===y around this offset
  class ScafContigAligner
  {
  protected:
    const ScaffoldGraph& sg;
    const SequenceMap& sequences;
    const AlignmentOptions& opts;

    const ScafVertex u;
    const ScafVertex v;
    const ScafEdge ux;
    const ScafEdge vy;
    const LengthType ux_length;
    const UncertainLength uv_length;
    const LengthType vy_length;
    const MultiType uv_multiplicity;

    Alignment best;
    SequenceMap::const_iterator u_contig_it, v_contig_it;

  public:

    //! constructor if we don't have the actual edge, but just the two endpoints
    /** in fact, the edge is not needed to exist in the graph for this constructor;
     *  if _uv_length is not set, its length-distribution is ignored (set to the flat distribution around -min_overlap - max_radius) */
    ScafContigAligner(const ScaffoldGraph& _sg,
                      const SequenceMap& _sequences,
                      const AlignmentOptions& _opts,
                      const ScafVertex& _u,
                      const ScafVertex& _v,
                      const UncertainLength& _uv_length = ReadDistribution(0),
                      const MultiType& _uv_multiplicity = 0):
      sg(_sg),
      sequences(_sequences),
      opts(_opts),
      u(_u),
      v(_v),
      ux(sg.incident_matching_edge(u)),
      vy(sg.incident_matching_edge(v)),
      ux_length(sg[ux].length),
      uv_length(_uv_length.is_valid() ? _uv_length : FLAT_DISTRIBUTION(- opts.max_overlap_radius - opts.min_overlap)),
      vy_length(sg[vy].length),
      uv_multiplicity(_uv_multiplicity ? _uv_multiplicity : std::min(sg[ux].multiplicity, sg[vy].multiplicity))
    {
      DEBUG5(std::cout << "aligning contig of "<<sg[u]<<" with contig of "<<sg[v]<<" with mean overlap of "<<-uv_length.mean<<std::endl);
    }

    //! constructor if we have the actual edge uv joining the two contigs
    ScafContigAligner(const ScaffoldGraph& _sg,
                      const SequenceMap& _sequences,
                      const AlignmentOptions& _opts,
                      const ScafEdge& _uv,
                      const bool consider_negative_certain = true):
      ScafContigAligner(_sg, _sequences, _opts, _sg.source(_uv), _sg.target(_uv), _sg.get_uncertain_length(_uv, consider_negative_certain), _sg[_uv].multiplicity)
    {}

    UncertainLength total_length() const
    {
      return uv_length + ux_length + vy_length;
    }
    // an alignment task is insane if:
    // 1. its total length is probably negative
    // 2. we trust the assembler but the task tries to align one of xy and vy INTO the other
    //    (that is uv_length is so negative that it pulls one of the contigs into the other)
    bool sanity_check() const
    {
      // if we can put contigs into contigs, we need xu + uv + vy > 0
      // if we cannot put contigs into contigs, we need uv + min(ux, vy) > 0
      const LengthType target_length = opts.contig_in_contig ?
                                        -ux_length - vy_length :
                                        -std::min(ux_length, vy_length);
      const float prob = uv_length.prob_of_picking_at_least(target_length);
      DEBUG5(std::cout << "new edge needs length at least "<< target_length
                       <<" and is distributed around "<<uv_length.mean
                       <<"; prob is "<<100.0*prob<<"%"<<std::endl);
      return prob >= opts.min_length_probability;
    }

    bool is_overlap() const 
    {
      return (uv_length < 0);
    }
    const Alignment& get_best_alignment() const
    {
      return best;
    }
    const Alignment get_threshold_alignment() const
    {
      return Alignment(0, theoretical_max_score(uv_length) * (opts.threshold_probability_percentage / 100.0));
    }

    //! return the highest scoring alignment
    /** if compute_consensus is set, also compute the consensus string according to the employed distance measure */
    void compute_best_alignment(const bool compute_consensus = false)
    {
      DEBUG5(std::cout << sg.get_edge_name(ux) << " is linked to contig '"<<sg[ux].contig_name<<"'"<<std::endl);

      const std::string& ux_cname = sg[ux].contig_name;
      u_contig_it = sequences.find(ux_cname);
      const OrientedSequence& u_contig = u_contig_it->second;
      DEBUG5(std::cout << "start vertex of "<<sg[ux].contig_name<<" is "<<u_contig.start_vertex<<std::endl);      
      const bool ux_seq_starts_at_u = (u_contig.start_vertex == sg[u].name);
      DEBUG5(if(ux_seq_starts_at_u) std::cout << "reversing "<<ux_cname<<" as it starts at "<<sg[u].name<<std::endl);
      const std::string ux_seq( ux_seq_starts_at_u ? reverse_complement(u_contig.sequence, opts.uracil) : u_contig.sequence);
      
      const std::string& vy_cname = sg[vy].contig_name;
      v_contig_it = sequences.find(vy_cname);
      const OrientedSequence& v_contig = v_contig_it->second;
      DEBUG5(std::cout << "start vertex of "<<v_contig_it->first<<" is "<<v_contig.start_vertex<<std::endl);
      const bool vy_seq_starts_at_v = (v_contig.start_vertex == sg[v].name);
      DEBUG5(if(!vy_seq_starts_at_v) std::cout << "reversing "<<vy_cname<<" as it does not start at "<<sg[v].name<<std::endl);
      const std::string vy_seq( vy_seq_starts_at_v ? v_contig.sequence : reverse_complement(v_contig.sequence, opts.uracil));
      
      DEBUG5(std::cout << "getting best overlap..."<<std::endl);
      best = opts.hamming_distance ? 
        get_best_overlap<Hamming_Distance_Calculator>(ux_seq, vy_seq, uv_length, opts, compute_consensus) :
        get_best_overlap<Levenstein_Distance_Calculator>(ux_seq, vy_seq, uv_length, opts, compute_consensus);

      if(compute_consensus && (best.second > 0)) best.consensus_sequence.start_vertex = sg[sg.matched_with(u)].name;

      DEBUG5(
        const Alignment threshold = get_threshold_alignment();
        if(best >= threshold){
          std::cout << std::setprecision(4)<<"best alignment of ("<<sg[sg.target(ux)]<<","<<sg[u]<<") & "<<sg.get_edge_name(vy)<<" is good: "
                    << best.first << "bp before the end of c1 - score "<<best.second
                    <<" ("<<(unsigned)(100.0*(double)best.second/theoretical_max_score(uv_length))<<"% of max) vs. "
                    << threshold.second<<" ("<<opts.threshold_probability_percentage<<"% of max)"<<std::endl;
          const int size = std::min((int)vy_seq.size(), best.first);
          std::cout<<"..."<<ux_seq.substr(ux_seq.size() - best.first, size)<<std::endl<<"___"<<vy_seq.substr(0, best.first)<<"..."<<std::endl;
        }
      );
    }

  };


  class ScafContigMerger : public ScafContigAligner
  {
    ScaffoldGraph& mutable_sg;
    SequenceMap& mutable_sequences;

  public:
    //! constructor if we don't have the actual edge, but just the two endpoints
    /** in fact, the edge is not needed to exist in the graph for this constructor */
    ScafContigMerger(ScaffoldGraph& _sg,
                      SequenceMap& _sequences,
                      const AlignmentOptions& _opts,
                      const ScafVertex& _u,
                      const ScafVertex& _v,
                      const UncertainLength& _uv_length,
                      const unsigned _uv_multiplicity,
                      const ReadDistribution& _dist = ReadDistribution(0)):
      ScafContigAligner(_sg, _sequences, _opts, _u, _v, _uv_length, _uv_multiplicity),
      mutable_sg(_sg),
      mutable_sequences(_sequences)
    {}
    //! constructor if we have the actual edge uv joining the two contigs
    ScafContigMerger(ScaffoldGraph& _sg,
                      SequenceMap& _sequences,
                      const AlignmentOptions& _opts,
                      const ScafEdge& _uv,
                      const bool consider_negative_certain = true):
      ScafContigAligner(_sg, _sequences, _opts, _uv, consider_negative_certain),
      mutable_sg(_sg),
      mutable_sequences(_sequences)
    {}

    //TODO: contracting edges is not really an alignment task... move this someplace else?
    // if we have a convincing alignment, merge the two contigs using the alignments shift value,
    void contract_edge(const OrientedSequence& replacement_sequence, StringCount* const use_count = NULL)
    {
      const unsigned replacement_length = replacement_sequence.sequence.length();
      // step 1: merge the mutable_sequences
      const ScafVertex x = sg.matched_with(u);
      const std::string& ux_cname = u_contig_it->first;
      const OrientedSequence& ux_seq = u_contig_it->second;

      const ScafVertex y = sg.matched_with(v);
      const std::string& vy_cname = v_contig_it->first;
      const OrientedSequence& vy_seq = v_contig_it->second;

      assert(replacement_sequence.start_vertex == sg[x].name || replacement_sequence.start_vertex == sg[y].name);

      DEBUG4(std::cout << "contracting 3-path "<<sg[x].name<<" ===[len: "<<sg[ux].length<<"]=== "<<sg[u].name<<" ---["<<sg[sg.find_edge(u,v).first]<<"]--- "<<sg[v].name<<" ===[len: "<<sg[vy].length<<"]=== "<<sg[y].name<<std::endl);
      //reorient_and_merge(*u_contig_it, *v_contig_it, new_start, new_end, best.first, new_seq, new_seq_name);

      // construct the new sequence name
      const std::string new_seq_name = (replacement_sequence.start_vertex == sg[x].name) ? 
        (
          (ux_seq.start_vertex == sg[u].name ? get_reversed_name(ux_cname) : ux_cname) + '+' +
          (vy_seq.start_vertex != sg[v].name ? get_reversed_name(vy_cname) : vy_cname)
        ) : (
          (vy_seq.start_vertex == sg[v].name ? get_reversed_name(vy_cname) : vy_cname) + '+' +
          (ux_seq.start_vertex != sg[u].name ? get_reversed_name(ux_cname) : ux_cname)
        );
      

      if(use_count){
        DEBUG5(std::cout << "new name is "<<new_seq_name<<" decreasing use-count of "<<ux_cname<<" and "<<vy_cname<<std::endl);
        for(const auto& cname: {ux_cname, vy_cname}){
          const auto cname_it = use_count->find(cname);
          assert(cname_it != use_count->cend());
          DEBUG5(std::cout << cname_it->first << " is used "<<cname_it->second<<" times"<<std::endl);
          if(cname_it->second == 1) {
            mutable_sequences.erase(cname_it->first);
            use_count->erase(cname_it);
          } else cname_it->second--;
        }
      }


      // step 2: merge the contigs
      DEBUG4(std::cout << "new length: "<< ux_length << " + " << uv_length << " + " << vy_length << " = "<<total_length()<<" (seq length: "<<replacement_sequence.sequence.length()<<")"<<std::endl);
      const ScafEdge new_e = contract_non_contig(mutable_sg, x, u, v, y, replacement_length, uv_multiplicity);
      DEBUG5(std::cout << "new edge "<<sg.get_edge_name(new_e)<<" has sequence '"<<new_seq_name<<"' starting at "<<replacement_sequence.start_vertex<<": "<<replacement_sequence.sequence<<std::endl);
      // and assign the new sequence to it
      mutable_sg[new_e].contig_name = new_seq_name;

      const bool emplaced = mutable_sequences.DEEP_EMPLACE(new_seq_name, replacement_sequence).second;
      if(use_count){
        if(emplaced)
          use_count->DEEP_EMPLACE(new_seq_name, 1);
        else
          (*use_count)[new_seq_name]++;
      }
    }

  };

}


