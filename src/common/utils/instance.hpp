

/** \file instance.hpp
 * instance class enriching ScaffoldGraph with solutions
 */

#ifndef INSTANCE_HPP
#define INSTANCE_HPP

#include <list>
#include <algorithm>

// boosts copy.hpp is broken, see https://svn.boost.org/trac/boost/ticket/10895
//#include <boost/graph/copy.hpp>

#include "utils/graph_typedefs.hpp"
#include "utils/graph_utils.hpp"
#include "utils/graph_infos.hpp"
#include "utils/solution.hpp"
#include "utils/scaffolding_typedefs.hpp"
#include "utils/scaffold_graph.hpp"

namespace scaffold{

  //! an instance is a ScaffoldGraph with given path- & cycle- numbers and a solution to keep track of deleted edges
  class Instance: public ScaffoldGraph
  {
  public:
    typedef Solution<> solution_type;

    unsigned num_paths;     //!< max number of paths in solutions
    unsigned num_cycles;    //!< max number of cycles in solutions
    unsigned num_objects;   //!< max number of objects (sum of paths & cycles) in solutions
    solution_type solution; //!< a partial solution
  public:

    // ==== constructors ====

    //! construct an empty solution solution
    Instance():
      ScaffoldGraph(), solution(){}

    //! construct an empty solution solution
    Instance(const unsigned np, const unsigned nc, const unsigned no = UINT_MAX):
      ScaffoldGraph(), num_paths(np), num_cycles(nc), num_objects(no), solution(){}

    //! construct an instance using the given pointer _g as graph - no copy is performed
    Instance(RawScaffoldGraph* const _g, const unsigned np, const unsigned nc, const unsigned no = UINT_MAX):
      ScaffoldGraph(_g), num_paths(np), num_cycles(nc), num_objects(no), solution() {}

    //! construct an instance using the given ScaffoldGraph, which is copied
    /** update_infos can be used to recompute all graph infos */
    Instance(const ScaffoldGraph& _g, const unsigned np, const unsigned nc, const unsigned no = UINT_MAX, const bool update_infos = false):
      ScaffoldGraph(_g, NULL, update_infos), num_paths(np), num_cycles(nc), num_objects(no), solution() {}

    //! copy constructor creating a copy of the graph and returning the translate map
    /** update_infos can be used to recompute all graph infos */
    Instance(const Instance& I, ScafMatching* const translate = NULL, const bool update_infos = false):
      ScaffoldGraph(I, translate, update_infos),
      num_paths(I.num_paths),
      num_cycles(I.num_cycles),
      num_objects(I.num_objects),
      solution(I.solution)
    {}
   
    // ==== graph modification ====

    //! split off one connected component from I and return an instance consisting only of it. If we have just one component, do nothing & return NULL
    /** NOTE: in case of a split-off, the result is guaranteed connected, while the remainder of the instance might not be */
    Instance* split_off_component()
    {
      RawScaffoldGraph* const h = ScaffoldGraph::split_off_component();
      if(h) 
        return new Instance(h, num_paths, num_cycles, num_objects);
      else
        return NULL;
    } // function

    //! disjointly merge I into us, merging partial solutions along the way
    void disjoint_union(const Instance& I)
    {
      ScaffoldGraph::disjoint_union(I);
      solution.combine_disjoint_union(I.solution);
    }

    //! delete an edge e and add the deletion to the solution if requested
    void delete_edge(const ScafEdge& e, const bool register_in_solution = false)
    {
      if(register_in_solution) solution.add_edge_global(get_edge_name(e), operator[](e).weight);
      ScaffoldGraph::delete_edge(e);
    }// function

    //! delete the edge (u,v) and add this deletion to the solution if requested; return success
    bool delete_edge(const ScafVertex& u, const ScafVertex& v, const bool register_in_solution = false)
    {
      if(register_in_solution) solution.add_edge_global(operator[](u).name, operator[](v).name, operator[](ScafVertexPair(u, v)).weight);
      return ScaffoldGraph::delete_edge(u, v);
    }

    //! remove all non-matching edges incident with u, except for e 
    void clear_vertex_except(const ScafVertex& u, const ScafEdge* const e, const bool register_in_solution = false)
    {
      DEBUG4(std::cout << "clearing "<<operator[](u).name << " avoiding "<< (e ? get_edge_name(*e) : EdgeName("",""))<<std::endl);
      for(auto r = get_incident_non_matching(u); r;)
        if(e && (*r == *e)) ++r; else delete_edge(*(r++), register_in_solution);
    }
    //! remove all non-matching edges incident with u, except for the one that is also incident with v 
    void clear_vertex_except(const ScafVertex& u, const ScafVertex* const v, const bool register_in_solution = false)
    {
      DEBUG4(std::cout << "clearing "<<operator[](u).name << " avoiding "<< (v ? operator[](*v).name : VertexName(""))<<std::endl);
      for(auto r = get_incident_non_matching(u); r;)
        if(v && (target(*r) == *v)) ++r; else delete_edge(*(r++), register_in_solution);
    }

  }; // Instance



} // namespace

#endif
