

/** \file predicates.hpp
 * collection of predicates allowing filtering of iterators or scoring
 */

#ifndef PREDICATES_HPP
#define PREDICATES_HPP

namespace scaffold{ namespace predicates {

  //! prototype predicate
  template<typename Element>
  struct Predicate
  {
    const bool invert;         //!< indicate whether the predicate should be inverted

    //! constructor
    Predicate(const bool _invert = false):
      invert(_invert) {}
    
    //! all predicates must implement operator()
    virtual bool operator()(const Element& e) const = 0;
  };


  //! a predicate that always evaluates to true
  template<typename Element>
  struct TruePredicate: public Predicate<Element>
  {
    using Predicate<Element>::Predicate;
    using Predicate<Element>::invert;

    bool operator()(const Element& e) const
    {
      return !invert;
    }
  };

  //! a predicate that is true iff the given element is (not) the given element
  /** changing the container will change the result of predicate checks */
  template<typename Element>
  struct EqualPredicate: public Predicate<Element>
  {
    using Predicate<Element>::Predicate;
    using Predicate<Element>::invert;
    
    const Element& e; //!< container reference

    //! constructor
    /** If _invert is set, then the predicate is FALSE iff the given element is in the container.
     * No copy of the container is performed. Instead we save a reference to it.
     */
    EqualPredicate(const Element& _e, const bool _invert = false):
      Predicate<Element>(_invert), e(_e) {}

    //! application of the predicate
    bool operator()(const Element& _e) const
    {
      return (invert != (e == _e));
    }
  };


  //! a predicate that is true iff the given element is (not) contained in a given container
  /** changing the container will change the result of predicate checks */
  template<typename Element, typename Container = unordered_set<Element> >
  struct ContainedPredicate: public Predicate<Element>
  {
    using Predicate<Element>::Predicate;
    using Predicate<Element>::invert;
    
    const Container& container; //!< container reference

    //! constructor
    /** If _invert is set, then the predicate is FALSE iff the given element is in the container.
     * No copy of the container is performed. Instead we save a reference to it.
     */
    ContainedPredicate(const Container& _container, const bool _invert = false):
      Predicate<Element>(_invert), container(_container) {}

    bool operator()(const Element& e) const
    {
      return (invert != contains(container, e));
    }
  };

  //! an edge predicate that is true iff the given edge is (not) a matching edge
  /** changing the graph will change the result of predicate checks */
  template<typename Graph>
  struct MatchingPredicate: public Predicate<Edge<Graph> >
  {
    using Element = Edge<Graph>;
    using Predicate<Element>::Predicate;
    using Predicate<Element>::invert;

    const Graph& g;     //!< reference to the graph for which containment should be checked

    //! constructer
    /** if _invert is set, then the predicate is FALSE iff the given edge is in the graph g */
    MatchingPredicate(const Graph& _g, const bool _invert = false):
      Predicate<Element>(_invert), g(_g) {}
    
    bool operator()(const Element& e) const
    {
      return (invert != g[e].is_matching_edge());
    }

  };


  //! an edge predicate that is true iff the given edge has its target (not) in a given set
  /** Changing the container or the graph will change the result of predicate checks.
   * Matching edges can be disallowed, by constructing with matching_OK = false.
   */
  template<class Graph, class Container = VertexSet<Graph> >
  struct TargetContainedPredicate: public Predicate<Edge<Graph> >
  {
    using Element = Edge<Graph>;
    using Predicate<Element>::Predicate;
    using Predicate<Element>::invert;

    const Container container; //!< container to store good targets
    const Graph& g;             //!< reference to the graph
    const bool matching_OK;     //!< indicate whether matching edges should be treated like any other edge (true) or always evaluate to false (false)

    //! constructor
    /** no copies are made, we only save references */
    TargetContainedPredicate(const Container& _container, const Graph& _g, const bool _invert = false, const bool _matching_OK = true):
      Predicate<Element>(_invert), container(_container), g(_g), matching_OK(_matching_OK) 
    {
    }

    bool operator()(const Element& e) const
    {
      if(!matching_OK && g[e].is_matching_edge()) return false;
      return (invert != contains(container, boost::target(e, g)));
    }
  };


}}// namespace

#endif
