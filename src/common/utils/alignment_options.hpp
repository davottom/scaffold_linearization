
#pragma once

#include "utils/command_line.hpp"
#include "utils/oriented_sequences.hpp"


//! alignment suboptions
struct AlignmentOptions{
  //! number of 'N' to insert into the solution sequence when an edge with NO_LENGTH is discovered between 2 contigs
  unsigned NO_LENGTH_num_N;

  //! maximum overlap radius when aligning two sequences around an expected overlap
  /** that is, how many bp to try to shift the second sequence to the left and right to try to find a better overlap **/
  unsigned max_overlap_radius;

  // how many base pairs an overlap has to have to be considered
  unsigned min_overlap;

  // percentage of the theoretical max. probability over which an alignment is considered "good", that is:
  // with
  // P1 = probability of finding an unequal base under uniform selection of indices of the overlap (= avg Hamming dist)
  // P2 = probability of the illumina producing a read with the implied insert-size
  // the probability of an alignment is (1 - P1) * P2
  // the theoretical max probability is the probability of the illumina choosing the mean insert size (avg Hamming dist 0)
  float threshold_probability_percentage;

  // if picking a read length that is at least as far from the mean insert size as x is at most this likely, then x is considered implausible
  float min_length_probability;

  // use Hamming distance instead of Levenstein distance
  bool hamming_distance;

  // maximum mismatches within 100bp to still be considered an acceptable alignment
  float max_mismatch_percentage;

  // allow trying to place contigs inside other contigs (requires quite some distrust in the assembler...)
  bool contig_in_contig;

  //! complement of A is U
  bool uracil;

  //! return true iff the arguments make sense
  void sanity_check() const
  {
    if(min_length_probability > 50) throw except::invalid_options("length probability should not be chosen > 50% as, otherwise, the probability of a positive length actually being positive might be smaller than this");
    if(max_mismatch_percentage > 100) throw except::invalid_options("mismatch percentage cannot exceed 100");
    // throw except::invalid_options("bla"); // will need utils/exceptions.hpp
  }

};

//! add alignment specific options to the global options list
void add_alignment_options(po::options_description& desc)
{
  desc.add_options()
      ("nN", po::value<unsigned>(), "number of 'N' to insert when an edge of unknown length is found in the solution (default: 100)")
      ("hd", "use Hamming distance instead of Levenstein distance when computing overlap score") 
      ("cc", "allow placing contigs inside other contigs (allow inter-contig edges with length -x where x > the length of one of the incident contigs) -- this requires quite some distrust in the assembler") 
      ("mr", po::value<unsigned>(), "maximum overlap radius to consider when aligning contigs (number of bp to try to the left and right of an expected overlap) (default: 150)")
      ("mm", po::value<unsigned>(), "maximum mismatches among 100bp to still be considered a good alignment (default: 20)")
      ("mo", po::value<unsigned>(), "minimum overlap required to consider when aligning contigs (default: 8)")
      ("tp", po::value<float>(), "alignment threshold probability in % (alignments scoring higher than this percent of the maximum probability are considered 'good') (default: 85)") 
      ("mp", po::value<float>(), "minimum cumulative probability (in %) for an insert size to be consitered plausible (a read length x is considered implausible iff it is at most this likely to pick a read-length that is at least as far from the mean insert size as x) (default: 30)") 
      ("u", "complement of A is U (default: auto-detect, with fallback T)")
      ;
}

//! detect if the given sequencemap uses U or T
bool detect_uracil(const scaffold::SequenceMap& sequences)
{
  for(const auto& named_seq: sequences){
    const std::string& seq = named_seq.second.sequence;
    const size_t index = seq.find_first_of("UTut");
    if(index != std::string::npos)
      return (seq[index] == 'U') || (seq[index] == 'u');
  }
  // if no sequence contains U or T, then just use T
  return false;
}

//! handle alignment-specific options passed by the command-line
AlignmentOptions handle_alignment_options(const po::variables_map& vm, const scaffold::SequenceMap * const sequences = NULL)
{
  AlignmentOptions align_opts;
  align_opts.NO_LENGTH_num_N    = get_option<unsigned>(vm, "nN", 100);
  align_opts.max_overlap_radius = get_option<unsigned>(vm, "mr", 150);
  align_opts.max_mismatch_percentage = get_option<unsigned>(vm, "mm", 20) / 100.0;
  align_opts.min_overlap        = get_option<unsigned>(vm, "mo", 8);
  align_opts.threshold_probability_percentage = get_option<float>(vm, "tp", 85.0);
  align_opts.min_length_probability = get_option<float>(vm, "mp", 30) / 100.0;
  align_opts.hamming_distance   = vm.count("hd");
  align_opts.contig_in_contig   = vm.count("cc");
  align_opts.uracil             = vm.count("u");

  if(!align_opts.uracil && (sequences != NULL))
    align_opts.uracil = detect_uracil(*sequences);

  return align_opts;
}


