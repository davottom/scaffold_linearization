
#pragma once
#include "utils/command_line.hpp"

struct ILPOptions {
  //! bool incidating whether multiplicities should be taken into account
  //bool ignore_multiplicities;

  //! bool indicating whether taking a contig-connection with multiplicity will gain all its weight
  /** if true, its weight will be gained if it is being used, no matter how often
   * if false, each time it is being taken, we gain a portion of its weight up to its full weight at its maximum multiplicity
   */
  bool multi_take_all;

  //! bool incidating whether contig jumps should be considered
  /** A contig jump is a read-pair whose ends are in contigs A & C, but in the genome there is a contig B between them */
  bool ignore_contig_jumps;

  // if the read-lengths required to jump a contig are at least this, then support jumping the contig
  float jump_threshold_probability;

};

//! add ILP specific options to the global options list
void add_ilp_options(po::options_description& desc)
{
  desc.add_options()
      ("jp", po::value<float>(), "jump threshold probability in % (if the read-pair jumping the contig is at least this likely to be long enough to fit the contig, then support using the jump) (default: 70)")
      ("ta", "let the solution take all weight of an edge when using it at least once, instead of gaining linearly (& scaling the weight)")
      ("nj", "ignore contig jumps")
      ;

}

//! handle ILP specific options passed by the command-line
ILPOptions handle_ilp_options(const po::variables_map& vm)
{
  ILPOptions ilp_opts;
  
  ilp_opts.multi_take_all      = vm.count("ta");
  ilp_opts.ignore_contig_jumps = vm.count("nj");
  //ilp_opts.ignore_multiplicities = vm.count("nm");

  ilp_opts.jump_threshold_probability = get_option<float>(vm, "jp", 70) / 100.0f;

  return ilp_opts;
}

