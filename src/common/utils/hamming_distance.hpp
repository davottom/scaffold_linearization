
#pragma once

#include "utils/utils.hpp"
#include "utils/string_distances.hpp"
#include "utils/string_utils.hpp"

class Hamming_Distance_Calculator : String_Distance_Calculator
{
  using String_Distance_Calculator::char_consensus;
public:
  using String_Distance_Calculator::String_Distance_Calculator;
  //! get the distance between s1[i:] and s2
  uint32_t get_distance(const uint32_t i) 
  {
    uint32_t result = 0;
    const uint32_t max_len = std::min(s1.length() - i, s2.length());
    for(uint32_t index = 0; index < max_len; ++index){
      result += (s1[i + index] != s2[index]);
      if(result > max_dist) return UINT_MAX;
    }
    return result;
  }

  //! get the consenus string between s1 and s2, that is, merge s1 and s2 (starting at index i of s1) together
  std::string get_consensus(const uint32_t i) 
  {
    std::string result;
    const uint32_t max_len = std::min(s1.length() - i, s2.length());
    for(uint32_t index = 0; index < max_len; ++index)
      result += char_consensus(s1[i + index], s2[index]);
    return result + s2.substr(max_len);
  }

};

