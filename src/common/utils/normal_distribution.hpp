

/** \file NormalDistribution.hpp
 * tools to compute the probability of an event under a given normal distribution
 */

#pragma once

#include <vector>
#include <limits> // for NaN

namespace scaffold{

  typedef enum{T_VARIANCE, T_STD_DEVIATION} VarianceType;
  //! normal distribution centered with given mean and standard deviation (makes heavy use of std::erf())
  template<typename MeanType = int>
  struct NormalDistribution{
    MeanType mean;
//    const float std_deviation;
    float variance;

    //! constructors
    NormalDistribution(const float _var, const MeanType& _mean = 0, const VarianceType vt = T_STD_DEVIATION):
      mean(_mean), variance(vt == T_VARIANCE ? _var : _var * _var)
    {}
    NormalDistribution(const std::pair<MeanType, float>& mean_and_stddev, const VarianceType vt = T_STD_DEVIATION):
      NormalDistribution(mean_and_stddev.second, mean_and_stddev.first, vt) {}
    NormalDistribution(): NormalDistribution(0) {}

    //! a normal distribution is valid iff its standard distribution is non-0
    bool is_valid() const
    {
      return variance != 0.0f;
    }

    //! compute the probability to pick between x and y under the normal distribution centered at mean with standard deviation
    float prob_of_picking_between(const float& x, const float& y) const
    {
      assert(y >= x);
      return prob_of_picking_at_least(x) - prob_of_picking_at_least(y);
    }
 
    //! compute the probability to pick between x - radius and x + radius
    float prob_of_picking_around(const float& x, const float& radius = 0.5f) const
    {
      return prob_of_picking_between(x - radius, x + radius);
    }
 
    //! compute the probability to pick something whose distance to the mean is at most x
    float prob_distance_to_mean_at_most(const float& x) const
    {
      const float Sqrt2variance = std::sqrt(2 * variance);
      return std::erf(((float)x) / Sqrt2variance);
    }
    //! compute the probability to pick something whose distance to the mean is at least x
    float prob_distance_to_mean_at_least(const float& x) const
    {
      return 1.0f - prob_distance_to_mean_at_most(x);
    }

    //! compute the probability to pick a value of at least x
    float prob_of_picking_at_least(const float& x) const
    {
      if(x >= mean)
        return prob_distance_to_mean_at_least(x - mean) / 2.0f;
      else
        return 1.0f - prob_of_picking_at_least(mean + mean - x);
    }
    //! compute the probability to pick a value of at least x
    float prob_of_picking_at_most(const float& x) const
    {
      return 1.0 - prob_of_picking_at_least(x);
    }

    //! given p, return the value x such that the probability of picking at most x is p
    // NOTE: 0 < p < 1
    MeanType quantile(const float& p) const
    {
      assert((0 <= p) && (p <= 1));

      const float Sqrt2variance = std::sqrt(2 * variance);
      return mean + Sqrt2variance * erf_inv_apx(2*p - 1);
    }
    
    //! define the sum of two normally distributed random variables
    NormalDistribution& operator+=(const NormalDistribution& n)
    {
      // we use the fact that the sum of two independent normally distributed random variables X & Y
      // is a normally distributed random variable Z
      mean += n.mean;
      variance += n.variance;
      return *this;
    }
    //! define the sum of two normally distributed random variables
    NormalDistribution& operator-=(const NormalDistribution& n)
    {
      // we use the fact that the sum of two independent normally distributed random variables X & Y
      // is a normally distributed random variable Z
      mean -= n.mean;
      variance += n.variance;
      return *this;
    }

    //! add something to the mean
    NormalDistribution& operator+=(const MeanType& shift)
    {
      mean += shift;
      return *this;
    }
    template<typename T>
    NormalDistribution operator+(const T& n) const
    {
      NormalDistribution result(*this);
      result += n;
      return result;
    }
    NormalDistribution operator-(const NormalDistribution<MeanType>& n) const
    {
      NormalDistribution result(*this);
      result -= n;
      return result;
    }

    //! support negation (and, thus, substraction)
    NormalDistribution operator-() const
    {
      return NormalDistribution(variance, -mean, T_VARIANCE);
    }

    //! comparator in order to put into maps and priority_queues
    bool operator<(const NormalDistribution& n) const
    {
      return mean < n.mean;
    }
 
    //! compute mean and variance from a sample, that is a container of MeanTypes
    template<class Container = std::vector<MeanType> >
    void from_sample(const Container& sample)
    {
      if(sample.size()){
        auto sum = 0;
        auto sum_of_squares = 0;
        for(const auto& i: sample){
          sum += i;
          sum_of_squares += i * i;
        }
        mean = ((float)sum) / sample.size();
        variance = ((float)sum_of_squares - (float)sum * sum / sample.size()) / sample.size();
      } else mean = variance = std::numeric_limits<float>::quiet_NaN();
    }

    //! compute mean and variance from a sample of pairs, that is a container of pairs, where pair.second is MeanType
    template<class Container = std::vector<MeanType> >
    void from_pairs(const Container& sample)
    {
      std::cout << "computing distribution from "<<sample.size()<<" samples"<<std::endl;
      if(sample.size()){
        auto sum = 0;
        auto sum_of_squares = 0;
        for(const auto& i: sample){
          sum += i.second;
          sum_of_squares += i.second * i.second;
        }
        mean = ((float)sum) / sample.size();
        variance = ((float)sum_of_squares - (float)sum * sum / sample.size()) / sample.size();
      } else mean = variance = std::numeric_limits<float>::quiet_NaN();
    }

  };

  template<typename T>
  std::ostream& operator<<(std::ostream& os, const NormalDistribution<T>& n)
  {
    return os << '~'<<n.mean;
  }
  

}

