
/** \file graph_parameters.hpp
 * compute simple graph parameters such as max degree, feedback edge set number, degeneracy, ...
 *
 * many functions in this class take a pointer to a graph_parameters struct; 
 * if this is not NULL, they will store the respective value in the struct as well as returning it
 */

#pragma once

#include <vector>
#include <queue>
#include <iostream>
#include <iomanip>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/connected_components.hpp>

#include "graph_typedefs.hpp"
#include "graph_utils.hpp"
#include "low_priority_queue.hpp"

using namespace boost;

namespace scaffold { namespace graph_parameters {

  //! comparator class, comparing two vertices u,v such that comp(u,v) = true <=> deg(u) < deg(v)
  template<class Graph>
  struct degree_less{
    const Graph& g;
    // constructor, needs to save a reference to the graph in which to compare degrees
    degree_less(const Graph& _g):g(_g) {}
    // actual compare operator
    bool operator()(const Vertex<Graph>& u, const Vertex<Graph>& v) const{
      return boost::degree(u, g) < boost::degree(v, g);
    }
  };

  //! comparator class, comparing two vertices u,v such that comp(u,v) = true <=> deg(u) > deg(v)
  template<class Graph>
  struct degree_greater{
    const Graph& g;
    // constructor, needs to save a reference to the graph in which to compare degrees
    degree_greater(const Graph& _g):g(_g) {}
    // actual compare operator
    bool operator()(const Vertex<Graph>& u, const Vertex<Graph>& v) const{
      return boost::degree(u, g) > boost::degree(v, g);
    }
  };
// can be used in priority queues like this:
//    typedef typename std::priority_queue<Vertex<Graph>, std::vector<Vertex<Graph> >, degree_greater<Graph> > degree_comparing_queue;
//    degree_comparing_queue degree_queue( (degree_greater<Graph>(g)) );

  //! an interval consists of a lower & an upper bound
  struct interval{
    unsigned lower_bound, upper_bound;
    interval(unsigned lb = 0, unsigned ub = UINT_MAX): lower_bound(lb), upper_bound(ub) {}
  };

  //! collection of graph parameters that can be computed
  /** if their exact computation is NPo-hard, an interval should be enough */
  struct graph_parameters {
    unsigned vertices;
    unsigned edges;
    unsigned components;
    unsigned max_degree;
    unsigned min_degree;
    unsigned h_index;
    unsigned degeneracy;
    interval treewidth;
    interval pathwidth;
    interval cliquewidth;
    interval vertex_cover;
    interval max_clique;
    interval independent_set;
    interval chromatic_number;
    interval FVS;
  };
  //! print intervals
  std::ostream& operator<<(std::ostream& os, const interval& i){
    return os << "["<<i.lower_bound<<","<<i.upper_bound<<"]";
  }
  //! print all graph parameters
  std::ostream& operator<<(std::ostream& os, const graph_parameters& params){
    os << "vertices:\t"<<   params.vertices   <<std::endl;
    os << "edges:\t\t"<<      params.edges      <<std::endl;
    os << "min degree:\t"<< params.min_degree <<std::endl;
    os << "max degree:\t"<< params.max_degree <<std::endl;
    os << "avg degree:\t"<< std::fixed << std::setprecision(1) << (2.0f*params.edges)/params.vertices <<std::endl;

    os << "components:\t"<< params.components <<std::endl;
    os << "FES:\t\t"<<      compute_FES(params.vertices, params.edges, params.components) <<std::endl;
    os << "h-index:\t"<<    params.h_index    <<std::endl;
    os << "degeneracy:\t"<< params.degeneracy <<std::endl;
    os << "FVS:\t\t"<<      params.FVS        <<std::endl;
    os << "treewidth:\t"<<  params.treewidth  <<std::endl;
    os << "pathwidth:\t"<<  params.pathwidth  <<std::endl;
    os << "vertex cover:\t"<<params.vertex_cover<<std::endl;
    os << "cliquewidth:\t"<<params.cliquewidth<<std::endl;
    os << "maxclique:\t"<<  params.max_clique <<std::endl;
    os << "ind.-set:\t"<<   params.independent_set <<std::endl;
    os << "chrom.-num:\t"<< params.chromatic_number<<std::endl;
    return os;
  }

  template<class Graph>
  using vertex_ordering = std::vector<Vertex<Graph> >;
  
  //! compute a component map and return the number of connected components
  /** this assumes that the vertex properties have a member called "index" representing the vertex index */
  template <class Graph>
  unsigned num_connected_components(const Graph& g, ComponentMap<Graph>& compMap, graph_parameters* const params = NULL)
  {
    typedef typename Graph::vertex_bundled VertexInfos;
    // trust that the VertexInfos have an ::index 
    unsigned num_components = boost::connected_components(g, boost::associative_property_map<ComponentMap<Graph> >(compMap), vertex_index_map(boost::get(&VertexInfos::index, g)));
    if(params) params->components = num_components;
    return num_components;
  }

  //! return the number of connected components (see num_connected_components() above)
  template <class Graph>
  unsigned num_connected_components(const Graph& g, graph_parameters* const params = NULL)
  {
    ComponentMap<Graph> compMap;
    return num_connected_components<Graph>(g, compMap, params);
  }

  //! compute the maximum degree of g and return a vertex that has this degree
  /** assumes that g is not the empty graph */
  template <class Graph>
  VertexAndDegree<Graph> get_max_degree(const Graph& g, graph_parameters* const params = NULL)
  {
    assert(num_vertices(g));
  
    VertexIterRange<Graph> r = vertices(g);
    VertexIter<Graph> max_v = std::max_element(r.first, r.second, degree_cmp<Graph>(g));
    const unsigned v_deg = boost::degree(*max_v, g);

    // finally, update parameters & return the result
    if(params)
      params->max_degree = v_deg;

    return {*max_v, v_deg};
  }

  //! compute the h-index of the graph g
  /** the h-index is the smallest number x such that there are at most x vertices of degree at least x */
  template <class Graph>
  unsigned h_index(const Graph& g, graph_parameters* const params = NULL)
  {
    // step 1: sort vertices descendingly by degree, using a priority_queue
    // step 1a: construct a priority queue that sorts incoming vertices by degree
    std::priority_queue<unsigned, std::vector<unsigned>, std::less<unsigned> > degree_queue;
    // step 1b: insert all vertices into the queue
    unsigned min_deg = UINT_MAX;
    for(VertexIterRange<Graph> r = vertices(g); r.first != r.second; ++r.first){
      const unsigned deg = degree(*r.first, g);
      // update minimum degree
      if(deg < min_deg) min_deg = deg;
      degree_queue.push(deg);
    }
    // extract the max degree
    const unsigned max_deg = degree_queue.top();
    // step 2: walk the degree order until #steps = degree
    unsigned x = 0;
    while(degree_queue.empty() ? false : (degree_queue.top() >= x)){
      ++x;
      degree_queue.pop(); 
    }
    // finally, update parameters & return the result
    if(params){
      params->h_index = x;
      params->min_degree = min_deg;
      params->max_degree = max_deg;
    }
    return x;
  }

  // compute a degeneracy and its ordering (that is, an ordering of the vertices minimizing the "right-degree")
  template <class Graph>
  unsigned degeneracy(const Graph& g, vertex_ordering<Graph>* order = NULL, graph_parameters* const params = NULL)
  {
    // use a priority queue to order vertices based on their degrees
    typedef typename std::low_priority_queue<Vertex<Graph> > DegreeQueue;
    typedef typename DegreeQueue::value_type VertexAndDegree;
    DegreeQueue degree_queue;
    // keep a mapping of vertices to iterators within the DegreeQueue having num_vertices(g) buckets to keep the load factor low
    typedef typename boost::unordered_map<Vertex<Graph>, typename DegreeQueue::const_iterator> VertexToIter;
    typedef typename VertexToIter::value_type VertexAndIter;
    VertexToIter vertex_to_iter(boost::num_vertices(g));
    // make a copy of g
    Graph h;
    typedef unordered_map<Vertex<Graph>, Vertex<Graph> >    VtoVMap;
    VtoVMap translate_map, inv;
    
    boost::copy_graph(g, h, orig_to_copy(associative_property_map<VtoVMap>(inv)).vertex_index_map(boost::get(&Graph::vertex_bundled::index, g)));
    // invert to get a translate map from copy to orig (if the FVS is requested)
    if(order) for(auto& i : inv) translate_map.emplace(i.second, i.first);
    
    // step 2: set up the degree queue
    unsigned min_deg = UINT_MAX;
    unsigned max_deg = 0;
    for(VertexIterRange<Graph> r = boost::vertices(h); r.first != r.second; ++r.first){
      const Vertex<Graph>& u = *r.first;
      const unsigned deg = boost::degree(u, h);
      // update minimum degree
      if(deg < min_deg) min_deg = deg;
      if(deg > max_deg) max_deg = deg;
      // set up the queue
      vertex_to_iter.emplace(VertexAndIter(u, degree_queue.insert(VertexAndDegree(u, deg))));
    }
    if(params) {
      params->min_degree = min_deg;
      params->max_degree = max_deg;
    }
    DEBUG5(std::cout << "found min degree "<<min_deg<<" & max_deg "<<max_deg<<std::endl);
    
    // step 3: repeatedly delete the smallest-degree vertex of h to find the degeneracy
    unsigned degeneracy = min_deg;
    while(boost::num_vertices(h)){
      // step 3a: pop a vertex of minimum degree...
      const typename DegreeQueue::value_type& next_pair = degree_queue.get_min();
      const Vertex<Graph> u = next_pair.first;
      const unsigned min_deg = next_pair.second;
      DEBUG5(std::cout << "found vertex of degree "<<boost::degree(u, h)<<" ("<<min_deg<<"), "<<boost::num_vertices(h)<<" vertices left"<<std::endl);
      degree_queue.pop_min();
      vertex_to_iter.erase(u);
      // update degeneracy if necessary
      if(min_deg > degeneracy) degeneracy = min_deg;
      DEBUG5(std::cout << "degeneracy set to "<<degeneracy<<std::endl);
      // step 3b: add it at the end of the ordering
      if(order) order->push_back(translate_map[u]); 
      // step 3c: decrease the degree of each its non-visited neighbors
      for(AdjIterRange<Graph> r = boost::adjacent_vertices(u, h); r.first != r.second; ++r.first){
        const Vertex<Graph>& v = *r.first;
        const typename DegreeQueue::const_iterator di = vertex_to_iter.at(v);
        DEBUG5(std::cout << "seeing a neighbor of degree "<<boost::degree(v, h)<<" (stored as "<<di->second<<")"<<std::endl);
        degree_queue.decrement_priority(di);
        DEBUG5(std::cout<<"decremented priority of someone to "<<di->second<<std::endl);
      }
      boost::clear_vertex(u, h);
      boost::remove_vertex(u, h);
    } // while
    if(params) params->degeneracy = degeneracy;
    return degeneracy;
  } // function


  //! compute a greedy feedback vertex set and return the number of vertices therein
  /** this works by getting the vertex of highest degree in the 2-core of the graph, putting it into the FVS and removing it */
  template <class Graph>
  unsigned feedback_vertex_set_greedy(const Graph& g, VertexSet<Graph>* FVS = NULL, graph_parameters *params = NULL)
  {
    unsigned result_fvs = 0;
    unsigned result_tw = 0;
    unsigned tw_ub = 0;

    // Step 1: make a copy of g
    Graph h;
    typedef unordered_map<Vertex<Graph>, Vertex<Graph> >    VtoVMap;
    VtoVMap translate_map, inv;
    boost::copy_graph(g, h, orig_to_copy(associative_property_map<VtoVMap>(inv)).vertex_index_map(boost::get(&Graph::vertex_bundled::index, g)));
    // invert inv to get a translate map from copy to orig (if the FVS is requested)
    if(FVS) for(auto& i : inv) translate_map.emplace(i.second, i.first);

    // Step 2: split off connected components
    while(true){
      VTranslateMap<Graph,Graph> tr_back;
      graph_parameters sub_params;

      Graph* comp = split_off_component(h, (VTranslateMap<Graph,Graph>*)NULL, &tr_back);
      if(comp) {
        result_fvs += feedback_vertex_set_greedy(*comp, FVS, &sub_params);
        delete comp;
        // update the global upper bound on the treewidth
        tw_ub = std::max(tw_ub, result_tw + sub_params.treewidth.upper_bound);
      } else break;
    };

    // use a priority queue to order vertices based on their degrees
    typedef typename std::low_priority_queue<Vertex<Graph>, unsigned> DegreeQueue;
    typedef typename DegreeQueue::value_type VertexAndDegree;
    DegreeQueue degree_queue;
    // keep a mapping of vertices to iterators within the DegreeQueue having num_vertices(g) buckets to keep the load factor low
    typedef typename boost::unordered_map<Vertex<Graph>, typename DegreeQueue::const_iterator> VertexToIter;
    typedef typename VertexToIter::value_type VertexAndIter;
    VertexToIter vertex_to_iter(boost::num_vertices(h));

    // step 3: setup data structures
    for(VertexIterRange<Graph> r = boost::vertices(h); r.first != r.second; ++r.first){
      const Vertex<Graph>& u = *r.first;
      const unsigned deg = boost::degree(u, h);
      vertex_to_iter.emplace(VertexAndIter(u, degree_queue.insert(VertexAndDegree(u, deg))));
    }

    while(true){
      // Step 4: delete all bridges from h
      BridgeMap<Graph> bridges;
      mark_bridges(h, bridges);
      for(const auto& i : bridges) {
        const UnorderedVertexPair<Graph>& b = i.first;
        boost::remove_edge(boost::edge(b.first, b.second, h).first, h);
        for(const Vertex<Graph>& u : {b.first, b.second})
          if(boost::degree(u, h) > 0)
            degree_queue.decrement_priority(vertex_to_iter.at(u));
          else{
            typename VertexToIter::const_iterator i = vertex_to_iter.find(u);
            degree_queue.erase(i->second);
            vertex_to_iter.erase(i);
            boost::remove_vertex(u, h);
          }
      }

      // if h still has edges left
      if(boost::num_edges(h)){
        // Step 5: get max degree vertex in the rest of the graph
        const VertexAndDegree& max_pair = degree_queue.get_max();
        const Vertex<Graph> max_deg_vertex = max_pair.first;
        DEBUG3(std::cout << "treated graph ("<<num_vertices(h)<<" vertices), found FVS ("<<FVS<<") candidate "<< h[max_deg_vertex].name << " with degree "<<max_pair.second<<std::endl);
        
        // Step 6: take the max degree vertex in g into the FVS and delete it in h
        if(FVS) FVS->insert(translate_map.at(max_deg_vertex));       
        for(AdjIterRange<Graph> r = boost::adjacent_vertices(max_deg_vertex, h); r.first != r.second; ++r.first){
          const Vertex<Graph>& v = *r.first;
          const typename VertexToIter::const_iterator v_iter = vertex_to_iter.find(v);
          const typename DegreeQueue::const_iterator dq_iter = v_iter->second;

          if(boost::degree(v, h) > 1){
            degree_queue.decrement_priority(dq_iter);
            DEBUG5(std::cout<<"decremented priority of someone to "<<dq_iter->second<<std::endl);
          } else{
            degree_queue.erase(dq_iter);
            vertex_to_iter.erase(v_iter);
            boost::remove_vertex(v, h);
          }
        }
        // clean up data structures
        boost::clear_vertex(max_deg_vertex, h);
        typename VertexToIter::const_iterator max_ref = vertex_to_iter.find(max_deg_vertex);
        degree_queue.erase(max_ref->second);
        vertex_to_iter.erase(max_ref);
        boost::remove_vertex(max_deg_vertex, h);

        ++result_fvs;
        ++result_tw;
      } else break;
    } // while
    if(params) {
      tw_ub = std::max(tw_ub, result_tw);
      // update the upper bound on the treewidth
      if(params) params->treewidth.upper_bound = std::min(params->treewidth.upper_bound, tw_ub);
      // update the FVS upper bound
      params->FVS.upper_bound = std::min(params->FVS.upper_bound, result_fvs);
    }
    return result_fvs;
  }


  //! compute the easy graph parameters
  /** considered easy:
   * number of vertices
   * number of edges
   * number of connected components
   * h-index
   * degeneracy
   */
  template <class Graph>
  void compute_easy_parameters(const Graph& g, graph_parameters& params)
  {
    params.vertices = boost::num_vertices(g);
    params.edges = boost::num_edges(g);
    DEBUG5(std::cout << "computing connected components..."<<std::endl);
    num_connected_components(g, &params);
    DEBUG5(std::cout << "computing h-index..."<<std::endl);
    h_index(g, &params);
    DEBUG5(std::cout << "computing degeneracy..."<<std::endl);
    degeneracy(g, (vertex_ordering<Graph>*)NULL, &params);
    // simple upper bounds:
    // 1. even if all edges take part in the max clique
    params.treewidth.upper_bound =
      params.pathwidth.upper_bound =
      params.cliquewidth.upper_bound =
      params.vertex_cover.upper_bound =
      params.max_clique.upper_bound =
      params.independent_set.upper_bound =
      params.chromatic_number.upper_bound =
      params.FVS.upper_bound = params.vertices;
  }

}} // namespace

