

/** \file low_priority_queue.hpp
 * implementation of a priority queue taking O(n+k) space
 * where k is the maximum priority & n is the number of elements in the queue
 * and the following times:
 * construct: O(1)
 * insert: O(log k)  (amortized)
 * erase (by iterator): O(log k)
 * change_priority (by iterator): O(log k)
 * get_min, get_max: O(1)
 * pop_min, pop_max: O(1)
 */

#pragma once

#include <list>
#include <map>

namespace std{

  template<typename InterListIt, typename IntraListIt>
  class LpqIteratorRaw{
  protected:
    // the iterator needs an iterator to the priority list and another iterator inside that list
    InterListIt to_list;
    InterListIt to_end;
    IntraListIt to_element;
  public:
    // construct
    LpqIteratorRaw(const InterListIt& _to_list, const InterListIt& _to_end, const IntraListIt& _to_element):
      to_list(_to_list), to_end(_to_end), to_element(_to_element) {}
    LpqIteratorRaw(const InterListIt& _to_list, const InterListIt& _to_end):
      to_list(_to_list), to_end(_to_end), to_element() 
    {
      if(to_list != to_end) to_element = to_list->second.begin();
    }
    LpqIteratorRaw(const LpqIteratorRaw<InterListIt, IntraListIt>& it):
      to_list(it.to_list), to_end(it.to_end), to_element(it.to_element) {}

    LpqIteratorRaw& operator++()
    {
      if(to_list != to_end){
        ++to_element;
        if(to_element == to_list->second.end()){
          ++to_list;
          if(to_list != to_end) 
            to_element = to_list->second.begin();
        }
      }
    }

    LpqIteratorRaw operator++(int)
    {
      LpqIteratorRaw _i(*this);
      ++(*this);
      return _i;
    }

    bool operator==(const LpqIteratorRaw& it) const
    {
      return (to_list == it.to_list) && ((to_list == to_end) || (to_element == it.to_element));
    }

    bool operator!=(const LpqIteratorRaw& it) const
    {
      return !operator==(it);
    }

    bool is_valid() const
    {
      return to_list != to_end;
    }

  };


  //! a low-priority queue of Elements with Priorities
  /** Priority should be orderable by its operator<() (see std::map) */
  template<class Element, class Priority = size_t>
  class low_priority_queue{

  private:
    // private types
    typedef typename list<Element>::iterator ListIter;
    typedef typename std::map<Priority, list<Element> > PrioToList;
    typedef typename PrioToList::iterator PrioToListIter;

#if GCC_VERSION >= 40900
    typedef typename list<Element>::const_iterator ListConstIter;
    typedef typename PrioToList::const_iterator PrioToListConstIter;
#else
    typedef typename list<Element>::iterator ListConstIter;
    typedef typename PrioToList::iterator PrioToListConstIter;
#endif

    // actual data container
    PrioToList prio_to_list;

  public:
    typedef pair<Element, Priority> value_type;

    template<typename ElementT, typename PriorityT>
    class IteratorRaw: public LpqIteratorRaw<PrioToListIter, ListIter>{
      using LpqIteratorRaw<PrioToListIter, ListIter>::to_list;
      using LpqIteratorRaw<PrioToListIter, ListIter>::to_end;
      using LpqIteratorRaw<PrioToListIter, ListIter>::to_element;
    public:
      using LpqIteratorRaw<PrioToListIter, ListIter>::LpqIteratorRaw;

      pair<ElementT&, PriorityT> operator*() const
      {
        return {*to_element, to_list->first};
      }
      template<typename OtherElementT, typename OtherPriorityT>
      IteratorRaw(const IteratorRaw<OtherElementT, OtherPriorityT>& it):
        LpqIteratorRaw<PrioToListIter, ListIter>(it) {}
    };
    typedef IteratorRaw<Element, Priority> iterator;
    typedef IteratorRaw<const Element, const Priority> const_iterator;

    //! construct an empty low-priority queue
    low_priority_queue() {}

    //! copy construct a low-priority queue
    low_priority_queue(const low_priority_queue<Element, Priority>& lpq):
      prio_to_list(lpq.prio_to_list)
    {}

    iterator begin()
    {
      return iterator(prio_to_list.begin(), prio_to_list.end());
    }

    const_iterator begin() const
    {
      return const_iterator(prio_to_list.begin(), prio_to_list.end());
    }

    iterator end()
    {
      return iterator(prio_to_list.end(), prio_to_list.end());
    }

    const_iterator end() const
    {
      return const_iterator(prio_to_list.end(), prio_to_list.end());
    }

    //! return whether the low-priority queue is empty
    const bool empty() const
    {
      return prio_to_list.empty();
    }

    //! insert an element into the low-priority queue, returning a const_iterator to the newly inserted item
    const_iterator insert(const Element& e, const Priority& prio = Priority())
    {
      const PrioToListIter prio_it = prio_to_list.DEFAULT_EMPLACE(prio).first;
      list<Element>& vlist = prio_it->second;
      vlist.push_back(e);
      DEBUG5(cout << "inserted item "<< vlist.back()<<" of priority "<<prio_it->first<<endl);
      return const_iterator(prio_it, prio_to_list.end(), std::prev(vlist.end()));
    }


    //! erase an element (given as iterator) from the low-priority queue
    void erase(const const_iterator& it)
    {
      const Priority& prio = it->second;

      // find the priority list that it is going to be deleted from
      const typename PrioToList::iterator p_iter = prio_to_list.find(prio);
      assert(p_iter != prio_to_list.end());
      list<Element>& vlist = p_iter->second;
      vlist.erase(it);
      // if we removed the last item of a priority, then remove its priority class
      if(vlist.empty()) prio_to_list.erase(p_iter);
    }

    //! change the priority of an item in the low-priority queue
    void change_priority(const const_iterator& it, const Priority& new_prio)
    {
      const Priority& old_prio = it->second;

      if(old_prio != new_prio){
        const typename PrioToList::iterator old_list = prio_to_list.find(old_prio);
        assert(old_list != prio_to_list.end());

        // if the priority didn't exist yet, insert an empty list to hold the value
        const typename PrioToList::iterator new_list = prio_to_list.DEFAULT_EMPLACE(new_prio).first;
        list<Element>& vlist = new_list->second;

        vlist.splice(vlist.end(), old_list->second, it);
        vlist.back().second = new_prio;

        if(old_list->second.empty()) prio_to_list.erase(old_list);
      }// if
    }

    //! decrement the priority of an item, given as iterator (see change_priority())
    void decrement_priority(const const_iterator& it)
    {
      change_priority(it, it->second - 1);
    }
    //! increment the priority of an item, given as iterator (see change_priority())
    void increment_priority(const const_iterator& it)
    {
      change_priority(it, it->second + 1);
    }

    //! return the min of the low-priority queue - this is any item of minimum priority
    const value_type& get_min() const
    {
      assert(!prio_to_list.empty());
      assert(!prio_to_list.begin()->second.empty());
      return value_type(prio_to_list.begin()->second.front(), prio_to_list.begin()->first);
    }
    //! remove the min of the low-priority queue
    void pop_min()
    {
      assert(!empty());
      
      list<Element>& vlist = prio_to_list.begin()->second;
      vlist.pop_front();
      if(vlist.empty()) prio_to_list.erase(prio_to_list.begin());
    }

    //! return the max of the low-priority queue - this is an item of maximum priority
    const value_type& get_max() const
    {
      assert(!prio_to_list.empty());
      assert(!prio_to_list.rbegin()->second.empty());
      return value_type(prio_to_list.rbegin()->second.back(), prio_to_list.rbegin()->first);
    }
    //! remove the max of the low-priority queue 
    void pop_max()
    {
      assert(!empty());

      list<Element>& vlist = prio_to_list.rbegin()->second;
      vlist.pop_front();
      if(vlist.empty()) prio_to_list.erase(prio_to_list.rbegin());
    }

  };



};


