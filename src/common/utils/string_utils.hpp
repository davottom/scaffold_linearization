

/** \file string_utils.hpp
 * collection of utilities working with strings
 */

#ifndef STRING_UTILS_HPP
#define STRING_UTILS_HPP

#include <string>
#include <boost/unordered_map.hpp>

#include "utils/exceptions.hpp"
#include "utils/vector2d.hpp"

#define WHITESPACES " \t"

typedef boost::unordered_map<std::string, unsigned> StringCount;

// longest prefix between 2 c-strings, see https://codereview.stackexchange.com/questions/104477/fastest-c-code-to-get-common-prefix-length
std::ptrdiff_t len_common_prefix(const char* a, const char* b)
{
    const unsigned min_len = std::min(std::strlen(b), std::strlen(a));
    return std::distance(b, std::mismatch(b, b + min_len, a).first);
}
//! get longest common prefix of two strings
std::ptrdiff_t len_common_prefix(const std::string& a, const std::string& b)
{
    return std::distance(b.begin(), std::mismatch(b.begin(), b.end(), a.begin()).first);
}

//! remove all characters not in 'skip_to' from the beginning of s
inline void skip_to(std::string& s, const std::string& skip_to)
{
  const size_t pos = s.find_first_of(skip_to);
  if(pos == std::string::npos) s.clear(); else s.erase(0,pos);
}

//! remove all characters in 'to_skip' from the beginning of s
inline void skip_all(std::string& s, const std::string& to_skip)
{
  const size_t pos = s.find_first_not_of(to_skip);
  if(pos == std::string::npos) s.clear(); else s.erase(0,pos);
}

//! consume an integer from the beginning of s and return it
long read_single_number(std::string& s)
{
  long result = std::stoi(s.c_str());
  // remove result from the number
  skip_all(s, "+-");
  skip_all(s, "0123456789");
  return result;
}

//! remove leading & trailing chars (whitespaces by default) from str
std::string trim(const std::string& str, const std::string& to_remove = WHITESPACES)
{
    const size_t first = str.find_first_not_of(to_remove);
    if(first == std::string::npos) return "";
    const size_t last = str.find_last_not_of(to_remove);
    return str.substr(first, last - first + 1);
}





//! merge two sequences seq1 and seq2 with overlap "overlap" according to their order
/** This functions segfaults for reasons that are beyond me...
 * For now, use the non-segfaulting "merge_strings()" below
 */
std::string merge_strings_segfault(const std::string& seq1,
                            const std::string& seq2,
                            const unsigned overlap)
{
  assert(overlap <= seq1.length());
  const unsigned seq1_len = seq1.length() - overlap;
  const unsigned seq2_len = seq2.length();
  const unsigned new_len = seq1_len + seq2_len;
  char* const new_seq = (char*)malloc(new_len + 1);
  char* const seq1_start = new_seq;
  char* const seq2_start = new_seq + seq1_len;
  std::cout << "got "<<new_len+1<<" bytes @"<<(long)new_seq<<std::endl;

  std::cout << "writing "<<(long)(seq2_start - seq1_start)<<" chars of "<<seq1<<" to "<<(long)seq1_start<<std::endl;
  memcpy(seq1_start, seq1.c_str(), seq1_len);
  
  std::cout << "writing "<<seq2_len<<" chars of "<<seq2<<" to "<<(long)seq2_start<<std::endl;
  memcpy(seq2_start, seq2.c_str(), seq2_len);
  new_seq[new_len+1] = 0;

  std::string result(new_seq);
  free(new_seq);
  return result;
}

//! merge two sequences seq1 and seq2 with overlap "overlap" according to their order
/** For example: merge_strings("abcde", "1234", 3) = "ab1234"
 */
std::string merge_strings(const std::string& seq1,
                            const std::string& seq2,
                            const unsigned overlap)
{

  std::string result = seq1.substr(0, seq1.length() - overlap) + seq2;
  return result;
}



#endif
