
/** \file alternating_path.hpp
 * tool to find alternating paths in a graph containing a perfect matching
 */

#ifndef ALTERNATING_PATH_HPP
#define ALTERNATING_PATH_HPP


#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/max_cardinality_matching.hpp>

#include "utils/utils.hpp"
#include "utils/graph_typedefs.hpp"
#include "utils/graph_utils.hpp"

namespace scaffold{

  //! return whether there is an alternating cycle in g containing the edge uv
  /** This works as follows:
   * If uv is a matching edge, then we remove it and ask for an augmenting path in g.
   * As match is spanning the vertices of g, such an augmenting path must be a u-v-path
   * and it must be alternating.
   * If uv is not a matching edge, then we delete the vertices u & v and its incident
   * edges and then ask for an augmenting path. Again, since match is spanning the
   * vertices of g, any augmenting path is a u'-v'-path where u' and v' are the vertices
   * matched with u and v in g, respectively. Such an augmenting path exists if and
   * only if uv is on an alternating cycle
   */
  template<class Graph>
  bool edge_on_alternating_cycle(const Vertex<Graph>& u,
                            const Vertex<Graph>& v,
                            const Graph& g,
                            const Matching<Graph>& match)
  {
    Graph h;
    // assert that g contains the edge uv
    assert(boost::edge(u, v, g).second);
    // assert that the matching is spanning g's vertices
    assert(boost::num_vertices(g) == 2 * match.size());

    // step 1: copy g
    Matching<Graph> translate;
    boost::copy_graph(g, h, orig_to_copy(associative_property_map<Matching<Graph> >(translate)).vertex_index_map(boost::get(&Graph::vertex_bundled::index, g)));
    
    Vertex<Graph>& u_in_h = translate.at(u); 
    Vertex<Graph>& v_in_h = translate.at(v);
    const Edge<Graph> uv_in_h = boost::edge(u_in_h, v_in_h, h).first;
    
    // translate the matching of g into a matching in h
    Matching<Graph> match_in_h;
    for(const VertexPair<Graph>& uv: match)
      match_in_h.emplace(VertexPair<Graph>(translate.at(uv.first), translate.at(uv.second)));
    
    // step 2: delete the matching edge(s) incident with u & v in h
    if(match_in_h.at(u_in_h) != v_in_h) {
      // if uv is not a matching edge itself, delete the vertices u & v and search for an augmenting path in the rest
      for(const Vertex<Graph>& x: {u_in_h, v_in_h}){
        remove_matching_pair<Graph>(x, match_in_h);
        boost::clear_vertex(x, h);
      }
    } else boost::remove_edge(uv_in_h, h);
    // step 3: try to augment the matching of J
    typedef typename boost::associative_property_map<Matching<Graph> > MateMap;
    MateMap matching(match_in_h);
    boost::edmonds_augmenting_path_finder<Graph, MateMap, VertexIndexMap<Graph> >
      augmentor(h, matching, boost::get(&Graph::vertex_bundled::index, h));

    // step 4: return whether the matching is augmentable
    return augmentor.augment_matching();
  }



}// namespace


#endif

