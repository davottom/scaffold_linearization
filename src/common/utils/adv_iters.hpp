

/** \file adv_edge_iters.hpp
 * Edge iterators that allow iterating over edges without access to the graph object.
 * The idea is to pass a predicate to the iterator and skip over edges for which the predicate evaluates to false
 */

#pragma once

#include "utils/graph_typedefs.hpp"
#include "utils/predicates.hpp"

namespace scaffold {
  using namespace predicates;

  //! an edge iterator that skips over all edges for which the predicate evaluates to false
  template<class Graph, class Iterable, class IterClass, class Predicate = TruePredicate<Iterable> >
  class PredicatedIter
  {
  public:
    const Graph& g;       //!< reference to the graph
    const Predicate pred; //!< a predicate
  protected:
    IterClass payload;       //!< internal boost iterator
    IterClass end;           //!< internal boost iterator to "past-the-end"

    //! skip over invalid edges after assignment or increment operations
    void fix_payload()
    {
      while(is_valid() && !pred(*payload)) ++payload;
    }

  public:

    //! constructor initializing all data members
    PredicatedIter(const Graph& _g,
                         const IterClass& _payload,
                         const IterClass& _end,
                         const Predicate& _pred = Predicate()):
      g(_g), pred(_pred), payload(_payload), end(_end)
    {
      fix_payload();
    }
    //! constructor using an iterator range as given by boost::edges() for example
    PredicatedIter(const Graph& _g,
                         const std::pair<IterClass, IterClass>& range,
                         const Predicate& _pred = Predicate()):
      g(_g), pred(_pred), payload(range.first), end(range.second)
    {
      fix_payload();
    }
    //! copy constructor, possibly different predicate
    template<class OtherPredicate = Predicate>
    PredicatedIter(const PredicatedIter<Graph, Iterable, IterClass, OtherPredicate>& _orig):
      PredicatedIter(_orig.g, _orig.get_internal_iterators(), pred(_orig.pred))
    {}
    //! copy constructor, possibly different predicate
    template<class OtherPredicate = Predicate>
    PredicatedIter(const PredicatedIter<Graph, Iterable, IterClass, OtherPredicate>& _orig, const Predicate& _pred):
      PredicatedIter(_orig.g, _orig.get_internal_iterators(), _pred)
    {}


    //! return true iff the iterator is not "past-the-end"
    bool is_valid() const
    {
      return payload != end;
    }

    //! = is_valid()
    operator bool() const
    {
      return is_valid();
    }

    //! dereference operator, simply dereferences the internal boost iterator
    const Iterable operator*() const
    {
      return *payload;
    }

    //! increment operator
    PredicatedIter& operator++()
    {
      if(is_valid()){
        ++payload;
        fix_payload();
      }
      return *this;
    }
    //! post-increment
    PredicatedIter operator++(int)
    {
      IterClass i = payload;
      ++(*this);
      return PredicatedIter<Graph, Iterable, IterClass, Predicate>(g, i, end, pred);
    }

    //! check against another IterClasser
    template<class OtherPred>
    bool operator==(const PredicatedIter<Graph, Iterable, IterClass, OtherPred>& it) const
    {
      return (payload == it.payload);
    }

    //! return the internal iterators
    std::pair<IterClass, IterClass> get_internal_iterators() const
    {
      return {payload, end};
    }

    //! assign from a pair of iterators
    PredicatedIter<Graph, Iterable, IterClass, Predicate>& operator=(const std::pair<IterClass, IterClass>& range)
    {
      payload = range.first;
      end = range.second;
      fix_payload();
      return *this;
    }
  };

  template<class Graph, class Predicate = TruePredicate<Vertex<Graph> >, class IterClass = VertexIter<Graph> >
  using PredicatedVertexIter = PredicatedIter<Graph, Vertex<Graph>, IterClass, Predicate>;

  template<class Graph, class Predicate = TruePredicate<Edge<Graph> >, class IterClass = OEdgeIter<Graph> >
  using PredicatedEdgeIter = PredicatedIter<Graph, Edge<Graph>, IterClass, Predicate>;

}// namespace



