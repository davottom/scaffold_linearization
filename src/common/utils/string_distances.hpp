

#pragma once

#include <string>
#include "utils/utils.hpp"

class String_Distance_Calculator
{
protected:
  const std::string& s1;
  const std::string& s2;
  // a good upper bound on the distances that we are interested in can help keep computation time low
  const uint32_t max_dist;

  //! a simple char consensus; This can be used if some transitions are more likely than others
  char char_consensus(const char& x, const char& y) const
  {
    return x;
  }

public:

  String_Distance_Calculator(const std::string& _s1,
                             const std::string& _s2,
                             const uint32_t _max_dist = UINT_MAX):
    s1(_s1), s2(_s2), max_dist(_max_dist)
  {
  }

  // dummy constructor for uniform interface
  String_Distance_Calculator(const std::string& _s1,
                             const std::string& _s2,
                             const uint32_t lower_index,
                             const uint32_t upper_index,
                             const uint32_t _max_dist = UINT_MAX):
    String_Distance_Calculator(_s1, _s2, _max_dist)
  {}


  //! get the distance between s1[i:] and s2
  virtual uint32_t get_distance(const uint32_t i) = 0;

  //! get the consenus string between s1 and s2, that is, merge s1 and s2 (starting at index i of s1) together
  virtual std::string get_consensus(const uint32_t i) = 0;

};


