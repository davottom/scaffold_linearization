#pragma once
#include <chrono>
#include <climits>

#include "utils/distributions.hpp"


namespace scaffold {

  struct JumpStatistics{
    NormalDistribution<> jump_network_sizes;
    NormalDistribution<> num_reasons_for_jump;
  };

  struct LinearizationStatistics {
    unsigned nb_cuts;
    unsigned weight_cut;
    unsigned tw;
    std::chrono::microseconds duration_tw_cut[25], duration_tw_weight[25],
      duration_ilp_cut[25], duration_ilp_weight[25];
  };

  struct SolutionGraphStats {
    unsigned nb_ambiguous_paths=0,
      nb_non_ambiguous_paths=0,
      min_degree=UINT_MAX,
      max_degree=0, sum_weight=0, nb_non_matching_edges=0;

    float average_degree=0;
    
  };


  struct CallbackStatistics {
    unsigned cuts_added;
    unsigned times_called;
    double root_relax;
    timer time_spent;

    CallbackStatistics(): cuts_added(0), times_called(0) {};
  };

}

