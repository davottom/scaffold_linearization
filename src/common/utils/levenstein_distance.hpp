
#pragma once

#include <queue>
#include "utils/vector2d.hpp"
#include "utils/utils.hpp"
#include "utils/string_utils.hpp"
#include "utils/string_distances.hpp"

enum Op : byte {OP_UNINITIALIZED, OP_DEL_LEFT, OP_DEL_RIGHT, OP_CHANGE, OP_PREFIX, OP_FAIL, OP_SUCCESS};
typedef std::pair<uint32_t, uint32_t> Coords;
typedef std::pair<Coords, uint32_t> CoordsAndBound;


// a DP cell comparer, that splits along the diagonal in the table
struct CoordsCmp
{
  bool operator()(const Coords& c1, const Coords& c2) const
  {
    const uint32_t sum1 = c1.first + c1.second;
    const uint32_t sum2 = c2.first + c2.second;
    return (sum1 == sum2) ? (c1.first < c2.first) : (sum1 < sum2);
  }
};


//typedef boost::unordered_map<Coords, Entry> Levenstein_Op_Table;
class Levenstein_Op_Table
{
public:
  // an op_table entry contains an op, a cost, and an upper bound
  struct Entry
  {
    Op op;
    uint32_t cost;
    uint32_t upper_bound;

    Entry(const uint32_t _ub = 0):
      op(OP_UNINITIALIZED), cost(UINT_MAX), upper_bound(_ub) {}

    void lower_cost(const Op& _op, const uint32_t _cost, const byte increment = 0)
    {
      if(_cost < cost - increment){
        op = _op;
        cost = _cost + increment;
      } 
    }
    void raise_ub(const uint32_t _ub)
    {
      if(_ub != UINT_MAX)
        upper_bound = std::max(upper_bound, _ub);
    }
    // mark the entry failed
    void mark_fail()
    {
      op = OP_FAIL;
      cost = UINT_MAX;
      upper_bound = -1;
    }
  };

private:
  const uint32_t lower_index;
  const uint32_t max_lower_i;
  const uint32_t max_lower_j;
  // the lower part is [0-upper_index,0-max_dist] and it's represented in full
  std::vector2d<Entry> lower_part;
  // everything outside the lower part is represented as map of coords to entries
  boost::unordered_map<Coords,Entry> upper_part;

  typedef std::set<Coords, CoordsCmp> CellQueue;

  void init_upper_part_cell(const std::string& s1,
                            const std::string& s2,
                            const uint32_t i,
                            const uint32_t j,
                            Entry& entry,
                            CellQueue& cell_queue)
  {
    const uint32_t prefix_len = len_common_prefix(s1.c_str() + i, s2.c_str() + j);
    if((i + prefix_len < s1.length()) && (j + prefix_len < s2.length())){
      if(entry.upper_bound + 1 > 1){
        if(prefix_len == 0){
          const bool i_good = (i + 1 < s1.length());
          const bool j_good = (j + 1 < s2.length());

          if(i_good){
            cell_queue.emplace(i + 1, j);
            operator[]({i + 1, j}).raise_ub(entry.upper_bound - 1);
            DEBUG6(std::cout << "del_left raised upper bound of ["<<i + 1<<", "<<j<<"] to "<<entry.upper_bound - 1<<std::endl);
          }
          if(j_good){
            cell_queue.emplace(i, j + 1);
            operator[]({i, j + 1}).raise_ub(entry.upper_bound - 1);
            DEBUG6(std::cout << "del_right raised upper bound of ["<<i<<", "<<j + 1<<"] to "<<entry.upper_bound - 1<<std::endl);
          }
          if(i_good && j_good){
            cell_queue.emplace(i + 1, j + 1);
            operator[]({i + 1, j + 1}).raise_ub(entry.upper_bound - 1);
            DEBUG6(std::cout << "change raised upper bound of ["<<i + 1<<", "<<j + 1<<"] to "<<entry.upper_bound - 1<<std::endl);
          }
        } else {
          // there is a non-empty common prefix, so jump there
          cell_queue.emplace(i + prefix_len, j + prefix_len);
          operator[]({i + prefix_len, j + prefix_len}).raise_ub(entry.upper_bound);
          DEBUG6(std::cout << "prefix raised upper bound of ["<<i + prefix_len<<", "<<j + prefix_len<<"] to "<<entry.upper_bound<<std::endl);
        }// if prefix
      } else {
        entry.op = OP_FAIL;
        entry.cost = UINT_MAX;
        entry.upper_bound = UINT_MAX;
      }
    }// if we're not at the end of s1 or s2
  }

  // initialize the table with lower_index and upper_index
  void initialize_bounds(const std::string& s1,
                         const std::string& s2,
                         const uint32_t lower_index,
                         const uint32_t upper_index,
                         const uint32_t max_dist)
  {
    DEBUG6(
    std::cout << "initializing "<<max_lower_i<<" x "<<max_lower_j<<" grid and upper_map from:"<<std::endl;
    std::cout << "["<<lower_index<<":"<<lower_index+max_lower_i<<"] of "<<s1<<" (len "<<s1.length()<<")"<<std::endl;
    std::cout << "[0:"<<max_lower_j<<"] of "<<s2<<" (len "<<s2.length()<<")"<<std::endl);
    // Step1: init lower part
    for(uint32_t i = lower_index; i < upper_index; ++i) lower_part[{i - lower_index,0}].upper_bound = max_dist;
    for(uint32_t j = 0; j < max_lower_j; ++j)
      for(uint32_t i = 0; i < max_lower_i; ++i){
        const bool chars_equal = (s1[lower_index + i] == s2[j]);
        Entry& entry = lower_part[{i,j}];
        const uint32_t entry_ub = entry.upper_bound;
        if(entry_ub > 0){
          lower_part[{i + 1, j}].raise_ub(entry_ub - 1);
          lower_part[{i, j + 1}].raise_ub(entry_ub - 1);
          lower_part[{i + 1,j + 1}].raise_ub(entry_ub - (chars_equal ? 0 : 1));
        } else{
          if(!chars_equal)
            entry.mark_fail();
          else
            lower_part[{i + 1, j + 1}].raise_ub(entry_ub);
        }
      }
    
    // Step2: prepare the cell_queue for the upper part of the table
    CellQueue cell_queue;
    for(uint32_t i = 0; i <= max_lower_i; ++i) cell_queue.emplace(lower_index + i, max_lower_j);
    for(uint32_t j = 0; j <= max_lower_j; ++j) cell_queue.emplace(lower_index + max_lower_i, j);
    
    DEBUG6(std::cout << "upper bounds in the cell_queue:"<<std::endl;
    for(const auto& c: cell_queue) std::cout << c<< ": "<<operator[](c).upper_bound << std::endl);

    // Step3: emplace cells with bounds in the upper part of the table
    while(!cell_queue.empty()){
      const Coords& c = *cell_queue.begin();
      init_upper_part_cell(s1, s2, c.first, c.second, operator[](c), cell_queue);
      cell_queue.erase(cell_queue.begin());
    }// while there are coordinates to check
    DEBUG4(std::cout << "initialized "<<(max_lower_i+1)*(max_lower_j+1)<<" + "<<upper_part.size()<<" cells"<<std::endl);
  }

// make the lower_part bigger than (upper_index - lower_index) x max_dist by this factor
#define SCALE_LOWER_PART 1.66f
public:
  Levenstein_Op_Table(const std::string& s1,
                      const std::string& s2,
                      const uint32_t _lower_index,
                      const uint32_t _upper_index,
                      const uint32_t _max_dist,
                      const float scale_lower = SCALE_LOWER_PART):
    lower_index(_lower_index),
    max_lower_i(std::min(((uint32_t)s1.length()) - 1, (uint32_t)(_upper_index + scale_lower * _max_dist)) - _lower_index),
    max_lower_j(std::min(((uint32_t)s2.length()) - 1, (uint32_t)((scale_lower + 1.0f) * _max_dist))),
    lower_part(max_lower_i + 1, max_lower_j + 1)
  {
    initialize_bounds(s1, s2, _lower_index, _upper_index, _max_dist);
  }

#define is_in_lower_part(x) (((x).first <= lower_index + max_lower_i) && ((x).second <= max_lower_j))
  Entry& operator[](const Coords& ij)
  {
    assert(ij.first >= lower_index);
    if(is_in_lower_part(ij))
      return lower_part[{ij.first - lower_index, ij.second}];
    else{
      DEBUG6(if(upper_part.find(ij) == upper_part.end()) std::cout << "creating new cell: "<<ij<<std::endl);
      return upper_part[ij];
    }
  }
  const Entry& at(const Coords& ij) const
  {
    assert(ij.first >= lower_index);
    if(is_in_lower_part(ij))
      return lower_part[{ij.first - lower_index, ij.second}];
    else{
      return upper_part.at(ij);
    }
  }
};

//! modified Levenstein distance of an alignment of two sequences
/** this Levenstein distance is modified to:
 * 1. absorb any suffix of s1 or s2 at no cost, as it will be used to find alignments of sub-sequences and
 *    we don't want to punish the fact that one of the sequences may be shorter.
 * 2. ignore transformations that cost at least "max_cost"
 * Note that the seeming advantage of short sequences is countered by using the AVERAGE distance for scoring.
 * NOTE: this is a modification of https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#C.2B.2B
 */
class Levenstein_Distance_Calculator : public String_Distance_Calculator
{
  using String_Distance_Calculator::s1;
  using String_Distance_Calculator::s2;
  using String_Distance_Calculator::max_dist;
  using String_Distance_Calculator::char_consensus;
  using String_Distance_Calculator::String_Distance_Calculator;
  // the actual table used to compute the distance and get the consensus
  // op_table[i,j].second is the levenstein distance between s1[i:] and s2[j:]
  Levenstein_Op_Table op_table;

// set a table entry with Op = x, cost = y, and increment = z
#define make_table_entry(x,y,z) ((y) < (max_dist) - (z)) ? Entry((x), (y) + (z)) : Entry(OP_FAIL, UINT_MAX, UINT_MAX)

  // compute the table entry at (i,j) recursively
  const uint32_t get_table_cost(const uint32_t i, const uint32_t j)
  {
    //DEBUG5(std::cout << "getting ["<<i<<", "<<j<<"]"<<std::endl);
    if((i == s1.length()) || (j == s2.length())) return 0;
    assert((i < s1.length()) && (j < s2.length()));

    // note: after initialize_table(), all needed talbe entries exist!
    Levenstein_Op_Table::Entry& entry = op_table[{i,j}];
      
    if(entry.op == OP_UNINITIALIZED){
      //DEBUG6(std::cout << "computing distance between "<<s1.substr(i) <<" & "<<s2.substr(j)<<std::endl);
      // initialize as failed
      entry.op = OP_FAIL;
      entry.cost = UINT_MAX;

      // if the entry is uninitialized, then compute it recursively; this way, each table entry is computed at most once
      // first, read as much of both strings as possible
      const uint32_t prefix_len = len_common_prefix(s1.c_str() + i, s2.c_str() + j);
      if(prefix_len == 0){
        if(entry.upper_bound != 0){
          //DEBUG5(std::cout << " == 1st branch of ["<<i<<", "<<j<<"]"<<std::endl);
          entry.lower_cost(OP_CHANGE, get_table_cost(i + 1, j + 1), 1);
          if(entry.cost > 1){
            //DEBUG5(std::cout << " == 2nd branch of ["<<i<<", "<<j<<"]"<<std::endl);
            entry.lower_cost(OP_DEL_LEFT, get_table_cost(i + 1, j), 1);
          }
          if(entry.cost > 1){
            //DEBUG5(std::cout << " == 3rd branch of ["<<i<<", "<<j<<"]"<<std::endl);
            entry.lower_cost(OP_DEL_RIGHT, get_table_cost(i, j + 1), 1);
          }
        }
      } else {
        // there is a non-empty common prefix, so jump there
        entry.lower_cost(OP_PREFIX, get_table_cost(i + prefix_len, j + prefix_len));
      }// if prefix
    }// if not OP_UNINITIALIZED
    //DEBUG5(std::cout << "returning ["<<i<<", "<<j<<"] = "<<entry.cost<<std::endl);
    return entry.cost;
  }


  //! compute the levenstein consensus
  void get_consensus(const uint32_t i, const uint32_t j, std::string& consensus) const
  {
    DEBUG5(std::cout << "initial consensus at ["<<i<<", "<<j<<"] (vs lengths of "<<s1.length()<<" & "<<s2.length()<<"): "<<consensus<<std::endl);
    if((i < s1.length()) && (j < s2.length())){
      const Op& op = op_table.at({i,j}).op;
      switch(op){
        case OP_FAIL: // the levenstein distance exceeds max_dist
          consensus = "";
          break;
        case OP_PREFIX:
          {
            const uint32_t prefix_len = len_common_prefix(s1.c_str() + i, s2.c_str() + j);
            assert(prefix_len != 0);
            consensus += s1.substr(i, prefix_len);
            get_consensus(i + prefix_len, j + prefix_len, consensus);
            break;
          }
        case OP_CHANGE:
          consensus += char_consensus(s1[i], s2[j]);
          get_consensus(i + 1, j + 1, consensus);
          break;
        case OP_DEL_RIGHT:
          get_consensus(i, j + 1, consensus);
          break;
        case OP_DEL_LEFT:
          get_consensus(i + 1, j, consensus);
          break;
        default:
          // we've reached an initialized cell that has OP_UNINITIALIZED... this cannot happen!
          std::cerr << "reached unreachable code block in Levenstein distance computation" << std::endl;
          exit(EXIT_FAILURE);
      }// swtich
    } else {
      // if we are at the border of s1 or s2, add what's left of s2
      consensus += s2.substr(j);
    }
    DEBUG5(std::cout << "final consensus at ["<<i<<", "<<j<<"]: "<<consensus<<std::endl);
  }// function


public:

  Levenstein_Distance_Calculator(const std::string& _s1,
                                 const std::string& _s2,
                                 const uint32_t lower_index,
                                 const uint32_t upper_index, // note that upper_index is exclusive!
                                 const uint32_t _max_dist = UINT_MAX):
    String_Distance_Calculator(_s1, _s2, _max_dist), op_table(s1, s2, lower_index, upper_index, _max_dist)
  {
    assert(lower_index <= upper_index);
  }

  //! get the Levenstein distance between s1[i:] and s2
  uint32_t get_distance(const uint32_t i)
  {
    assert(i < s1.length());
    return get_table_cost(i, 0);
  }

  //! get the Levenstein consenus string between s1 and s2, that is, merge s1 and s2 (starting at index i of s1) together
  std::string get_consensus(const uint32_t i)
  {
    assert(i < s1.length());
    std::string consensus = s1.substr(0, i);
    // first make sure the table for [i,0] is computed
    get_table_cost(i, 0);
    get_consensus(i, 0, consensus);
    return consensus;
  }

};

