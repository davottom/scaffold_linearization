

/** \file scoring.hpp
 * collection of scoring functions
 */

#pragma once

#include "utils/predicates.hpp"

namespace scaffold{ namespace scoring {

  //! a scoring function for a pair of something and unsigned that just returns the unsigned
  template<typename Element>
  struct IdentityScore{
    unsigned operator()(const std::pair<Element, unsigned>& e) const
    {
      return e.second;
    }
  };

  //! a scoring function returning the distance to a reference score given at construction
  template<typename Element>
  struct DistanceScore{
    const unsigned reference_point;
    
    DistanceScore(const unsigned _reference_point): reference_point(_reference_point) {}
    unsigned operator()(const std::pair<Element, unsigned>& e) const
    {
      return (unsigned)abs((long)e.second - (long)reference_point);
    }
  };

  //! a scoring returning the distance to a reference score if a predicate evaluates to true and UINT_MAX otherwise
  template<typename Element, class Predicate>
  struct PredicatedDistanceScore: public DistanceScore<Element>{
    using DistanceScore<Element>::reference_point;
    const Predicate predicate;

    PredicatedDistanceScore(const Predicate& _predicate, const unsigned _reference_point):
      DistanceScore<Element>::DistanceScore(_reference_point), predicate(_predicate) {}
    
    unsigned operator()(const std::pair<Element, unsigned>& e) const
    {
      if(predicate(e.first))
        return UINT_MAX;
      else
        return DistanceScore<Element>::operator()(e);
      //(unsigned)abs((long)e.second - (long)reference_point);
    }
  };

}}//namespace

