

/** \file solution.hpp
 * defines a solution class, storing edge names and their weight
 */

#ifndef SOLUTION_HPP
#define SOLUTION_HPP

#include <list>
#include "utils/utils.hpp"
#include "utils/graph_typedefs.hpp"

namespace scaffold{

  //! a solution is a list of edge names with weights; it keeps track of the overall weight for O(1) access
  template<class Compare = std::less<size_t> >
  class Solution {
  public:
    typedef std::pair<EdgeName, unsigned> value_type;
  protected:
    std::list<value_type> entries; //!< edges of the solution paired with their weights at the time of addition
    unsigned global_weight = 0; //!< keeps track of the global weight for O(1) checks
  public:

    //! return whether there are no solutions stored in the object
    bool no_solutions() const
    {
      return entries.empty();
    }

    //! return the total weight of this solution
    unsigned total_weight() const
    {
      return global_weight;
    }

    //! invalidate this solution by removing all edges
    void invalidate()
    {
      entries.clear();
      global_weight = 0;
    }

    //! combine two solutions for the graphs g & h to form a solution for the disjoint union g + h
    void combine_disjoint_union(const Solution<Compare>& S)
    {
      entries.insert(entries.end(), S.entries.begin(), S.entries.end());
      global_weight += S.global_weight;
    }

    //! add an edge to the list
    void add_edge_global(const EdgeName& e, const unsigned weight)
    {
      DEBUG4(std::cout << "registering edge ("<<e.first<<","<<e.second<<") globally"<<std::endl);
      entries.emplace_back(e, weight);
      global_weight += weight;
    }
    void add_edge_global(const VertexName& u, const VertexName& v, const unsigned weight)
    {
      add_edge_global(EdgeName(u,v), weight);
    }

    friend std::ostream& operator<<(std::ostream& os, const Solution<Compare>& S)
    {
      return os << S.entries;
    } // function
  };


} // namespace


#endif
