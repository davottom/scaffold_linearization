Graph-based scaffolder
======
`linscaf` is a tool aiming to linearize scaffolds containing some ambiguous paths (see [our article](https://hal-lirmm.ccsd.cnrs.fr/lirmm-03218029v2) for more information). `linscaf` is a subpart of a bigger project called `scaftool` (coming soon) aiming to solve the scaffolding problem. 

`linscaf` implements the two exacts methods described in **Linearizing Genomes: Kernalization, Exact Methods and Local Search**. If you to replicate the tests, the instances are available in `data/instances`. You can use the script in `scripts/linearization.sh`.

Pre-requisites
------
+ [Boost Graph Library (BGL)](https://www.boost.org/doc/libs/1_71_0/more/getting_started/unix-variants.html) 1.59 or above
+ CPLEX: if you want to solve the SBC with an ILP (optional).
+ HTD (available [here](https://github.com/mabseher/htd)): if you want to solve SBC with a tree decomposition (optional).

Compilation
------
Compile a **Makefile** using **cmake**. In the *source* directory:

```console
foo@bar:<path/to/scaftools/source>$ cmake .
```

If you want to use CPLEX, use the option -DCPLEX=ON.
If you do not have set up CPLEX environment variables use option `-DCPLEX_ROOT_DIR` to specify the directory CPLEX is installed in (usually `/opt/ibm/ILOG/CPLEX_STUDIOXXX...` where XXX is the CPLEX version):

```console
foo@bar:<path/to/scaftools/source>$ cmake -DCPLEX=ON -DCPLEX_ROOT_DIR="path/to/CPLEX/ .
```

If you want to use HTD, use the option -DHTD=ON. You need to indicate where the library is installed with the option `-DHTD_DIR`

```console
foo@bar:<path/to/scaftools/source>$ cmake -DHTD=ON -DHTD_DIR=/path/to/htd .
```


If everything went smoothly with **cmake** compile using **make**:
```console
foo@bar:<path/to/scaftools/source>$ make
```

Usage
------
Should the compilation process go unhindered, an executable file will be produced in the **bin** directory: `linearization`

The usage syntax:
```console
foo@bar:<path/to/scaftools/source>$ ./bin/<exe> [options] --fn <scaffold graph dot format file>
```

The executable will accept options from the following **io** options:
+ **--help**                produce help message
+ **--fn** *\<arg\>*              file to read solution graph from
+ **--is** *\<arg\>*              file to read contig sequences from (fasta format) (required for **--os**)
+  **--os** *\<arg\>*              file to output solution sequences to (fasta format) (requires **--is**)
+  **--og** *\<arg\>*              file to output resulting graph to (dot format)

In addition, alignment options are available when pre/post-processing solutions:
+  **--nN** *\<arg\>*              number of 'N' to insert when an edge of unknown length is found in the solution (default: 100)
+  **--hd**                  use Hamming distance instead of Levenstein distance when computing overlap score
+  **--cc**                  allow placing contigs inside other contigs (allow inter-contig edges with length -x where x > the length of one of the incident contigs) -- this requires quite some distrust in the assembler
+  **--mr** *\<arg\>*              maximum overlap radius to consider when aligning contigs (number of bp to try to the left and right of an expected overlap) (default: 150)
+  **--mm** *\<arg\>*              maximum mismatches among 100bp to still be considered a good alignment (default: 20)
+  **--mo** *\<arg\>*              minimum overlap required to consider when aligning contigs (default: 8)
+  **--tp** *\<arg\>*              alignment threshold probability in % (alignments scoring higher than this percent of the maximum probability are considered 'good') (default: 85)
+ **--mp** *\<arg\>*              minimum cumulative probability (in %) for an insert
                      size to be consitered plausible (a read length x is
                      considered implausible iff it is at most this likely to
                      pick a read-length that is at least as far from the
                      mean insert size as x) (default: 30)

+ **--u**                   complement of A is U (default: auto-detect, with
                       fallback T)
+ **--cv**                  when linearizing, minimize cleaned vertices instead of
                       deleted edges
+ **--app**                 when linearizing, use approximation algorithm (default:
                       ILP)
+ **--brut**                when linearizing, use the brutal algorithm (default:
                       ILP)
+ **--dec**                 deconstruct the graph into sequences after the
                       linearization process
+ **--rrules**              apply the linearization reduction rules on the graph
                       (for readability purposes)
+ **--lstats** *\<arg\>*          file to output linearization stats
+ **--sgstats** *\<arg\>*          file to output solution graph stats (skip linearization
                       process)
+ **--ltw**             when linearizing, use the tree decomposition method
+ **--lcompare**        when linearizing, compare computation time of tw and 
                        ilp methods; the computation is made 25 times for each 
                        method and each score and the average time is written 
                        in the stats file

Known Issues
------
There is a bug in BGL up to version 1.58.0 ([bug #10895](https://svn.boost.org/trac/boost/ticket/10895)) that requires applying the bundled patch *boost_copy_component.patch*. This can be done by entering the boost install dir (usually /usr/include/boost/) and running (root privileges may be required depending on the installation directory):

```console
foo@bar:<path_to_boost>$ patch -p0 < /path/to/boost_copy_component.patch
```
