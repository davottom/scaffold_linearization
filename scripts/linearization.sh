#!/bin/bash
in=data/instances
out=data/result

for file in $in/*.dot
do
    filename=$(basename -- "$file" _scaffold.dot)
    echo $filename
    bin/linearization --fn "$file" --og $out/$filename.dot --lstats data/result/stats.txt --lcompare
done

